/*
 * sysinit.h
 *
 *  Created on: 1 de oct. de 2015
 *      Author: sergio
 */
#ifndef SRC_SYSINIT_H_
#define SRC_SYSINIT_H_

#define PORT_PIN_LED1  0x02
#define PIN_LED1       0x0A
#define PORT_PIN_LED2  0x02
#define PIN_LED2       0x0B
#define PORT_PIN_LED3  0x02
#define PIN_LED3       0x0C

#define PORT_PIN_RGB   0x02
#define PIN_RGB_RED    0x00
#define PIN_RGB_GRN    0x01
#define PIN_RGB_BLU    0x02

#define GPIO_PORT_LED1 0x00
#define GPIO_PIN_LED1  0x0E
#define GPIO_PORT_LED2 0x01
#define GPIO_PIN_LED2  0x0B
#define GPIO_PORT_LED3 0x01
#define GPIO_PIN_LED3  0x0C

#define GPIO_PORT_RGB  0x05
#define GPIO_PIN_RED   0x00
#define GPIO_PIN_GRN   0x01
#define GPIO_PIN_BLU   0x02

#define UART2_TX_PORT  0x07
#define UART2_TX_PIN   0x01

#define UART2_RX_PORT  0x07
#define UART2_RX_PIN   0x02
#define UART_USB       LPC_USART2

void SystemInit(void);

#endif /* SRC_SYSINIT_H_ */
