/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <ipc_msg.h>
#include "lpc4337_ipc.h"


#ifdef CORE_M0

#include <system_init.h>
#include <fs.h>
#include <romfs.h>

#include <defFilesRom.h>
#include <datBoxFs.h>
#include <eeprom_drv.h>

#include <shell.h>

#include <stdio.h>

#include <core_cm0.h>

extern unsigned char defFilesRom[defFilesRom_SZ];

FILE * getStdin(void)
{
	return stdin;
}

static struct stFileSystems filesRomFS =
{
    .fsName = "/romfs",
    .openDir = romFsOpenDir,
    .readDir = romFsReadDir,
    .closeDir = romFsCloseDir,
    .openFile = romFsOpen,
    .readFile = romFsRead,
    .writeFile = NULL,
    .seekFile = romFsSeek,
    .closeFile = romFsClose,
	.format = NULL,
    .unlink = NULL,
    .stat = NULL,
    .fsData = defFilesRom
};

#define blkSzEEPROM 128
unsigned char rwBuffEEPROM[blkSzEEPROM];
dbfsRoot dbfsEEPROM =
{
  .absAddr = (void *)0x20040000,
  .length = (16*1024) - 128,
  .maxFiles = 32,
  .blkSize = blkSzEEPROM,
  .rwBuff = rwBuffEEPROM,
  .fcnRd = plt_eeprom_read,
  .fcnWr = plt_eeprom_write
};


struct stFileSystems fsEEPROM =
{
		.fsName = "/rwfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsEEPROM
};

static tipo_msg_m0 cola[CANT_MENSAJES];

int main(void)
{
  plt_system_init();  
  registerFS(&filesRomFS);
  registerFS(&fsEEPROM);

  IPC_initMsgQueue(cola, sizeof(tipo_msg_m0), CANT_MENSAJES);

  startInteractiveShell();

  while(1);
  return 0;
}

#endif

//#define CORE_M4

#ifdef CORE_M4



#include <chip.h>
#include <stdint.h>
#include <stdbool.h>
#include "sysinit.h"
#include <string.h>
#include <core_cm4.h>

#include "lpc4337_ipc.h"

#define TICKRATE_HZ 4

typedef enum CPUID {
	CPUID_MIN,
	CPUID_M4 = CPUID_MIN,
	CPUID_M0APP,
	CPUID_M0SUB,
	CPUID_MAX
} CPUID_T;

/* M0 Boot loader */
int M0Image_Boot(CPUID_T cpuid, uint32_t m0_image_addr)
{
	/* Make sure the alignment is OK */
	if (m0_image_addr & 0xFFF) {
		return -1;
	}
	/* Sanity check, see if base address and reset handler address resides in same region */
	if ((m0_image_addr & 0xFFF00000) != ((*(unsigned long *) (m0_image_addr + 4)) & 0xFFF00000)) {
		return -2;
	}

	if (cpuid == CPUID_M0APP) {
		/* Make sure the M0 core is being held in reset via the RGU */
		Chip_RGU_TriggerReset(RGU_M0APP_RST);
		Chip_Clock_Enable(CLK_M4_M0APP);
		/* Keep in mind the M0 image must be aligned on a 4K boundary */
		Chip_CREG_SetM0AppMemMap(m0_image_addr);
		Chip_RGU_ClearReset(RGU_M0APP_RST);
	}

	if (cpuid == CPUID_M0SUB) {
		/* Make sure the M0 core is being held in reset via the RGU */
		Chip_RGU_TriggerReset(RGU_M0SUB_RST);
		Chip_Clock_Enable(CLK_PERIPH_CORE);
		/* Keep in mind the M0 image must be aligned on a 4K boundary */
		Chip_CREG_SetM0SubMemMap(m0_image_addr);
		Chip_RGU_ClearReset(RGU_M0SUB_RST);
	}

	return 0;
}

void SystemInit(void)
{
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;
	extern void *isrVector;
	*pSCB_VTOR = (unsigned int) &isrVector;
#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif
	Chip_SystemInit();
	Chip_GPIO_Init(LPC_GPIO_PORT);

	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);

	Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);

}

uint16_t *cont = (uint16_t *) 0x10089FFE;

void TIMER1_IRQHandler(void)
{
//	static bool On = false;

	if (Chip_TIMER_MatchPending(LPC_TIMER1, 1)) {
		Chip_TIMER_ClearMatch(LPC_TIMER1, 1);
		(*cont) ++;

//		On = (bool) !On;
//		Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, (bool) On);
	}


	return;

}

void (*funcion[])(tipo_dir)={
		conmutar_led_m4, 					//0
		estado_led_m4, 						//1
		iniciar_contador_m4,				//2
		valor_contador_m4,					//3
		pausar_contador_m4,					//4
		reiniciar_contador_m4,				//5
		restaurar_contador_m4				//6
};

static tipo_msg_m4 cola[CANT_MENSAJES];

void M0CORE_IRQHandler(void)
{
	tipo_msg_m4 msg;

	Chip_CREG_ClearM0AppEvent();

	if(IPC_tryPopMsg(&msg)!=QUEUE_VALID) return;

	funcion[msg.funId](msg.carga);

	return;
}


int main(void)
{
  uint32_t timerFreq;
  SystemCoreClockUpdate();
  SystemInit();
  M0Image_Boot(CPUID_M0APP, (uint32_t) 0x1b000000);
  Chip_TIMER_Init(LPC_TIMER1);
  Chip_RGU_TriggerReset(RGU_TIMER1_RST);
  while (Chip_RGU_InReset(RGU_TIMER1_RST)) {}

  /* Get timer 1 peripheral clock rate */
  timerFreq = Chip_Clock_GetRate(CLK_MX_TIMER1);

  /* Timer setup for match and interrupt at TICKRATE_HZ */
  Chip_TIMER_Reset(LPC_TIMER1);
  Chip_TIMER_MatchEnableInt(LPC_TIMER1, 1);
  Chip_TIMER_SetMatch(LPC_TIMER1, 1, (timerFreq / TICKRATE_HZ));
  Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, 1);
  Chip_TIMER_Enable(LPC_TIMER1);

  /* Se habilita la interrupcion de TIMER1*/
//  NVIC_EnableIRQ(TIMER1_IRQn);
//  NVIC_ClearPendingIRQ(TIMER1_IRQn);
//  Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, (bool) true);

// Inicio el contador del ejemplo LUA con IPC

  *cont = 0;

  /*Creo la cola de mensajes IPC*/
  IPC_initMsgQueue(cola, sizeof(tipo_msg_m4), CANT_MENSAJES);

  /*Habilito las interrupciones de señales del M0*/
  NVIC_EnableIRQ(M0APP_IRQn);
  NVIC_ClearPendingIRQ(M0APP_IRQn);


  while(1);
  return 0;
}




#endif
