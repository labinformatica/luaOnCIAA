# Ejemplo 13_basic_cmd_ipc

En este ejemplo trata de ser un ejemplo integrador, donde se observen las funcionalidades
de LUA, junto con File System y con el agregado de IPC.

## IPC en este ejemplo

En este ejemplo se carga el LUA con toda la funcionalidad desarrollada hasta ahora para
el módulo IPC. 

## ¿Cómo programar este ejemplo?

Una vez construido el proyecto debemos ubicarnos en el directorio de este ejemplo
y ejecutar los siguientes comandos:

>>>
make program-m4
>>>

Para programar el Cortex M4. Y luego:

>>>
make program-m0
>>>

Para programar el Cortex M0.

Es decir, no se debe programar el Loader ☺️ .
