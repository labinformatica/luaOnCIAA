#include "system_init.h"
#include <string.h>
#include <stdio.h>
#include <lpc_types.h>

int main(void)
{
  int op;
  int v1, v2, r;
  plt_system_init();
  while(TRUE)
  {
    printf("Indique la operación a realizar:\n\r");
    printf("1 - Suma\n\r");
    printf("2 - Resta\n\r");
    scanf("%d", &op);
    if((op >=1) && (op<=2))
    {
      printf("Ingrese primer operador entero:");
      fflush(stdout);
      scanf("%d", &v1);
      printf("Ingrese segundo operador entero:");
      fflush(stdout);
      scanf("%d", &v2);
      switch(op)
      {
        case 1:
          r = v1 + v2;
          printf("%d + %d = %d\n\r", v1, v2, r);
          break;
        case 2:
          r = v1 - v2;
          printf("%d - %d = %d\n\r", v1, v2, r);
          break;
      }
    }
    else
      printf("Opción inválida\n\r");
  }
  return 0;
}
