/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "chip.h"
#include <stdint.h>
#include <lpc_types.h>
#include <string.h>
#include <ipc_msg.h>

#include "sysinit.h"

#define TICKRATE_HZ 10

#define CANT_MENSAJES 16

#define SHARED_MEM_IPC      0x2000C000 //Aquí estaran ubicadas las colas del IPC


//Defino el tipo de mensaje que voy a armar
typedef struct d{
		uint8_t port;
		uint8_t pin;
	}dato;

typedef struct un_msg{

	uint16_t cpu;
	int funId;
	dato data;
} tipo_msg;

//Defino a continuación un vector de mensajes
static tipo_msg cola[CANT_MENSAJES];

//Defino la siguiente enumeración para trabajar el vector de colas
typedef enum CPUID {
	CPUID_MIN,
	CPUID_M4 = CPUID_MIN,
	CPUID_M0APP,
	CPUID_M0SUB,
	CPUID_MAX
} CPUID_T;

//Defino funciones que penderan/apagaran leds
void prender_led (dato Data)
{
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, Data.port, Data.pin, TRUE);
	return;
	}

void apagar_led (dato Data)
{
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, Data.port, Data.pin, FALSE);
	return;
	}

//Defino vector de funciones
void (*funcion[])( dato ) = {prender_led, apagar_led};

//Funcion para configurar los puertos, la cola IPC y demas.
void configurar(void)
{

	Chip_GPIO_Init(LPC_GPIO_PORT);

	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);

	Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);

	IPC_initMsgQueue(cola, sizeof(tipo_msg), CANT_MENSAJES);


	return;
}

#ifdef CORE_M0

#include <core_cm0.h>

#define CPUID_CURR     CPUID_M0APP

void M0_M4CORE_IRQHandler(void)
{
  tipo_msg msg_leer, msg_mandar;

  Chip_CREG_ClearM4Event();

  if (IPC_tryPopMsg(&msg_leer)!=QUEUE_VALID) {return;}

  funcion[msg_leer.funId]((dato) msg_leer.data);
  msg_mandar.cpu = CPUID_M4;
  msg_mandar.funId =  msg_leer.funId^0x01;
  msg_mandar.data.port = GPIO_PORT_LED2;
  msg_mandar.data.pin = GPIO_PIN_LED2;

  IPC_tryPushMsg(msg_mandar.cpu,&msg_mandar);

  msg_mandar.cpu = CPUID_M4;
  msg_mandar.funId =  msg_leer.funId;
  msg_mandar.data.port = GPIO_PORT_LED3;
  msg_mandar.data.pin = GPIO_PIN_LED3;

  IPC_tryPushMsg(msg_mandar.cpu,&msg_mandar);

}


int main(void)
{



configurar();

NVIC_EnableIRQ(M4_IRQn);
NVIC_ClearPendingIRQ(M4_IRQn);


while(1);


return 0;
}



#endif

#ifdef CORE_M4

#include <core_cm4.h>
#define CPUID_CURR     CPUID_M4



void TIMER0_IRQHandlerIPC(void)
{

  static int fun = 0;
  tipo_msg msg;


  if (Chip_TIMER_MatchPending(LPC_TIMER0, 0)) {
    Chip_TIMER_ClearMatch(LPC_TIMER0, 0);
    fun = fun ^ 0x01;
    msg.cpu = CPUID_M0APP;
    msg.funId = fun;
    msg.data.port = GPIO_PORT_LED1;
    msg.data.pin = GPIO_PIN_LED1;

    IPC_tryPushMsg(msg.cpu, &msg);}

    return;
  }

void M0CORE_IRQHandler(void)
{
	tipo_msg msg;

	Chip_CREG_ClearM0AppEvent();

	if(IPC_tryPopMsg(&msg)!=QUEUE_VALID) {return;}

	funcion[msg.funId]((dato) msg.data);

	return;
}

extern int M0Image_Boot(CPUID_T cpuid, uint32_t m0_image_addr);

int main(void)
{

	uint32_t timerFreq;

  Chip_SystemInit();
  SystemCoreClockUpdate();

  M0Image_Boot(CPUID_M0APP, (uint32_t) 0x1b000000);

  configurar();

  Chip_TIMER_Init(LPC_TIMER0);
  Chip_RGU_TriggerReset(RGU_TIMER0_RST);

  while (Chip_RGU_InReset(RGU_TIMER0_RST)) {}

  timerFreq = Chip_Clock_GetRate(CLK_MX_TIMER0);

  Chip_TIMER_Reset(LPC_TIMER0);
  Chip_TIMER_MatchEnableInt(LPC_TIMER0, 0);
  Chip_TIMER_SetMatch(LPC_TIMER0, 0, (timerFreq / TICKRATE_HZ));
  Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, 0);
  Chip_TIMER_Enable(LPC_TIMER0);

  NVIC_EnableIRQ(TIMER0_IRQn);
  NVIC_ClearPendingIRQ(TIMER0_IRQn);
  NVIC_EnableIRQ(M0APP_IRQn);
  NVIC_ClearPendingIRQ(M0APP_IRQn);




  while(1);

return 0;



}
#endif
