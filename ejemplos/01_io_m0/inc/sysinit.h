/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef SRC_SYSINIT_H_
#define SRC_SYSINIT_H_

#define PORT_PIN_LED1  0x02
#define PIN_LED1       0x0A
#define PORT_PIN_LED2  0x02
#define PIN_LED2       0x0B
#define PORT_PIN_LED3  0x02
#define PIN_LED3       0x0C

#define PORT_PIN_RGB   0x02
#define PIN_RGB_RED    0x00
#define PIN_RGB_GRN    0x01
#define PIN_RGB_BLU    0x02

#define GPIO_PORT_LED1 0x00
#define GPIO_PIN_LED1  0x0E
#define GPIO_PORT_LED2 0x01
#define GPIO_PIN_LED2  0x0B
#define GPIO_PORT_LED3 0x01
#define GPIO_PIN_LED3  0x0C

#define GPIO_PORT_RGB  0x05
#define GPIO_PIN_RED   0x00
#define GPIO_PIN_GRN   0x01
#define GPIO_PIN_BLU   0x02

#define UART2_TX_PORT  0x07
#define UART2_TX_PIN   0x01

#define UART2_RX_PORT  0x07
#define UART2_RX_PIN   0x02
#define UART_USB       LPC_USART2

#endif /* SRC_SYSINIT_H_ */
