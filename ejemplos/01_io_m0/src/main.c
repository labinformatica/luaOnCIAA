/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "chip.h"
#include <stdint.h>
#include <stdbool.h>
#include "stopwatch.h"
#include <string.h>
#include "sysinit.h"

#define TICKRATE_HZ 10

#if defined(CORE_M0)
void M0_TIMER0_IRQHandler(void)
{
  static bool On = false;
  if (Chip_TIMER_MatchPending(LPC_TIMER0, 0)) {
    Chip_TIMER_ClearMatch(LPC_TIMER0, 0);
    On = (bool) !On;
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2, (bool) On);
  }
}
#elif defined(CORE_M4)
void TIMER0_IRQHandler(void)
{
  static bool On = false;
  if (Chip_TIMER_MatchPending(LPC_TIMER0, 0)) {
    Chip_TIMER_ClearMatch(LPC_TIMER0, 0);
    On = (bool) !On;
    Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2, (bool) On);
  }
}
#else
  #error You must define "CORE_M0" or "CORE_M4" as main processor
#endif

int main(void)
{ 
  uint32_t timerFreq;
  char txt[] = "Hola mundo\r\n";  
  #if defined(CORE_M4)
    Chip_SystemInit();
    SystemCoreClockUpdate();
  #endif
  Chip_GPIO_Init(LPC_GPIO_PORT);

  Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
  Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
  Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);

  Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
  Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
  Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);

  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);

  Chip_SCU_PinMux(UART2_TX_PORT, UART2_TX_PIN, SCU_MODE_INACT, FUNC6);
  Chip_SCU_PinMux(UART2_RX_PORT, UART2_RX_PIN, SCU_MODE_INACT, FUNC6);
  Chip_UART_Init(UART_USB);
  Chip_UART_SetBaud(UART_USB, 115200);
  Chip_UART_ConfigData(UART_USB, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
  Chip_UART_TXEnable(UART_USB);

  Chip_TIMER_Init(LPC_TIMER0);
  Chip_RGU_TriggerReset(RGU_TIMER0_RST);
  while (Chip_RGU_InReset(RGU_TIMER0_RST)) {}

  timerFreq = Chip_Clock_GetRate(CLK_MX_TIMER0);

  Chip_TIMER_Reset(LPC_TIMER0);
  Chip_TIMER_MatchEnableInt(LPC_TIMER0, 0);
  Chip_TIMER_SetMatch(LPC_TIMER0, 0, (timerFreq / TICKRATE_HZ));
  Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, 0);
  Chip_TIMER_Enable(LPC_TIMER0);

  NVIC_EnableIRQ(TIMER0_IRQn);
  NVIC_ClearPendingIRQ(TIMER0_IRQn);
  Chip_UART_SendBlocking(UART_USB, txt, 12);

  while(1);
  return 0;
}
