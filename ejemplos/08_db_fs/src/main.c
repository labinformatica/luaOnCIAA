/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <system_init.h>
#include <serial_drv.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <datBoxFs.h>
#include <chip.h>
#include <fs.h>
#include <eeprom_drv.h>

void writeRam(struct stDbFsStruct * notUsed, uint32_t offset, unsigned char *readFrom, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        ((unsigned char *)(baseAddr+offset))[i] = readFrom[i];
}

void readRam (struct stDbFsStruct *notUsed, unsigned char *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        writeTo[i] = ((unsigned char *)(baseAddr+offset))[i];
}

//
// RAM Config
//

#define blockSize 128
dbfsRoot dbfsRam =
{
  .absAddr = (void *)0x2000C000,
  .length = 0x4000,
  .maxFiles = 32,
  .blkSize = blockSize,
  .rwBuff = NULL,
  .fcnRd = readRam,
  .fcnWr = writeRam
};

struct stFileSystems ramFs =
{
		.fsName = "/ramfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsRam
};


unsigned char dbfsRWbuffEE[blockSize];

dbfsRoot dbfsEE =
{
  .absAddr = (void *)0x20040000,
  .length = (16*1024) - 128,
  .maxFiles = 32,
  .blkSize = blockSize,
  .rwBuff = dbfsRWbuffEE,
  .fcnRd = plt_eeprom_read,
  .fcnWr = plt_eeprom_write
};


struct stFileSystems rwfs =
{
		.fsName = "/rwfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsEE
};

#define TESTBYTECOUNT 8193

int main(void)
{
  FILE * f;
  unsigned char rwByte;
  unsigned char cmp;
  unsigned char wByte;
  int j;
  int byteToWrite;

  dirEntry dir;
  struct stFileSystems * fs;
  fileEntry * file;

  plt_system_init();
  printf("Iniciando...\n\r");
  fflush(stdout);
  dbfsRam.rwBuff = malloc(blockSize);

  printf("Formateando...\n\r");
  fflush(stdout);
  dbfsFormat(&ramFs);
  printf("Listo formato.\n\r");
  fflush(stdout);

  if(registerFS(&ramFs))
  {
	  printf("Iniciando pruebas de lectura/escritura secuenciales:\n");
	  for(byteToWrite = 1; byteToWrite < TESTBYTECOUNT; byteToWrite++)
	  {
	    f = fopen("/ramfs/fileTest.txt", "wb");
	    if(!f)
	    {
	    	printf("Error abriendo el archivo para escritura, iteración %d\n", byteToWrite);
	    	while(1);
	    }
	    rwByte = 0;
	    for(j=0;j<byteToWrite;j++)
	    {
	      fwrite(&rwByte, 1, 1, f);
	      rwByte++;
	    }
	    fclose(f);
		fs = getRegisteredFS(0);
		fs->openDir(fs, &dir);
		file = fs->readDir(&dir);
		if (file->size != byteToWrite)
		{
		   	printf("Tamaño registrado inconsistente. Registrado %lu real %d\n", file->size, byteToWrite);
		   	while(1);
	    }
	    f = fopen("/ramfs/fileTest.txt", "rb");
	    if(!f)
	    {
	    	printf("Error abriendo el archivo para lectura, iteración %d\n", byteToWrite);
	    	while(1);
	    }
	    cmp = 0;
	    for(j = 0;j < byteToWrite;j++)
	    {
	      fread(&rwByte, 1, 1, f);
	      if (rwByte != cmp)
	      {
		    	printf("Error de validación de escritura %d\n", byteToWrite);
		    	while(1);
	      }
	      cmp++;
	    }
	    fclose(f);
	    printf("Test con %d bytes ok\n", byteToWrite);
	  }


	  printf("Formateando por segunda vez\n\r");
	  fflush(stdout);
	  dbfsFormat(&ramFs);
	  printf("Listo formato.\n\r");
	  printf("Iniciando pruebas de lectura/escritura incrementales:\n");
	  wByte = 0;
	  f = fopen("/ramfs/fileTest.txt", "wb");
      if(!f)
	  {
	   	printf("Error abriendo el archivo para escritura \"wb\"\n");
	  	while(1);
	  }
      fclose(f);

      for(byteToWrite = 1; byteToWrite < TESTBYTECOUNT; byteToWrite++)
	  {
	  	    f = fopen("/ramfs/fileTest.txt", "rb+");
	  	    if(!f)
	  	    {
	  	    	printf("Error abriendo el archivo para escritura, iteración %d\n", byteToWrite);
	  	    	while(1);
	  	    }
	  	    fseek(f, 0, SEEK_END);
	  	    fwrite(&wByte, 1, 1, f);
	  	    wByte++;
	  	    fclose(f);


	  		fs = getRegisteredFS(0);
	  		fs->openDir(fs, &dir);
	  		file = fs->readDir(&dir);
	  		if (file->size != byteToWrite)
	  		{
	  		   	printf("Tamaño registrado inconsistente. Registrado %lu real %d\n", file->size, byteToWrite);
	  		   	while(1);
	  	    }

	  		f = fopen("/ramfs/fileTest.txt", "rb");
	  	    if(!f)
	  	    {
	  	    	printf("Error abriendo el archivo para lectura, iteración %d\n", byteToWrite);
	  	    	while(1);
	  	    }
	  	    cmp = 0;
	  	    for(j=0;j<byteToWrite;j++)
	  	    {
	  	      fread(&rwByte, 1, 1, f);
	  	      if (rwByte != cmp)
	  	      {
	  		    	printf("Error de validación de escritura %d\n", byteToWrite);
	  		    	while(1);
	  	      }
	  	      cmp++;
	  	    }
	  	    printf("Test incremental con %d bytes ok\n", byteToWrite);
	  	    fclose(f);
	  	  }
  }
  else
  {
    printf("Error registrando RAM File System\n");
    fflush(stdout);
  }
  printf("\n-= Fin =-\n");

  while(1);
  return 0;
}
