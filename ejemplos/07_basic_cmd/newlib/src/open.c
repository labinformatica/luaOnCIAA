/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <fs.h>
#include <stdio.h>
#include <stdlib.h>

//#undef errno
//extern int  errno;

int _open (char *name, int flags, int mode)
{
  int f = -1;
  int fsNEnd = 0;
  int fnLen = strlen(name);
  unsigned int i = 0;
  unsigned int j = 0;
  int eqc = 0;
  int match = 0;
  struct stFileSystems * fs;
  /* Buscamos el nombre del punto de montaje */
  for(i = 1 ; (i < fnLen) ; i++){
	  if(name[i] == '/'){
		  fsNEnd = i;
	  }
  }
  // Si tenemos un nombre de punto de montaje y un nombre de archivo ...
  if(fsNEnd){
	  for(i = 0 ; ((i < getRegisteredFSCount()) && (match == 0)) ; i++){

		  eqc = 0;
		  fs = getRegisteredFS(i);

		  for((j=0); j < fsNEnd; j++){
			  if(fs->fsName[j] == name[j]){
				  eqc++;
			  }
		  }
		  if((eqc == fsNEnd) && (fs->fsName[fsNEnd] == '\0') && (name[fsNEnd] == '/')){  /* comparar final de cadena del fsName con / */
				  match = 1;
		  }
	  }
	  if(match){
		  f = fs->openFile(name, flags, mode, fs);
	  }
  }
  return f;
}
