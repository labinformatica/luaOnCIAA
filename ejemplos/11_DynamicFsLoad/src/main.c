/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*
 * Para probar este ejemplo es necesario agregar la definición de la constante simbólica USE_FS_DYNAMIC_LOAD
 * a nivel de proyecto. Esto habilita la inclusión de la función "loadRamFs" en el shell pero a costa
 * de que los demás ejemplos no se puedan compilar. Es por esto también que se recomienda habilitar la
 * compilación de solo este ejemplo.
 * En conclusión, para probar este ejemplo requiere:
 *  - Hacer make clean de todo el proyecto.
 *  - Editar el archivo buildOpt.mk que se encuentra en el directorio raíz y descomentar la línea 27 y
 *    comentar la línea 28 de modo que queden como:
       FSLOAD=-DUSE_FS_DYNAMIC_LOAD
       #FSLOAD=""
 *  - Modificar el archivo makefile dentro de la carpeta ejemplos de modo que las dos primeras líneas
 *  queden de este modo:
       buildApp:=11_DynamicFsLoad
       xbuildApp:=$(wildcard *_*)

 *  Esta modificación hará que solo se compile el ejemplo 11. Si se quiere que se compilen todos los
 *  ejemplos debería hacerse esto.
       xbuildApp:=11_DynamicFsLoad
       buildApp:=$(wildcard *_*)

 *  - Ejecutar make
 *  - Ingresar a la carpeta del ejemplo 11:
     cd ejemplos/11_DynamicFsLoad
 *  - Programar el ejemplo en la placa:
     make program-m4
 *  - Ejecutar fsImgBuilder
 *  - Cargar o crear una imagen de sistema de archivos de tamaño menor o igual a 0x4000 bytes (que es
 *  el tamaño reservado para el alojamiento de la imagen en el linker script).
 *  - Salir del editor de sistemas de archivos.
 *  - Iniciar una sesión en la placa utilizando una terminal. Se podrá observar el sistema de archivos
 *  cargado con los archivos agregados a la imagen del sistema.
 */



#include <system_init.h>
#include <serial_drv.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <datBoxFs.h>
#include <romfs.h>
#include <defFilesRom.h>
#include <chip.h>
#include <fs.h>
#include <eeprom_drv.h>
#include <shell.h>

void writeRam(struct stDbFsStruct * notUsed, uint32_t offset, unsigned char *readFrom, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        ((unsigned char *)(baseAddr+offset))[i] = readFrom[i];
}

void readRam (struct stDbFsStruct *notUsed, unsigned char *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        writeTo[i] = ((unsigned char *)(baseAddr+offset))[i];
}

//
// RAM Config
//

#define blockSize 128
dbfsRoot dbfsRam =
{
  .absAddr = (void *)0x2000C000,
  .length = 0x4000,
  .maxFiles = 32,
  .blkSize = blockSize,
  .rwBuff = NULL,
  .fcnRd = readRam,
  .fcnWr = writeRam
};

struct stFileSystems ramFs =
{
		.fsName = "/ramfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsRam
};

extern unsigned char defFilesRom[defFilesRom_SZ];

FILE * getStdin(void)
{
	return stdin;
}

static struct stFileSystems filesRomFS =
{
    .fsName = "/romfs",
    .openDir = romFsOpenDir,
    .readDir = romFsReadDir,
    .closeDir = romFsCloseDir,
    .openFile = romFsOpen,
    .readFile = romFsRead,
    .writeFile = NULL,
    .seekFile = romFsSeek,
    .closeFile = romFsClose,
	.format = NULL,
    .unlink = NULL,
    .stat = NULL,
    .fsData = defFilesRom
};

#define blkSzEEPROM 128
unsigned char rwBuffEEPROM[blkSzEEPROM];
dbfsRoot dbfsEEPROM =
{
  .absAddr = (void *)0x20040000,
  .length = (16*1024) - 128,
  .maxFiles = 32,
  .blkSize = blkSzEEPROM,
  .rwBuff = rwBuffEEPROM,
  .fcnRd = plt_eeprom_read,
  .fcnWr = plt_eeprom_write
};


struct stFileSystems fsEEPROM =
{
		.fsName = "/rwfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsEEPROM
};
int main(void)
{
  plt_system_init();
  dbfsRam.rwBuff = malloc(blockSize);

  printf("Formateando...\n\r");
  fflush(stdout);
  dbfsFormat(&ramFs);
  printf("Listo formato.\n\r");
  fflush(stdout);
  registerFS(&filesRomFS);
  registerFS(&ramFs);
  registerFS(&fsEEPROM);
  startInteractiveShell();

  while(1);
  return 0;
}
