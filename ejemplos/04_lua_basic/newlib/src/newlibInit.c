/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <serial_drv.h>
#include <lpc_types.h>
#include <unistd.h>
#include <stdio.h>
#include <fs.h>

int stdinRead(int file, uint8_t *ptr,  int len)
{
  uint8_t newLine[] = "\r\n";
  unsigned int readed = 0;
  unsigned int i=0;
  int ret;
  unsigned char ok = 0;
  while(!ok)
  {
    readed = readed + plt_recv_default_usart(&ptr[readed], len);
    while(i < readed)
    {
      if(ptr[i] == 13)
      {
        ptr[i] = '\n';
    	ok = 1;
        plt_send_default_usart(newLine, 2);
      }
      else if(ptr[i] == 0x04)
      {
        ptr[i] = 0;
        return 0;
      }
      else
        plt_send_default_usart(&ptr[i], 1);
      i++;
    }
  }
  ret = readed;
  return ret;
}

int stdoutWrite (int file, const uint8_t *buf, int nbytes)
{
  int ret = 0;
  int i;
  uint8_t c = '\r';
  for(i=0; i < nbytes; i++)
  {    
    if(buf[i]=='\n')
      plt_send_default_usart(&c, 1);
    ret += plt_send_default_usart(&buf[i], 1);
  }
  return ret;
}

static struct stFileSystems consoleInput =
{
	    .openDir = NULL,
	    .readDir = NULL,
	    .closeDir= NULL,
	    .openFile = NULL,
	    .readFile = stdinRead,
	    .writeFile = NULL,
	    .seekFile = NULL,
	    .closeFile = NULL,
	    .unlink = NULL,
	    .stat = NULL,
	    .fsData = NULL
};

static struct stFileSystems consoleOutput =
{
	    .openDir = NULL,
	    .readDir = NULL,
	    .closeDir= NULL,
	    .openFile = NULL,
	    .readFile = NULL,
	    .writeFile = stdoutWrite,
	    .seekFile = NULL,
	    .closeFile = NULL,
	    .unlink = NULL,
	    .stat = NULL,
	    .fsData = NULL
};

void newLibInit(void)
{
  setbuf(stdout, NULL);
  registerStdFD(&consoleInput, STDIN_FILENO);
  registerStdFD(&consoleOutput, STDOUT_FILENO);
  registerStdFD(&consoleOutput, STDERR_FILENO);
}
