#ifndef _FS_DEFINE_____
#define _FS_DEFINE_____
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <datBoxFs.h>
#include <romfs.h>
#include <defFilesRom.h>
#include <chip.h>
#include <fs.h>
//------------------------------------
//     RAM FS
//------------------------------------
void writeRam(struct stDbFsStruct * notUsed, uint32_t offset, unsigned char *readFrom, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        ((unsigned char *)(baseAddr+offset))[i] = readFrom[i];
}

void readRam (struct stDbFsStruct *notUsed, unsigned char *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        writeTo[i] = ((unsigned char *)(baseAddr+offset))[i];
}

#define blockSize 128
dbfsRoot dbfsRam =
{
  .absAddr = (void *)0x10004000,
  .length = 0x4000,
  .maxFiles = 32,
  .blkSize = blockSize,
  .rwBuff = NULL,
  .fcnRd = readRam,
  .fcnWr = writeRam
};

struct stFileSystems ramFs =
{
		.fsName = "/ramfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsRam
};
//-----------------------------------
//      ROM FS
//-----------------------------------

extern unsigned char defFilesRom[defFilesRom_SZ];

FILE * getStdin(void)
{
	return stdin;
}

static struct stFileSystems filesRomFS =
{
    .fsName = "/romfs",
    .openDir = romFsOpenDir,
    .readDir = romFsReadDir,
    .closeDir = romFsCloseDir,
    .openFile = romFsOpen,
    .readFile = romFsRead,
    .writeFile = NULL,
    .seekFile = romFsSeek,
    .closeFile = romFsClose,
	.format = NULL,
    .unlink = NULL,
    .stat = NULL,
    .fsData = defFilesRom
};
//-----------------------------------
//      FS EEPROM
//-----------------------------------
#define blkSzEEPROM 128
unsigned char rwBuffEEPROM[blkSzEEPROM];
dbfsRoot dbfsEEPROM =
{
  .absAddr = (void *)0x20040000,
  .length = (16*1024) - 128,
  .rwBuff = rwBuffEEPROM,
  .fcnWr = plt_eeprom_write,
  .fcnRd = plt_eeprom_read,
  .maxFiles = 32,
  .fcnOpn = NULL,
  .fcnCls = NULL,
  .blkSize = blkSzEEPROM
};


struct stFileSystems fsEEPROM =
{
		.fsName = "/rwfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsEEPROM
};

//-----------------------------------------
//    FLASH FS interno
//-----------------------------------------

extern unsigned char _binary_testImage_img_start;
extern unsigned char _binary_testImage_img_end;
extern unsigned char _binary_testImage_img_size;

void readFlash (struct stDbFsStruct *notUsed, unsigned char *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        writeTo[i] = ((unsigned char *)(baseAddr+offset))[i];
}

#define blkSzDbFsFlash 128
unsigned char rwBuffFlash[blkSzDbFsFlash];
dbfsRoot dbfsFlash =
{
  .absAddr = (void *)&_binary_testImage_img_start,
  .length = 8192,
  .maxFiles = 32,
  .blkSize = blkSzDbFsFlash,
  .rwBuff = rwBuffFlash,
  .fcnRd = readFlash,
  .fcnWr = NULL
};


struct stFileSystems fsFlash =
{
		.fsName = "/flashFs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = NULL,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsFlash
};
//------------------------------------------------------------------------------------
#endif
