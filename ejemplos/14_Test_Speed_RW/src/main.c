/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <system_init.h>
#include <serial_drv.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <datBoxFs.h>
#include <romfs.h>
#include <defFilesRom.h>
#include <chip.h>
#include <fs.h>
#include <eeprom_drv.h>
#include <shell.h>
#include <tmr_drv.h>
#include "fileSystem.h"
//------------------------------------------------------------------------------------
char buf[1024]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,
27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,
56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,
85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,
110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,
132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,
154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,
176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,
198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,
220,221,222,223,224,225,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,
54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,
83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,
109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,
131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,
153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,
175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,
197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,
219,220,221,222,223,224,225,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,
52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,
81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,
108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,
130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,
152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,
174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,
196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,
218,219,220,221,222,223,224,225,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,
51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,
80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,
107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,
129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,
151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,
173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,
195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,
217,218,219,220,221,222,223,224,225};

uint64_t sys_count;
void print_time(uint64_t t){
	uint16_t s;
	printf(" en: %lu ms",(uint32_t)t);
	if(t > 1000){
		s=((uint32_t)t/1000);
		printf("(%d seg)",s);
	}
	printf("\n");
}

int main(void)
{ uint8_t i,a;
uint8_t bufR[1024];
  plt_system_init();  
  registerFS(&filesRomFS);
  registerFS(&fsEEPROM);
  dbfsRam.rwBuff = malloc(blockSize);
  fflush(stdout);
  dbfsFormat(&ramFs);
  fflush(stdout);
  registerFS(&ramFs);
  FILE *f;

//---------------------------------------------------------
//      PRUEBA EN RAMFS RW
//---------------------------------------------------------
  printf("\nPrueba de velocidad en RAMFS:\n");
  sys_count=getSysTick();
  for(a=0;a<10;a++){
	  	  f=fopen("/ramfs/test_RW.txt","w");
	  if(f==NULL){printf("\n Error en crear el archivo");
	  }else{
		  for(i=0;i<10;i++)
			  fwrite( buf, sizeof(char), sizeof(buf), f );
		  fclose (f);
	  }
  }
  sys_count=getSysTick()-sys_count;
  printf("Se escribio 100kB");
  print_time(sys_count);
/*********************************************************/
  sys_count=getSysTick();
  for(a=0;a<10;a++){
	  f=fopen("/ramfs/test_RW.txt","r");
	  if(f==NULL){printf("\n Error leer el archivo de prueba");
	  }else{
		  for(i=0;i<10;i++)
			  fread( bufR, sizeof(char), sizeof(bufR), f );
		  fclose (f);
	  }
  }
  sys_count=getSysTick()-sys_count;
  printf("Se leyo 100kB");
  print_time(sys_count);
//---------------------------------------------------------
//      PRUEBA EN FLASH EXTERNA RW
//---------------------------------------------------------
    printf("\nPrueba de velocidad en FLASH EXTERNA:\n");
    sys_count=getSysTick();
    for(a=0;a<10;a++){
  	  f=fopen("/eflash/test_RW.txt","w");
  	  if(f==NULL){printf("\n Error en crear el archivo");
  	  }else{
  		  for(i=0;i<10;i++)
  			  fwrite( buf, sizeof(char), sizeof(buf), f );
  		  fclose (f);
  	  	  }
    }
    sys_count=getSysTick()-sys_count;
    printf("Se escribio 100kB");
    print_time(sys_count);
/*********************************************************/
    sys_count=getSysTick();
    for(a=0;a<10;a++){
  	  f=fopen("/eflash/test_RW.txt","r");
  	  if(f==NULL){printf("\n Error leer el archivo de prueba");
  	  }else{
  		  for(i=0;i<10;i++)
  			  fread( bufR, sizeof(char), sizeof(bufR), f );
  		  fclose (f);
  	  }
    }
    sys_count=getSysTick()-sys_count;
    printf("Se leyo 100kB");
    print_time(sys_count);
//---------------------------------------------------------
//      PRUEBA EN FLASH INTERNA R
//---------------------------------------------------------
    printf("\nPrueba de velocidad en FLASH INTERNA:\n");
    sys_count=getSysTick();
    for(a=0;a<10;a++){
      f=fopen("/flashFs/test_R.txt","r");
      if(f==NULL){
      	printf("\n Error leer el archivo de prueba");
      }else{
      	for(i=0;i<10;i++)
      		fread( bufR, sizeof(char), sizeof(bufR), f );
      	fclose (f);
      }
    }
    sys_count=getSysTick()-sys_count;
    printf("Se leyo 100kB");
    print_time(sys_count);
//----------------------------------------------------------------
//      PRUEBA EN EEPROM RW
//----------------------------------------------------------------
      printf("\nPrueba de velocidad en EEPROM:\n");
      sys_count=getSysTick();
      for(a=0;a<10;a++){
    	  f=fopen("/rwfs/test_RW.txt","w");
    	   if(f==NULL){printf("\n Error en crear el archivo");
    	  }else{
    		  for(i=0;i<10;i++)
    			  fwrite( buf, sizeof(char), sizeof(buf), f );
    		  fclose (f);
    	  }
      }
      sys_count=getSysTick()-sys_count;
      printf("Se escribio 100kB");
      print_time(sys_count);
/*********************************************************/
      sys_count=getSysTick();
      for(a=0;a<10;a++){
      f=fopen("/rwfs/test_RW.txt","r");
      if(f==NULL){printf("\n Error leer el archivo de prueba");
      }else{
    	  for(i=0;i<10;i++)
    		  fread( bufR, sizeof(char), sizeof(bufR), f );
    	  fclose (f);
      	  }
      }
      sys_count=getSysTick()-sys_count;
      printf("Se leyo 100kB");
      print_time(sys_count);


/*
//---------------------------------------------------------
//      PRUEBA EN FLASH INTERNA R
//---------------------------------------------------------
        printf("\nPrueba de velocidad en FLASH INTERNA:\n");
        sys_count=getSysTick();
        for(a=0;a<10;a++){
          f=fopen("/romfs/test_R.txt","r");
          if(f==NULL){
          	printf("\n Error leer el archivo de prueba");
          }else{
          	for(i=0;i<10;i++)
          		fread( bufR, sizeof(char), sizeof(bufR), f );
          	fclose (f);
          }
        }
        sys_count=getSysTick()-sys_count;
        printf("Se leyo 100kB");
        print_time(sys_count);*/
//--------------------------------------------------------------
//      FIN DE PRUEBAS
//--------------------------------------------------------------
  while (1);
  return 0;
}
