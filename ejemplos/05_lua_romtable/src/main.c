/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <system_init.h>
#include <fs.h>
#include <romfs.h>

#include <defFilesRom.h>
#include <datBoxFs.h>
#include <eeprom_drv.h>
#include <shell.h>
#include <stdio.h>
#include <stdlib.h>


extern const unsigned char defFilesRom[defFilesRom_SZ];

FILE * getStdin(void)
{
	return stdin;
}

static struct stFileSystems filesRomFS =
{
    .fsName = "/romfs",
    .openDir = romFsOpenDir,
    .readDir = romFsReadDir,
    .closeDir = romFsCloseDir,
    .openFile = romFsOpen,
    .readFile = romFsRead,
    .writeFile = NULL,
    .seekFile = romFsSeek,
    .closeFile = romFsClose,
	.format = NULL,
    .unlink = NULL,
    .stat = NULL,
    .fsData = defFilesRom
};

#define blkSzEEPROM 128
unsigned char rwBuffEEPROM[blkSzEEPROM];
dbfsRoot dbfsEEPROM =
{
  .absAddr = (void *)0x20040000,
  .length = (16*1024) - 128,
  .rwBuff = rwBuffEEPROM,
  .fcnWr = plt_eeprom_write,
  .fcnRd = plt_eeprom_read,
  .maxFiles = 32,
  .fcnOpn = NULL,
  .fcnCls = NULL,
  .blkSize = blkSzEEPROM
};


struct stFileSystems fsEEPROM =
{
		.fsName = "/rwfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsEEPROM
};


void writeRam(struct stDbFsStruct * notUsed, uint32_t offset, unsigned char *readFrom, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        ((unsigned char *)(baseAddr+offset))[i] = readFrom[i];
}

void readRam (struct stDbFsStruct *notUsed, unsigned char *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
    unsigned int i;
    for(i=0; i < len; i++)
        writeTo[i] = ((unsigned char *)(baseAddr+offset))[i];
}

//
// RAM Config
//

#define blockSize 128
dbfsRoot dbfsRam =
{
  .absAddr = (void *)0x2000C000,
  .length = 0x4000,
  .maxFiles = 32,
  .blkSize = blockSize,
  .rwBuff = NULL,
  .fcnRd = readRam,
  .fcnWr = writeRam
};

struct stFileSystems ramFs =
{
		.fsName = "/ramfs",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsRam
};

int main(void)
{
  plt_system_init();  
  registerFS(&filesRomFS);
  registerFS(&fsEEPROM);
  dbfsRam.rwBuff = malloc(blockSize);
  fflush(stdout);
  dbfsFormat(&ramFs);
  fflush(stdout);
  registerFS(&ramFs);
  startInteractiveShell();
  while(1);
  return 0;
}
