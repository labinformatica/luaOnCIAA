/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <errno.h>
#include <string.h>
#include <lpc_types.h>
#include <stdio.h>
#include <fs.h>


//#undef errno
//extern int  errno;

int
_unlink (char *name)
{

	  int f = -1;
	  int fnStart = 0;
	  int fsNEnd = 0;
	  int fnLen = strlen(name);
	  int i;
	  struct stFileSystems * fs;
	  /* Buscamos el nombre del punto de montaje */
	  i = 0;
	  for(i = 1; (i < fnLen - 1); i++)
	    if(name[i] == '/')
	    {
	      fnStart = i + 1;
	      fsNEnd = i - 1;
	    }
	  // Si tenemos un nombre de punto de montaje y un nombre de archivo ...
	  if(fsNEnd && fnStart)
	  {
	    for(i=0; i < getRegisteredFSCount(); i++)
	    {
	       fs = getRegisteredFS(i);
	       // Encontramos el punto de montaje
	       if(strncmp(fs->fsName, name, fsNEnd) == 0)
	       {
	    	   if(fs->unlink != NULL)
	             f = fs->unlink(name, (struct stFileSystems *)fs);
	    	   else
	    		 printf("Error: operación no soportada por el sistema de archivos\n\r");
	       }
	    }
	  }
	  return f;

}
