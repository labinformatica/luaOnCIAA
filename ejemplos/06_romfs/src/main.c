/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <system_init.h>
#include <serial_drv.h>
#include <string.h>
#include <stdio.h>
#include <chip.h>
#include <fs.h>
#include <romfs.h>
#include <unistd.h>
#include "defFilesRom.h"
#include "memory.h"

extern unsigned char defFilesRom[defFilesRom_SZ];

struct stFileSystems filesRomFS =
{
	.fsName = "romfs",
	.openDir = romFsOpenDir,
	.readDir = romFsReadDir,
	.closeDir = romFsCloseDir,
	.unlink = NULL,
	.stat = NULL,
    .fsData = defFilesRom
};

int main(void)
{
  unsigned int totalUsed = 0;
  dirEntry dir;
  fileEntry *file;
  struct stFileSystems *fs;
  plt_system_init();
  if(registerFS(&filesRomFS))
  {
	printf("ROM File System registrado\n");
    printf("Archivos encontrados %d\n", romFsGetFileCount(defFilesRom));
    printf("Espacio usado: %d\n", romFsGetUsedSpace(defFilesRom));
    fs = getRegisteredFS(0);
    fs->openDir(fs->fsData, &dir);
    while((file = fs->readDir(&dir)) != 0)
    {
      printf("\t-> %s [%lu]\n", file->fn, file->size);
      totalUsed += file->size;
    }
  }
  else
  {
    printf("Error registrando ROM File System\n");
    fflush(stdout);
  }
  printf("-= Fin =-\n");

  while(1);
  return 0;
}
