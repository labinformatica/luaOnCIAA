/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <chip.h>
#include "pio.h"
#include <module.h>
#include <gpio_drv.h>

//Retorna el numero correspondiente a los datos pasados por LUA
static int index_gd (lua_State *L,unsigned char *dato,int cant)
{
	uint16_t i=1;
	uint8_t o=0;
	dato[0]=0x0;
	dato[3]=0x0;
	dato[4]=0x0;
	dato[5]=0x0;

	int n = lua_gettop(L); //cantidad de argunentos
	if(n==0 || n<cant){
		return luaL_error( L, "Argumento invalido" );
	}
	if(lua_isstring(L,1)){  //P0_0
		int8_t *str = luaL_checklstring(L,1,NULL);
        if(str[0]=='P'||str[0]=='G'){
        	dato[0]=str[0];
        	dato[1]=str[1]-48;
        	dato[2]=str[3]-48;
        	if((str[4]>47) &&(str[4]<58)){
        		dato[2]*=10;
        		dato[2]+=str[4]-48;
        	}
        	o=1;
    		i=2;
        }
	}

	for(;i<=n;i++)
		dato[i+o]=lua_tointegerx(L,i,NULL);
	return 1;
}
//Decodifica la direccion del PIN-PUERTO
static int pin_port(unsigned char *dato,unsigned char *port,unsigned char *pin){
	if (dato[1]>39){
		dato[1]-=40;
		if((dato[1]>=0)&&(dato[1]<10)){
			*port=DISP[dato[1]].port;
			*pin=DISP[dato[1]].offset;
		}else{
			fprintf(stderr,"Error en salida seleccionada\n");
			return 0;
		}
		return 1;
	}else{
		if(dato[0]=='G'){
			if(dato[1]>=0&&dato[1]<6)*port=dato[1];
			else {
				fprintf(stderr,"Error en el puerto seleccionado\n");
			    return 0;
			}
			if(dato[2]>=0&&dato[2]<19)*pin=dato[2];
			else {
				fprintf(stderr,"Error en el pin seleccionado\n");
				return 0;
			}
			return 2;
		}else {
			if(dato[0]=='P'){
				if(dato[1]>9){
					fprintf(stderr,"Error en el puerto seleccionado\n");
				    return 0;
				}
				if(dato[2]>20){
					fprintf(stderr,"Error en el pin seleccionado\n");
					return 0;
				}
				if(PinToR(dato[1],dato[2],port,pin)==0) return 0;
				return 2;
			}else{
					if(dato[1]>5){
						fprintf(stderr,"Error en el puerto seleccionado\n");
					    return 0;
					}
					*port=dato[1];
					if(dato[2]>18){
						fprintf(stderr,"Error en el pin seleccionado\n");
						return 0;
					}
					*pin=dato[2];
					return 2;
			}
		}
	}
	return 0;
}
//-----------------------------------------------------------------------
/*
 * Para el uso de los puertos visto desde el esquematico de la CIAA escribir "P2_0" (para LED_R), en el caso de
 * referirse a los registros GPIO "G5_0" (para LED_R)
 * En el caso de usar los LED o Pulsador de la placa usar los nombres en mayuscula separado por '_'
 * Ejemplo "TEC_1","TEC_2","LED_1","LED_G"
 *
 *
 * gpio.setup(gpio.LED1,gpio.OUTPUT)
 * gpio.setup(gpio.LED1,gpio.IN,gpio.PULLDOWN)
 * gpio.setup(port,pin,gpio.IN)
 * gpio.setup(0,14,gpio.IN,gpio.PULLUP)
 * */
static int pin_c(lua_State *L){              
	uint8_t port,pin,offset=0;
	uint8_t dato_d[6];
	if(index_gd(L,dato_d,2)!=0){
	offset= pin_port(dato_d,&port,&pin);
	if(offset==0) return luaL_error( L, "Error puerto invalido");
	offset-=1;
	if(dato_d[2+offset]==50){
	      switch(dato_d[3+offset]){
	         	 case 0:
	         	 	(void) gpio_IO ( port, pin, PIN_INPUT );
	        	 break;
	         	 case 52:
	      			(void) gpio_IO ( port, pin, PIN_PULLUP );
	         	 break;

	         	 case 53:
	      			(void) gpio_IO ( port, pin, PIN_PULLDOWN);
	         	 break;

	         	 case 54:
	      			(void) gpio_IO ( port, pin, PIN_NOPULL );
	         	 break;
	         	 default:
	         		return luaL_error( L, "Error en PULL");
	         }
	         return 0;
		}else if(dato_d[2+offset]==51){
			(void) gpio_IO ( port, pin, PIN_OUTPUT );
			return 0;
		}else{
     		return luaL_error( L, "Error en la direccion seleccionada");
		}
	}
	return 0;
}
/*
 * gpio.write(gpio.LED1,1)
 * gpio.write(gpio.LED1,0)
 * gpio.write(port,pin,1)
 * gpio.write(0,14,1)
 * */
static int pin_setval(lua_State *L){              
	uint8_t port,pin,offset=0;
	uint8_t dato[5];
    if(index_gd(L,dato,2)!=0){
    	offset= pin_port(dato,&port,&pin);
    	if(offset==0) return luaL_error( L, "Error puerto invalido");
    	offset-=1;
    	switch(dato[2+offset]){
		case 0:
			(void) gpio_IO ( port, pin, PIN_CLEAR );
		break;
		case 1:
			(void) gpio_IO ( port, pin, PIN_SET );
		break;
 		default:
 			return luaL_error( L, "Error en estado del puerto");
		}
    }
	return 0;
}
/*
 * gpio.read(port,pin)
 * gpio.read(gpio.TEC_1)
 */
static int pin_getval(lua_State *L){
	uint8_t port,pin;
	uint8_t dato[5];
	uint16_t i,offset;
    if(index_gd(L,dato,1)!=0){
    	offset= pin_port(dato,&port,&pin);
    	if(offset==0) return luaL_error( L, "Error puerto invalido");
    	offset-=1;
    	i=gpio_IO ( port, pin, PIN_GETVAL );
    	lua_pushinteger(L,i);
    	return 1;
    }
    return 0;
}
/********************************************************************
          Aceeso a puerto
*********************************************************************/
static int port_c(lua_State *L){
	uint8_t dato[5];//port;
  if(index_gd(L,dato,2)!=0){
		if(dato[1]>6){
			fprintf(stderr,"Error en el pin seleccionado\n");
			return 0;
		}
		if(dato[2]==10){
	         switch(dato[3]){
	         	 case 0:
	         	 	(void) gpio_IO ( dato[1], 0xFF, PORT_INPUT );
	        	 break;
	         	 case 12:
	 //     			(void) gpio_IO ( dato[1], pin, PIN_PULLUP );
	         	 break;

	         	 case 13:
	//      			(void) gpio_IO ( dato[1], pin, PIN_PULLDOWN);
	         	 break;

	         	 case 14:
	//      			(void) gpio_IO ( dato[1], pin, PIN_NOPULL );
	         	 break;
	         	 default:
	         		 fprintf(stderr,"Error en PULL");
	         }
	         return 0;
		}else if(dato[2]==11){
			(void) gpio_IO ( dato[1], 0xFF, PORT_OUTPUT );
			return 0;
		}else{
			fprintf(stderr,"Error en la direccion seleccionada\n");
			return 0;
		}

	}
	return 0;
}


static int gpio_index (lua_State *L){
	uint8_t *s = luaL_checkstring( L ,1 );
	uint8_t *s1 = luaL_checkstring( L ,2 );
    if(!s || !s1)
	return 2;
    return 0;
}

/*------------------------------------------------------------*/
//------Definicion de Funciones Metametodos-------------------//
static const LUA_REG_TYPE gpio_tipo[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( pin_c ) },
  { LSTRKEY( "write" ), LFUNCVAL( pin_setval ) },
  { LSTRKEY( "read" ), LFUNCVAL( pin_getval ) },
  { LSTRKEY( "port" ), LFUNCVAL( port_c ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "LED_1"  ), LNUMVAL( 40 ) },
  { LSTRKEY( "LED_2"  ), LNUMVAL( 41 ) },
  { LSTRKEY( "LED_3"  ), LNUMVAL( 42 ) },
  { LSTRKEY( "LED_R"  ), LNUMVAL( 43 ) },
  { LSTRKEY( "LED_G"  ), LNUMVAL( 44 ) },
  { LSTRKEY( "LED_B"  ), LNUMVAL( 45 ) },
  { LSTRKEY( "TEC_1"  ), LNUMVAL( 46 ) },
  { LSTRKEY( "TEC_2"  ), LNUMVAL( 47 ) },
  { LSTRKEY( "TEC_3"  ), LNUMVAL( 48 ) },
  { LSTRKEY( "TEC_4"  ), LNUMVAL( 49 ) },
  { LSTRKEY( "IN"     ), LNUMVAL( 50 ) },
  { LSTRKEY( "OUT"    ), LNUMVAL( 51 ) },
  { LSTRKEY( "PULLUP" ), LNUMVAL( 52 ) },
  { LSTRKEY("PULLDOWN"), LNUMVAL( 53 ) },
  { LSTRKEY( "NOPULL" ), LNUMVAL( 54 ) },
  { LSTRKEY( "LOW"  )  , LNUMVAL( 0 )  },
  { LSTRKEY( "HIGH" )  , LNUMVAL( 1 )  },
  { LSTRKEY( "__metatable" ), LROVAL( gpio_tipo ) },
#endif
  { LSTRKEY( "__index" ), LFUNCVAL( gpio_index ) },
  { LNILKEY, LNILVAL }
};


LUAMOD_API int luaopen_gpio (lua_State *L) {
#ifdef LUA_ROMTABLE
	return 0;
#else
	luaL_newlib(L, gpio_tipo);
	lua_pushnumber(L, 40);
	lua_setfield(L, -2, "LED_1");
	lua_pushnumber(L, 41);
	lua_setfield(L, -2, "LED_2");
	lua_pushnumber(L, 42);
	lua_setfield(L, -2, "LED_3");
	lua_pushnumber(L, 43);
	lua_setfield(L, -2, "LED_R");
	lua_pushnumber(L, 44);
	lua_setfield(L, -2, "LED_G");
	lua_pushnumber(L, 45);
	lua_setfield(L, -2, "LED_B");
	lua_pushnumber(L, 46);
	lua_setfield(L, -2, "TEC_1");
	lua_pushnumber(L, 47);
	lua_setfield(L, -2, "TEC_2");
	lua_pushnumber(L, 48);
	lua_setfield(L, -2, "TEC_3");
	lua_pushnumber(L, 49);
	lua_setfield(L, -2, "TEC_4");
	lua_pushnumber(L, 50);
	lua_setfield(L, -2, "IN");
	lua_pushnumber(L, 51);
	lua_setfield(L, -2, "OUT");
	lua_pushnumber(L, 52);
	lua_setfield(L, -2, "PULLUP");
	lua_pushnumber(L, 53);
	lua_setfield(L, -2, "PULLDOWN");
	lua_pushnumber(L, 54);
	lua_setfield(L, -2, "NOPULL");
	lua_pushnumber(L, 0);
	lua_setfield(L, -2, "LOW");
	lua_pushnumber(L, 1);
	lua_setfield(L, -2, "HIGH");
	return 1;
#endif
}

//MODULE_REGISTER_ROM(PIO, gpio, gpio_tipo, luaopen_gpio, 1)


