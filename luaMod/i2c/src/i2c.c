/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <chip.h>
#include "i2c.h"
#include <i2c_drv.h>
#include <module.h>


// Lua: speed = i2c.setup( mode, port,addrs)
static int i2c_setup(lua_State *L){
  uint16_t mode,port,addr;
  mode =luaL_checkinteger(L,1);
  port =luaL_checkinteger(L,2);

  if(mode > SLAVE){
		return luaL_error( L, "Mode error" );}
  if(port > I2C1){
		return luaL_error( L, "Invalid port" );}
  if (mode == SLAVE && lua_gettop(L) < 3){
	  return luaL_error( L, "Addrs error" );}

  if (lua_gettop( L ) > 2) {	// SLAVE mode
    addr = luaL_checkinteger( L, 3 );
	if (addr < 0 || addr > 127)
		return luaL_error( L, "Invlid Addrs" );
		printf("I2C %d set SLAVE\n", port);
  } else {
	  addr=0;
	  i2c_init(port);
	  set_i2c(port,1,I2C_SLOW);
	  printf("I2C %d set MASTER\n", port);
  }
  return 0;
}
//----------------------------------------------------------------
// Lua: wrote = i2c.write( mode, port, addr, data1, [data2], ..., [datan] )
// data can be either a string, a table or an 8-bit number
static int i2c_write( lua_State *L )
{
  uint16_t mode = luaL_checkinteger( L, 1 );
  uint16_t port = luaL_checkinteger( L, 2 );
  uint16_t addr = luaL_checkinteger( L, 3 );
  const int8_t *pdata;
  unsigned datalen, i;
  int16_t numdata = 0;
  uint16_t len = 0;
  uint16_t argn;
  uint8_t buf[256];
  memset(buf, 0, 256);

  if( lua_gettop( L ) < 4 )
    return luaL_error( L, "Error" );
  for( argn = 4; argn <= lua_gettop( L ); argn ++ )
  {
    if( lua_type( L, argn ) == LUA_TNUMBER )//Es numero?
    {
      numdata = ( int )luaL_checkinteger( L, argn );
      if( numdata < 0 || numdata > 255 )
        return luaL_error( L, "Data out Range" );
	  buf[len] = numdata;
      len ++;
    }
    else if( lua_istable( L, argn ) )
    {
/*      datalen = lua_objlen( L, argn );
      for( i = 0; i < datalen; i ++ )
      {
        lua_rawgeti( L, argn, i + 1 );
        numdata = ( int )luaL_checkinteger( L, -1 );
        lua_pop( L, 1 );
        if( numdata < 0 || numdata > 255 )
          return luaL_error( L, "wrong arg range" );
        buf[len] = numdata;
        len ++;
      }*/
    }
    else
    {
      pdata = luaL_checklstring( L, argn, &datalen );
      for( i = 0; i < datalen; i ++ ) {
	    buf[len] = pdata[i];
        len ++;
	  }
    }
  }
  int ret = i2c_pl_wite(mode,port,(u_char)addr,(u_char *)buf,len);
  if (mode == MASTER && ret == 0)
	printf("Master write error\n");
  lua_pushinteger( L, len );
  return 1;
}

// Lua: read = i2c.read( mode, port, addr, len )
static int i2c_read( lua_State *L )
{
  unsigned mode = luaL_checkinteger( L, 1 );
  unsigned port = luaL_checkinteger( L, 2 );
  if (port != I2C0 && port != I2C1) {
	return luaL_error( L, "Error port" );
  }
  unsigned addr = luaL_checkinteger( L, 3 );
  unsigned len = luaL_checkinteger( L, 4 );
  if (len < 0) {
	return luaL_error( L, "Error leng" );
  } else if (len == 0) {
	luaL_pushresult( 0 );
	return 1;
  }
  luaL_Buffer b;

  luaL_buffinit( L, &b );
  uint8_t data[1024] = {0};
  int r_size = i2c_pl_read( mode, port, (u_char)addr,(u_char *) data, len);
  if (mode == MASTER && r_size == 0) {
	printf("Master read error\n");
  }
  luaL_addlstring(&b,data, strlen(data));
  luaL_pushresult( &b );
  return 1;
}

// Module function map
static const LUA_REG_TYPE i2c_map[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( i2c_setup ) },
  { LSTRKEY( "write" ), LFUNCVAL( i2c_write ) },
  { LSTRKEY( "read" ), LFUNCVAL( i2c_read ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "MASTER"  ), LNUMVAL( 0 ) },
  { LSTRKEY( "SLAVE"  ), LNUMVAL( 1 ) },
  { LSTRKEY( "I2C_0"  ), LNUMVAL( 0 ) },
  { LSTRKEY( "I2C_1"  ), LNUMVAL( 1 ) },
  { LSTRKEY( "__metatable" ), LROVAL( i2c_map ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};




LUALIB_API int luaopen_i2c(lua_State *L)
{
#ifdef LUA_ROMTABLE
	return 0;
#else
	luaL_newlib(L, i2c_map);
	lua_pushnumber(L, MASTER);
	lua_setfield(L, -2, "MASTER");
	lua_pushnumber(L, SLAVE);
	lua_setfield(L, -2, "SLAVE");
	lua_pushnumber(L, I2C0);
	lua_setfield(L, -2, "I2C_0");
	lua_pushnumber(L, I2C1);
	lua_setfield(L, -2, "I2C_1");
	return 1;
#endif
}










