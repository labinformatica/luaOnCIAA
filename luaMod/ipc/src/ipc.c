/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <chip.h>
#include "lpc4337_ipc.h"
#include "ipc.h"
#include <module.h>

//----Define-------------------------------

#define puerto 0
#define pin 1

const uint8_t direccion [6][2]={
		{0,14},
		{1,11},
		{1,12},
		{5,0},
		{5,1},
		{5,2}
};



//ipc.conmutar(led)
static int ipc_conmutar(lua_State *L)
{
	tipo_led led;

	led=luaL_checkinteger(L,1);

	if(led<0 || led>5) return luaL_error(L,"Led invalido");

	conmutar_led_m0(direccion[led][puerto], direccion[led][pin]);

	return 0;
}


//ipc.estado(led)
static int ipc_estado(lua_State*L)
{
	tipo_led led;
	bool ok;

	bool hayCola = verificar_cola_m0();

	if(hayCola == false)
	{
		printf("No se inicializó la cola, se iniciará automaticamente ahora\n");
		inicializar_cola_m0();
	}
	led=luaL_checkinteger(L,1);

	if(led<0 || led>5) return luaL_error(L,"Led invalido");

	ok = estado_led_m0(direccion[led][puerto],direccion[led][pin]);

	if(ok)
		printf("LED encendido\n");
	else
		printf("LED apagado\n");

	return 0;

}


//ipc.iniciar()
static int ipc_init(lua_State*L)
{
	inicializar_cola_m0();
	return 0;
}

//ipc.pendientes()
static int ipc_mensajes_pendientes(lua_State*L)
{
	int cant = mensajes_pendientes_m0();

	if(cant == -3) return luaL_error(L,"No se inicializó la cola\n");

	else if (cant == 0)
		printf("No hay mensajes pendientes\n");
	else
		printf("Hay %u mensajes pendientes\n",cant);

	return 0;

}

//ipc.pop()
//static int ipc_pop(lua_State*L)
//{
////	hacer_pop();  //la accion de esta funcion esta comentada porque sirve para debug
//	return 0;
//}

//ipc.iniciar_contador()
static int iniciar_contador(lua_State*L)
{
	iniciar_contador_m0();
	return 0;
}

//ipc.valor_contador()
static int valor_contador(lua_State*L)
{
	int cont = valor_contador_m0();

//	cont = valor_contador_m0();

	printf("El valor del contador es: %u\n", cont);

	return 0;
}

//ipc.pausar_contador()
static int pausar_contador(lua_State *L)
{
	pausar_contador_m0();
	return 0;
}

//ipc.reiniciar_contador()
static int reiniciar_contador(lua_State*L)
{

	reiniciar_contador_m0();
	return 0;
}

//ipc.restaurar_contador()
static int restaurar_contador(lua_State*L)
{
	restaurar_contador_m0();
	return 0;
}

////ipc.ayuda() ipc.help()
//static int ipc_help(lua_State*L)
//{
//	printf("El modulo IPC cuenta con las siguientes\n\r");
//	fflush(stdout);
//	printf("funciones:\n\r");
//	fflush(stdout);
//	printf("\t \"conmutar(LEDn)\" Función utilizada\n\r");
//	printf("para conmutar el estado del LEDn.\n");
//	printf("\t \"estado(LEDn)\" Esta función se\n");
//	printf("utiliza para saber el estado del LEDn.\n");
//	printf("\t \"inicio()\" Esta función se utiliza\n");
//	printf("para iniciar la cola IPC.\n");
//	printf("\t \"pendientes()\" Función que devuelve\n");
//	printf("la cantidad de mensajes sin leer.\n");
//	printf("\t \"pop()\" Esta función se utiliza\n");
//	printf("para quitar un mensaje de la cola IPC.\n");
//	printf("\t \"help()\" o \"ayuda()\" Despliega\n");
//	printf("este menú de ayuda.\n");
//
//}

//mapa de funciones y constantes

static const LUA_REG_TYPE ipc_map[] =
{
  { LSTRKEY( "conmutar" ), LFUNCVAL ( ipc_conmutar ) },
  { LSTRKEY( "estado" ), LFUNCVAL( ipc_estado ) },
  { LSTRKEY( "inicio" ), LFUNCVAL( ipc_init ) },
  { LSTRKEY( "pendientes" ), LFUNCVAL( ipc_mensajes_pendientes ) },
  { LSTRKEY( "iniciar_contador" ), LFUNCVAL( iniciar_contador ) },
  { LSTRKEY( "valor_contador" ), LFUNCVAL( valor_contador ) },
  { LSTRKEY( "pausar_contador" ), LFUNCVAL( pausar_contador ) },
  { LSTRKEY( "reiniciar_contador" ), LFUNCVAL( reiniciar_contador ) },
  { LSTRKEY( "restaurar_contador" ), LFUNCVAL( restaurar_contador ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY("LED1"), LNUMVAL(LED1) },
  { LSTRKEY("LED2"), LNUMVAL(LED2) },
  { LSTRKEY("LED3"), LNUMVAL(LED3) },
  { LSTRKEY("LEDR"), LNUMVAL(LEDR) },
  { LSTRKEY("LEDG"), LNUMVAL(LEDG) },
  { LSTRKEY("LEDB"), LNUMVAL(LEDB) },
  { LSTRKEY( "__metatable" ), LROVAL( ipc_map ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};



//Funcion api

LUAMOD_API int luaopen_ipc(lua_State *L)
{
#ifdef LUA_ROMTABLE
	return 0;
#else
	luaL_newlib(L,ipc_map);

	lua_pushnumber(L,LED1);
	lua_setfield(L,-2,"LED1");

	lua_pushnumber(L,LED2);
	lua_setfield(L,-2,"LED2");

	lua_pushnumber(L,LED3);
	lua_setfield(L,-2,"LED3");

	lua_pushnumber(L,LEDR);
	lua_setfield(L,-2,"LEDR");

	lua_pushnumber(L,LEDG);
	lua_setfield(L,-2,"LEDG");

	lua_pushnumber(L,LEDB);
	lua_setfield(L,-2,"LEDB");
	return 1;
#endif
}




