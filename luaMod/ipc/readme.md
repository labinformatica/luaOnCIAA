# Soporte IPC en LUA

Mediante este modulo se da soporte a la librería IPC desde LUA

## Estructura del módulo

Los elementos que componen todo el árbol de directorios para este
módulo se observan a continuación:

```mermaid
graph TD;
A("ipc");
  A-->B("inc");
  B-->F("ipc.h")
  B-->G("lpc4337_ipc.h");
  A-->C("src");
  C-->H("ipc.c");
  C-->I("lpc4337_ipc.c");
  A-->D("makefile");
  A-->E("readme.md");
```
### Contenido de cada archivo

* **ipc.h**
En este archivo se encuentra la definicion de un enum luego utilizado
como constante en LUA.

* **ipc.c**
Acá encontramos todas las funciones que se ejecutarán desde LUA. Estas
funciones estan relacionadas con sus nombres debido a la tabla _ipc___map[]_ 
y luego agregadas al espacio de trabajo de LUA gracias a _luaopen___ipc_.

* **lpc4337_ipc.h**
En este archivo encontramos las definiciones de tipos de datos para armar
los mensajes y los prototipos de las funciones.

* **lpc4337_ipc.c**
En este archivo encontramos el los cuerpos de las funciones, tanto las que 
se ejecutan en el Cortex M4 como las que se ejecutan en el Cortex M0.
A estas funciones llaman las funciones del archivo _ipc.c_ y son estas funciones
las que hacen las modificaciones sobre el hardware.
Es necesario que en archivo _main.c_ a ejecutar se cree el vector de funciones
que se llame cuando se ejecute la interrupcion _M0APP_IRQn_.
Este vector de funciones tendrá la siguiente forma

```C
void (*funcion[])(tipo_dir)={
		conmutar_led_m4, 					//0
		estado_led_m4, 						//1
		iniciar_contador_m4,				//2
		valor_contador_m4,					//3
		pausar_contador_m4,					//4
		reiniciar_contador_m4,				//5
		restaurar_contador_m4				//6
};
```

De esta forma cuando se encola un nuevo mensaje el M4 solo indexa este vector
con el ID de la función que le llegó en el mensaje desde el M0


## Funciones y constantes del módulo

Las funciones que contiene este módulo son las siguientes:

1. **conmutar(LEDn)**
	Con esta función se indica al M4 que modifique el estado del led LEDn.
2. **estado(LEDn)**
	Con esta función se consulta al M4 el estado del LEDn.
3. **inicio()**
	Con esta función se inicia la cola IPC del core M0.
	{-Función en desuso-}
4. **pendientes()**
	Con esta función es posible saber cuantos mensajes pendientes hay
	en la cola del M0
	{+De estar todo OK, no debería existir ningún mensaje pendiente+}
5. **iniciar_contador()**
	Esta función inicia un contador que es incrementado por el core M4.
	Se muestra a través de este contador que la comunicación entre procesadores
	puede servir para comunicar datos en el entorno de ejecución del otro core. 😋
6.	**valor_contador()**
	Con esta función se puede saber el valor actual del contador.
7. **pausar_contador()**
	Esta función, como lo dice su nombre, sirve para pausar la cuenta del contador.
8. **reiniciar_contador()**
	Con esta función se pone en marcha nuevamente la cuenta luego de una pausa.
9. **restaurar_contador()**
	Haciendo uso de esta función se vuelve el contador a cero y se comienza
	la cuenta nuevamene.

Las constantes que contiene este módulo son las siguientes:

- **LED1** o **led1** Se usa para actuar sobre el _LED1_ de la EDU-CIAA
- **LED2** o **led2** Se usa para actuar sobre el _LED2_ de la EDU-CIAA
- **LED3** o **led3** Se usa para actuar sobre el _LED3_ de la EDU-CIAA
- **LEDR** o **ledr** Se usa para actuar sobre el _LEDR_ de la EDU-CIAA
- **LEDG** o **ledg** Se usa para actuar sobre el _LEDG_ de la EDU-CIAA
- **LEDB** o **ledb** Se usa para actuar sobre el _LEDB_ de la EDU-CIAA

## Uso del módulo

Para usar las funciones de este módulo se debe **siempre** anteponer
el nombre del módulo con un punto. Como se observa a continuación:

>>>
ipc.conmutar(LED2)
>>>

Ejecutando eso en el interprete LUA se modificará el estado del LED2 de la EDU-CIAA

