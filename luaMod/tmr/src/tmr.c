/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <chip.h>
#include "tmr.h"
#include <module.h>
#include <tmr_drv.h>

static uint8_t itmr[4]={0,0,0,0};

//tmr.setup(periodo, id)
static int tmr_stup (lua_State *L ){
	uint16_t id_t=0, i;
	uint16_t per = luaL_checkinteger( L, 1 );
	uint16_t id  = luaL_checkinteger( L, 2 );
	for(i=0;i<4;i++){
		if(itmr[i]==0)break;
		if(i==3) {
			luaL_error( L, "Error, no hay timer disponible" );
			return 0;
		}
	}
	if((per==0) || (id == 0)){
		luaL_error( L, "Error en el periodo" );
		return 0;
	}





	return id_t;
}


static int tmr_start (lua_State *L ){




	return 0;
}

static int tmr_stop (lua_State *L ){




	return 0;
}
//retatdo en ms
static int tmr_delay(lua_State *L ){
	uint16_t per;
	uint64_t t;
	per = luaL_checkinteger( L, 1 );
	if(per==0){
		luaL_error( L, "Error en el periodo" );
		return 0;
	}
	t=getTick();
	while((getTick()-t)<=per){};
	return 0;

}




static const LUA_REG_TYPE tmr_map[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( tmr_stup ) },
  { LSTRKEY( "start" ), LFUNCVAL( tmr_start ) },
  { LSTRKEY( "stop" ), LFUNCVAL( tmr_stop ) },
  { LSTRKEY( "delay" ), LFUNCVAL( tmr_delay ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "MASTER"  ), LNUMVAL( 0 ) },
  { LSTRKEY( "SLAVE"  ), LNUMVAL( 1 ) },
  { LSTRKEY( "I2C_0"  ), LNUMVAL( 0 ) },
  { LSTRKEY( "I2C_1"  ), LNUMVAL( 1 ) },
  { LSTRKEY( "__metatable" ), LROVAL( tmr_map ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};




LUALIB_API int luaopen_tmr(lua_State *L)
{
#ifdef LUA_ROMTABLE
	return 0;
#else
	luaL_newlib(L, tmr_map);
/*	lua_pushnumber(L, MASTER);
	lua_setfield(L, -2, "MASTER");
	lua_pushnumber(L, SLAVE);
	lua_setfield(L, -2, "SLAVE");
	lua_pushnumber(L, I2C0);
	lua_setfield(L, -2, "I2C_0");
	lua_pushnumber(L, I2C1);
	lua_setfield(L, -2, "I2C_1");*/
	return 1;
#endif
}
