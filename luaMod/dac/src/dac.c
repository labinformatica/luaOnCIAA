/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <chip.h>
#include <module.h>
#include "dac.h"
#include "dac_drv.h"

//----Define-------------------------------

/*
 * Habilitar DAC: dac.Config(ENABLE).
 * Deshabilitar DAC: dac.Config(DISABLE).
 */
static int dac_Config(lua_State *L){

	unsigned char state;

	state = luaL_checkinteger(L,1);

	CONF_dac(state);

	return 0;
}

/*
 * Escribir DAC: dac.WriteValue(512) - Valor de 512 en el DAC.
 */
static int dac_WriteValue(lua_State *L){

	uint16_t value;

	value = luaL_checkinteger(L,1);

	WRITE_dac(value);

	return 0;
}

static const LUA_REG_TYPE dac_tipo[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( dac_Config ) },
  { LSTRKEY( "write" ), LFUNCVAL( dac_WriteValue ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "DISABLE"  ), LNUMVAL( DISABLE ) },
  { LSTRKEY( "ENABLE"  ), LNUMVAL( ENABLE ) },
  { LSTRKEY( "__metatable" ), LROVAL( dac_tipo ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};

LUAMOD_API int luaopen_dac (lua_State *L) {
#ifdef LUA_ROMTABLE
	return 0;
#else
  luaL_newlib(L, dac_tipo);
  lua_pushnumber( L, DISABLE );
  lua_setfield( L, -2, "DISABLE" );
  lua_pushnumber( L, ENABLE );
  lua_setfield( L, -2, "ENABLE" );
  return 1;
#endif
}

