/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <chip.h>
#include "spi.h"
#include <module.h>
#include <gpio_drv.h>
#include <spi_drv.h>
#include <spi_memory_drv.h>
#include <stdlib.h>


struct SPI_STRUCT{
	uint8_t ID;
	uint8_t pin;
	uint8_t port;
	uint8_t uint;
	SPI_conf spiconf;
};

typedef struct SPI_STRUCT spi_struct;

  spi_struct *spi[DEVICE];
/*
 * LA inicializacion de los pines se realiza al arranque del firmware en "spi_memory_drv.c"
 *
 *  Lua: clock = setup(pin, MASTER/SLAVE, clock, cpol, cpha, databits )
 *
 */
static int spi_setup( lua_State* L )
{

  int8_t *str;
  uint16_t mode,pin,bit;
  uint32_t speed;
  uint8_t dato[3],i=0,nmode,pos;
  int16_t n = lua_gettop(L); //cantidad de argunentos
  if(n<5) return luaL_error( L, "Error, configuracion invalida");

  if(lua_isstring(L,1)){
	  dato[1]=0xFF;
	  str = luaL_checklstring(L,1,NULL);
      if(str[0]=='P'||str[0]=='G'){
      	dato[0]=str[0];
      	if((str[1]>47) &&(str[1]<58)){ 		//Verifica que el numero sea valido en ASCII
      		dato[1]=str[1]-48;
      		if((str[3]>47) &&(str[3]<58)){
      			dato[2]=str[3]-48;
      			if((str[4]>47) &&(str[4]<58)){
      				dato[2]*=10;
      				dato[2]+=str[4]-48;
      			}//En el caso q no corresponda a un pin, se procesa la cadena entera

      			if(dato[0]=='G'){
      				if(dato[1]>5)
      				    return luaL_error(L,"Error en el puerto seleccionado");
      				if(dato[2]>18)
      					return luaL_error(L,"Error en el pin seleccionado");
      			}else{
      				if(dato[0]=='P'){
      					if(dato[1]>9)
      						return luaL_error(L,"Error en el puerto seleccionado");
      					if(dato[2]>20)
      						return luaL_error(L,"Error en el pin seleccionado");
      					if(PinToR(dato[1],dato[2],&dato[1],&dato[2])==0) return luaL_error(L,"Error en Puerto/Pin");
      				}
      			}
      		}
      	}
      }
      if(dato[1]==0xFF){
    	  pin=PIO_SET(str,0,1);
      	  if (pin ==0xFFFF) return luaL_error(L,"Error en Puerto/Pin");
      	  get_pin_port(pin,&dato[1],&dato[2]);
      }
  }else{
	  dato[0]=luaL_checkinteger(L, 1);
	  dato[1]=luaL_checkinteger(L, 2);
	  i=1;
  }

  mode=luaL_checkinteger(L, 2+i);
  if(mode>1)return luaL_error(L,"Error en MASTER/SLAVE");
  speed=luaL_checkinteger(L, 3+i);
  if(speed>40000000)return luaL_error(L,"Error, bitrate excedido");
  if(luaL_checkinteger(L, 4+i)>1)return luaL_error(L,"Error en CPOL");
  if(luaL_checkinteger(L, 5+i)>1)return luaL_error(L,"Error en CPHA");
  if(luaL_checkinteger(L, 4+i)==0){//cpol = 0
	  if(luaL_checkinteger(L, 5+i)==0)nmode=0; //cpha = 0
	  else nmode=1;
  }else{
	  if(luaL_checkinteger(L, 5+i)==0)nmode=2;
	  else nmode=3;
  }
  bit=luaL_checkinteger(L, 6+i);  //databits
  if((bit>3)&&(bit<17)){
	  bit-=1;
  }else return luaL_error(L,"Error en configuracion bit");
/*
 *	Buscamos una posicion libre
 */
  pos=DEVICE+2;
  for(i=0;i<DEVICE;i++){
	  if(spi[i]==NULL){
		  if(pos>DEVICE)pos=i;
	  }
	  else{
		  if(spi[i]->pin==dato[2])
			  if(spi[i]->port==dato[1])
				  return luaL_error(L,"Error PIN/PORT ya configurado");
	  }
  }
  if(pos==DEVICE)return luaL_error(L,"Dispositivos Maximo alcanzado");
  spi[pos] = malloc(sizeof(spi_struct));
  plt_spi_init();
  plt_conf_spi(bit,mode,speed,nmode,&spi[pos]->spiconf);
  spi[pos]->pin=dato[2];
  spi[pos]->port=dato[1];
  spi[pos]->ID=pos;
  if(gpio_IO(spi[pos]->port,spi[pos]->pin,PIN_OUTPUT)==0)return luaL_error(L,"Error PORT/PIN");
  gpio_IO(spi[pos]->port,spi[pos]->pin,PIN_SET);
  lua_pushinteger( L, spi[pos]->ID );

  return 1;
}



/*************************************************************************/
// Lua: spi_del( id ) //elimina un SS de la lista
static int spi_del ( lua_State* L )
{	uint8_t id;
	if(lua_gettop(L)==0)return luaL_error(L,"Error en el argumento");
	id=luaL_checkinteger(L, 1);
	if(id>DEVICE)return luaL_error(L,"Error ID fuera rango");
	if(spi[id]==NULL)return luaL_error(L,"Posicion Vacia");
	else
	{
		free(spi[id]);
		spi[id]=NULL;
	}
	return 0;
}
/**********************************************************************/
// Helper function: generic write/readwrite

static int spi_help( lua_State *L )
{
  printf("Ejemplo de uso SPI\nid=spi.setup(pin, spi.MASTER, baudios, spi.CPOL_0, spi.CPHA_0, bit)\n");
  printf("id=spi.setup(\"GPIO0\", spi.MASTER, 1000000, spi.CPOL_0, spi.CPHA_0, 8)\n");
  printf("Para eliminar un dispositivo: spi.del(id)\n");
  printf("datos=spi.read(id, cantidad datos a leer)/nspi.write(id,dato1,dato2,..)\nLectura-Escritura read_dato=rw(id,send_dato,cantidad datos a leer)");
  return 0;
}
/****************************************************************/
// read( id, cant datos a leer )
static int spi_read1 (lua_State* L)
{
	uint8_t id, *buff;
	uint16_t count;
	uint16_t ret=0;
	luaL_Buffer b;
	luaL_buffinit( L, &b );

	if(lua_gettop(L)<2) return luaL_error(L,"Error en los parametros"); //cantidad de argunentos
	id= luaL_checkinteger( L, 1 );
	count=luaL_checkinteger( L, 2 );
	if(count>256)return luaL_error(L,"Error, cantidad de datos excedida");
	buff=calloc(count,sizeof(uint8_t));
	if(id<DEVICE){
		if(spi[id]!=NULL){
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_CLEAR);
			ret=plt_spi_read(&spi[id]->spiconf,buff,count);
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_SET);
		}else return luaL_error(L,"Error en ID");
	}
	luaL_addlstring(&b,buff, ret);
	luaL_pushresult( &b );
	free(buff);
	return 1;
}



// write( id, out1, out2, ... )
static int spi_write1( lua_State* L )
{
	uint8_t id, *buff;
	uint16_t ret=0,argn;
	uint16_t len,data;
	int16_t n=lua_gettop(L);

	if(n < 2) return luaL_error(L,"Error en los parametros"); //cantidad de argunentos
	buff=calloc(n-1,sizeof(uint8_t));
	id= luaL_checkinteger( L, 1 );

	for( argn = 1; argn <= n ; argn ++ )
	{
		if( lua_type( L, argn ) == LUA_TNUMBER )//Es numero?
		{
			data = ( uint16_t )luaL_checkinteger( L, argn );
			if( data < 0 || data > 255 )
				return luaL_error( L, "Dato fuera de rango" );
			buff[len] = data;
			len ++;
		}
	}
	if(id<DEVICE){
		if(spi[id]!=NULL){
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_CLEAR);
			ret=plt_spi_write(&spi[id]->spiconf,buff,len);
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_SET);
		}else return luaL_error(L,"Error en ID");
	}
	lua_pushinteger( L, ret );
	free(buff);
	return 1;
}

// read_dato = readwrite( id, out1, out2, ...,datos a leer )
static int spi_readwrite( lua_State* L )
{
	uint8_t id,data, *txbuff,*rxbuff;
	uint16_t count,n,argn;
	uint16_t ret=0,len;
	luaL_Buffer b;
	luaL_buffinit( L, &b );
/**********************************************************************/
	id= luaL_checkinteger( L, 1 ); //Retorna la Cantidad de datos pasados por el stack
	if(n < 3) return luaL_error(L,"Error en los parametros"); //cantidad de argunentos

	count=luaL_checkinteger( L, n );
	if(count>256)return luaL_error(L,"Error, cantidad de datos excedida");

//Extrae del stack los datos a enviar
	txbuff=calloc(n-2,sizeof(uint8_t));
	rxbuff=calloc(count,sizeof(uint8_t));
	for( argn = 1; argn <= n-1 ; argn ++ )
	{
		if( lua_type( L, argn ) == LUA_TNUMBER )//Es numero?
		{
			data = ( uint16_t )luaL_checkinteger( L, argn );
			if( data < 0 || data > 255 )
				return luaL_error( L, "Datos fuera Rango" );
			txbuff[len] = data;
			len ++;
		}
	}

	if(id<DEVICE){
		if(spi[id]!=NULL){
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_CLEAR);
			plt_spi_write(&spi[id]->spiconf,txbuff,len);
			ret=plt_spi_read(&spi[id]->spiconf,rxbuff,count);
			gpio_IO(spi[id]->port,spi[id]->pin,PIN_SET);
		}else return luaL_error(L,"Error en ID");
	}
	luaL_addlstring(&b,rxbuff, ret);
	luaL_pushresult( &b );
	free(txbuff);
	free(rxbuff);


  return 0;
}
static int spi_getid( lua_State *L)
{
	uint8_t buf[3];
	get_ID(buf);
	lua_pushnumber(L, buf[0]);
	lua_pushnumber(L, buf[1]);
	lua_pushnumber(L, buf[2]);
	return 3;
}

static const LUA_REG_TYPE spi_map[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( spi_setup    ) },
  { LSTRKEY( "del"   ), LFUNCVAL ( spi_del      ) },
  { LSTRKEY( "read"  ), LFUNCVAL ( spi_read1     ) },
  { LSTRKEY( "write" ), LFUNCVAL ( spi_write1    ) },
  { LSTRKEY( "rw"    ), LFUNCVAL ( spi_readwrite) },
  { LSTRKEY( "help"  ), LFUNCVAL ( spi_help     ) },
  { LSTRKEY( "getid" ), LFUNCVAL ( spi_getid    ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "MASTER"  ), LNUMVAL( MASTER ) },
  { LSTRKEY( "SLAVE"   ), LNUMVAL( SLAVE  ) },
  { LSTRKEY( "CPHA_0"  ), LNUMVAL( 0      ) },
  { LSTRKEY( "CPHA_1"  ), LNUMVAL( 1      ) },
  { LSTRKEY( "CPOL_0"  ), LNUMVAL( 0      ) },
  { LSTRKEY( "CPOL_1"  ), LNUMVAL( 1      ) },
  { LSTRKEY( "__metatable" ), LROVAL( spi_map ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};

LUALIB_API int luaopen_spi(lua_State *L)
{
#ifdef LUA_ROMTABLE
	return 0;
#else
	luaL_newlib(L, spi_map);
	lua_pushnumber(L, MASTER);
	lua_setfield(L, -2, "MASTER");
	lua_pushnumber(L, SLAVE);
	lua_setfield(L, -2, "SLAVE");
	lua_pushnumber(L, 0);
	lua_setfield(L, -2, "CPHA_0");
	lua_pushnumber(L, 1);
	lua_setfield(L, -2, "CPHA_1");
	lua_pushnumber(L, 0);
	lua_setfield(L, -2, "CPOL_0");
	lua_pushnumber(L, 1);
	lua_setfield(L, -2, "CPOL_1");
	return 1;
#endif
}
