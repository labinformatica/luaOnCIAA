/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <stdio.h>
#include <string.h>
#include <chip.h>
#include <module.h>
#include "adc.h"
#include "adc_drv.h"

//----Define-------------------------------

#define PIO_OUTPUT      0

/*
 * Config y Habilitar ADC 0: adc.setup(0,ENABLE).
 * Config y Deshabilitar ADC 0: adc.setup(0,DISABLE).
 */
static int adc_Config(lua_State *L){

	unsigned char adc, state;

	adc = luaL_checkinteger(L,1);
	state = luaL_checkinteger(L,2);

	(void) CONF_adcCHNL(adc,state);

	return 0;
}

/*
 * Config rate y resol ADC 0: adc.configrate(0, 200000, 8) - 200kS/s y 8 bits.
 */
static int adc_ConfigRate(lua_State *L){

	unsigned char adc, resol;
	uint32_t rate;

	adc = luaL_checkinteger(L,1);
	rate = luaL_checkinteger(L,2);
	resol = luaL_checkinteger(L,3);

	(void) CONF_adcRATE (adc, rate, resol);

	return 0;
}

/*
 * Leer valor ADC 0 - CHANNEL 1: adc.read(0, 1).
 */
static int adc_ReadValue(lua_State *L){

	unsigned char adc;
	uint8_t channel;
	uint16_t data;

	adc = luaL_checkinteger(L,1);
	channel = luaL_checkinteger(L,2);

	data = READ_adcVAL (adc, channel);

	lua_pushinteger( L, data);

	return 1;
}
static const LUA_REG_TYPE adc_tipo[] =
{
  { LSTRKEY( "setup" ), LFUNCVAL ( adc_Config ) },
  { LSTRKEY( "configrate" ), LFUNCVAL( adc_ConfigRate ) },
  { LSTRKEY( "read" ), LFUNCVAL( adc_ReadValue ) },
#ifdef LUA_ROMTABLE
  { LSTRKEY( "ENABLE"  ), LNUMVAL( ENABLE ) },
  { LSTRKEY( "DISABLE"  ), LNUMVAL( DISABLE ) },
  { LSTRKEY( "ADC_1"  ), LNUMVAL( 0 ) },
  { LSTRKEY( "ADC_2"  ), LNUMVAL( 1) },
  { LSTRKEY( "ADC_3"  ), LNUMVAL( 2) },
  { LSTRKEY( "__metatable" ), LROVAL( adc_tipo ) },
#endif
  { LSTRKEY( "__index" ), LNILVAL },
  { LNILKEY, LNILVAL }
};


LUAMOD_API int luaopen_adc (lua_State *L) {

#ifdef LUA_ROMTABLE
	return 0;
#else
  luaL_newlib(L, adc_tipo);
  lua_pushnumber( L, DISABLE );
  lua_setfield( L, -2, "DISABLE" );
  lua_pushnumber( L, ENABLE );
  lua_setfield( L, -2, "ENABLE" );
  lua_pushnumber( L, 0 );
  lua_setfield( L, -2, "ADC_1" );
  lua_pushnumber( L, 1 );
  lua_setfield( L, -2, "ADC_2" );
  lua_pushnumber( L, 2 );
  lua_setfield( L, -2, "ADC_3" );
  return 1;
#endif
}

