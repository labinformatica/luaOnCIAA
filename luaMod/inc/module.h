/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef modules_h
#define modules_h

//Definiciones de la inclusion de los modulos 
#include <modules.h>
#define LUA_GPIO	"gpio"
#define LUA_TMR		"tmr"
#define LUA_I2C		"i2c"
#define LUA_SPI		"spi"
#define LUA_ADC		"adc"
#define LUA_DAC		"dac"
#define LUA_IPC		"ipc"


LUAMOD_API int (luaopen_gpio) (lua_State *L) ;
LUAMOD_API int (luaopen_tmr ) (lua_State *L) ;
LUAMOD_API int (luaopen_i2c ) (lua_State *L) ;
LUAMOD_API int (luaopen_spi ) (lua_State *L) ;
LUAMOD_API int (luaopen_adc ) (lua_State *L) ;
LUAMOD_API int (luaopen_dac ) (lua_State *L) ;
LUAMOD_API int (luaopen_ipc ) (lua_State *L) ;

//A continuación se agrega un "template" de la definicion que se debe
//utilizar para agregar un nuevo modulo. Se tiene que modificar
//"LUA_TEMPLATELIBNAME", "template", "luaopen_template"y "DEFtemplate

//#ifdef "DEFtemplate"
//#define LUA_TEMPLATELIBNAME	"template"
//LUAMOD_API int (luaopen_template) (lua_State *L);
//#endif


#endif
