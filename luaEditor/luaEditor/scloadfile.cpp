/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "maindlg.h"
#include "ui_maindlg.h"
#include "scloadfile.h"
#include <QBuffer>


scLoadFile::scLoadFile(mainDlg *dlg, QString fileName):serialCmd(dlg)
{
    this->fileName = fileName;
}

QByteArray scLoadFile::dataToSend(void)
{
    QByteArray toSend;
    sendCmd = "cat " + this->fileName +" \r\n";
    toSend = sendCmd.toUtf8();
    return toSend;
}

void scLoadFile::processResponse(QByteArray data)
{  

  stFileInfo * fileInfo = new stFileInfo;
  QBuffer *ioBuff = new QBuffer();
  data.remove(0, sendCmd.length());
  ioBuff->open(QIODevice::ReadWrite);
  ioBuff->write(data);
  ioBuff->seek(0);
  this->mainWnd()->addNewSrcTab();
  this->mainWnd()->ui->srcTabs->setCurrentIndex(this->mainWnd()->ui->srcTabs->count()-1);
  this->mainWnd()->ui->srcTabs->setTabText(this->mainWnd()->ui->srcTabs->count()-1, this->fileName);

  ((QsciScintilla *)this->mainWnd()->ui->srcTabs->currentWidget())->read(ioBuff);
  ((QsciScintilla *)this->mainWnd()->ui->srcTabs->currentWidget())->setModified(false);
  ioBuff->close();
  delete ioBuff;
  fileInfo->fileName = this->fileName;
  fileInfo->isLocal = false;
  fileInfo->isSaved = true;
  this->mainWnd()->filesInfo.push_back(fileInfo);
}

scLoadFile::~scLoadFile(void)
{

}
