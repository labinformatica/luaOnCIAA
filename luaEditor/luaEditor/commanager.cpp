/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "commanager.h"
#include <string.h>
#include <QMessageBox>

void comManager::executeCmd(void)
{
    if(serPort->isOpen())
    {
        state = stRunning;        
        serPort->write(cmdList.head()->dataToSend());
    }
}

void comManager::dataReady()
{
    QByteArray response;
    serialCmd * cmd;
    int eotPos;
    while(serPort->bytesAvailable() > 0)
      recivedData = recivedData + serPort->readAll();    
    eotPos = recivedData.indexOf(prompt1);
    if(eotPos != -1)
    {
        response = recivedData.left(eotPos);
        recivedData.remove(0, eotPos + prompt1.length());
        cmdList.head()->processResponse(response);
        cmd = cmdList.dequeue();
        delete cmd;
        if(cmdList.count() > 0)
             executeCmd();
        else
             state = stIdle;
    }
}

void comManager::dReadyPromp(void)
{
    switch(pS)
    {
      case psNone:
        pS = psOne;
        prompt1 = serPort->readAll();
        prompt1.remove("\n");
        prompt1.remove("\r");
        serPort->write("\r\n");
        break;
      case psOne:
        pS = psTwo;
        prompt2 = serPort->readAll();
        prompt2.remove("\n");
        prompt2.remove("\r");
        break;
      default:
        break;
    }
    if (pS == psTwo)
    {
        if(prompt1 == prompt2)
        {
          disconnect(serPort, SIGNAL(readyRead()), this, SLOT(dReadyPromp()));
          connect(serPort, SIGNAL(readyRead()), this, SLOT(dataReady()));
        }
        else
          QMessageBox::critical(0,"Error", "No se ha detectado un prompt adecuado");
    }
}

comManager::comManager(QObject *parent) :
    QObject(parent)
{
  serPort = new QextSerialPort(QextSerialPort::EventDriven);
  state = stIdle;
  recivedData.clear();
}

comManager::~comManager(void)
{
  if(serPort->isOpen())
      serPort->close();  
  delete serPort;  
}

bool comManager::openDevice(QString port)
{
    bool ret = false;
    serialCmd * cmd;
    if(serPort->isOpen())
        serPort->close();
    while(!cmdList.empty())
    {
        cmd = cmdList.dequeue();
        delete cmd;
    }
    serPort->setPortName(port);
    serPort->setBaudRate(BAUD115200);
    serPort->setDataBits(DATA_8);
    serPort->setFlowControl(FLOW_OFF);
    serPort->setParity(PAR_NONE);
    serPort->setStopBits(STOP_1);
    serPort->open(QIODevice::ReadWrite);        
    ret = serPort->isOpen();
    if (ret)
    {
        pS = psNone;
        connect(serPort, SIGNAL(readyRead()), this, SLOT(dReadyPromp()));
        serPort->write("\r\n");
    }
    return ret;
}

void comManager::closeDevice(void)
{
    disconnect(serPort, SIGNAL(readyRead()), this, SLOT(dataReady()));
    serPort->close();
    state = stIdle;
    while(!cmdList.empty())
        delete cmdList.dequeue();
}

bool comManager::isConnected(void)
{
    return serPort->isOpen();
}

void comManager::addCmd(serialCmd *cmd)
{
    cmdList.enqueue(cmd);
    if(state == stIdle)
        executeCmd();
}
