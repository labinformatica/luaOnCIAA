/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef MAINDLG_H
#define MAINDLG_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QVector>
#include "Qsci/qscilexerlua.h"
#include "Qsci/qsciapis.h"
#include "commanager.h"

struct stFileInfo
{
    QString fileName;
    bool isLocal;
    bool isSaved;
};

namespace Ui {
class mainDlg;
}

class comManager;

class mainDlg : public QMainWindow
{
    Q_OBJECT

public:
    explicit mainDlg(QWidget *parent = 0);
    ~mainDlg();
    Ui::mainDlg *ui;
    void refreshPortList(void);
    void connectSerial(void);
    void disconnectSerial(void);
    void addNewSrcTab(void);
    comManager cM;
    QVector<stFileInfo *> filesInfo;
private slots:
    void on_btnRefresh_clicked();
    void on_btnConnect_clicked();
    void on_btnConsoleSend_clicked();
    void on_btnConsoleClear_clicked();
    void fileItemDblClicked(QTreeWidgetItem * item, int column);
    void on_actSalir_triggered();

    void on_actNuevo_triggered();

private:
    QsciLexerLua *lexer;
    int newFileCounter;
};

#endif // MAINDLG_H
