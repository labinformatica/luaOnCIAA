/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "scgetfilesystems.h"
#include "maindlg.h"
#include "ui_maindlg.h"
#include <QStringList>
#include "scgetfilenames.h"

scGetFileSystems::scGetFileSystems(mainDlg *dlg):serialCmd(dlg)
{

}

QByteArray scGetFileSystems::dataToSend(void)
{
    QByteArray toSend;
    sendCmd = "ls -fs \r\n";
    toSend = sendCmd.toUtf8();
    return toSend;
}

void scGetFileSystems::processResponse(QByteArray data)
{
    int i;
    int pos;
    QList<QTreeWidgetItem *> fs;
    QStringList headersText;
    QTreeWidgetItem *fsEntry;
    data.remove(0, sendCmd.length());
    pos = data.indexOf("\r\n");
    while( pos != -1)
    {
        data.remove(pos, 2);
        pos = data.indexOf("\r\n");
    }
    QStringList lines = QString(data).split("|", QString::SkipEmptyParts);

    this->mainWnd()->ui->fileTree->clear();
    this->mainWnd()->ui->fileTree->setColumnCount(2);
    headersText.append(QObject::tr("Nombre"));
    headersText.append(QObject::tr("Tamaño"));
    this->mainWnd()->ui->fileTree->setHeaderLabels(headersText);
    for(i = 0; i < lines.count(); i++)
    {
        fsEntry = new QTreeWidgetItem(this->mainWnd()->ui->fileTree);
        fsEntry->setIcon(0, QIcon(":/icons/fileSystem"));
        fsEntry->setText(0, lines[i]);
        fs.append(fsEntry);
        this->mainWnd()->cM.addCmd(new scGetFileNames(this->mainWnd(), lines[i]));
    }
    this->mainWnd()->ui->fileTree->expandAll();
}

scGetFileSystems::~scGetFileSystems(void)
{

}
