#-------------------------------------------------
#
# Project created by QtCreator 2018-01-10T18:55:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = luaEditor
TEMPLATE = app

CONFIG += staticlib

INCLUDEPATH +=../QScintilla_gpl-2.10.2/Qt4Qt5
LIBS += -L../QScintilla_gpl-2.10.2/Qt4Qt5
LIBS += -L../QScintilla_gpl-2.10.2/Qt4Qt5/release
LIBS += -L../QScintilla_gpl-2.10.2/Qt4Qt5/debug
LIBS += -lqscintilla2_qt5

include(../qextserialport-1.2rc/src/qextserialport.pri)


SOURCES += main.cpp\
        maindlg.cpp \
    commanager.cpp \
    serialcmd.cpp \
    scconsoleio.cpp \
    scgetfilesystems.cpp \
    scgetfilenames.cpp \
    scloadfile.cpp

HEADERS  += maindlg.h \
    commanager.h \
    serialcmd.h \
    scconsoleio.h \
    scgetfilesystems.h \
    scgetfilenames.h \
    scloadfile.h

FORMS    += maindlg.ui

RESOURCES += \
    icons.qrc
