/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "maindlg.h"
#include "ui_maindlg.h"
#include "Qsci/qscilexerlua.h"
#include "Qsci/qsciapis.h"
#include <qextserialenumerator.h>
#include "scconsoleio.h"
#include "scloadfile.h"
#include "scgetfilesystems.h"
#include <qdebug.h>
#include <QTabBar>

void mainDlg::refreshPortList(void)
{
    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();
    ui->cmbSerialPort->clear();
    foreach(QextPortInfo info, ports)
        ui->cmbSerialPort->addItem(info.physName);
    ui->cmbSerialPort->setCurrentIndex(0);
}

void mainDlg::connectSerial(void)
{
    cM.openDevice(ui->cmbSerialPort->currentText());
    if(cM.isConnected())
        ui->btnConnect->setIcon(QIcon(":/icons/desconectado"));        
    else
        ui->btnConnect->setIcon(QIcon(":/icons/conectado"));        
}

void mainDlg::disconnectSerial(void)
{
    cM.closeDevice();
    if(cM.isConnected())
        ui->btnConnect->setIcon(QIcon(":/icons/desconectado"));
    else
    {
        ui->btnConnect->setIcon(QIcon(":/icons/conectado"));
        ui->fileTree->clear();
    }
}

void mainDlg::addNewSrcTab(void)
{
    QsciScintilla * srcEd = new QsciScintilla(ui->srcTabs);
    // Configuramos el editor
    QFont srcFont("Courier", 12);
    srcFont.setFixedPitch(true);
    srcEd->setFont(srcFont);

    QFontMetrics fontmetrics = QFontMetrics(srcEd->font());
    srcEd->setMarginsFont(srcEd->font());
    srcEd->setMarginWidth(0, fontmetrics.width(QString::number(999)) + 6);
    srcEd->setMarginLineNumbers(0, true);
    srcEd->setMarginsBackgroundColor(QColor("#cccccc"));

    lexer->setDefaultFont(srcEd->font());
    srcEd->setLexer(lexer);
    srcEd->setAutoCompletionThreshold(1);
    srcEd->setAutoCompletionSource(QsciScintilla::AcsAll);

    srcEd->setCaretLineVisible(true);
    srcEd->setCaretLineBackgroundColor(QColor("#ffe4e4"));

    QsciScintilla::FoldStyle state = static_cast<QsciScintilla::FoldStyle>((!srcEd->folding()) * 5);
    if (!state)
        srcEd->foldAll(false);
    srcEd->setFolding(state);
    srcEd->setUtf8(true);
    ui->srcTabs->addTab(srcEd,"");
}

void mainDlg::fileItemDblClicked(QTreeWidgetItem * item, int column)
{
  if(item != 0)
      if(item->parent() != NULL)
          cM.addCmd(new scLoadFile(this, item->parent()->text(0) + "/" + item->text(0)));
}

mainDlg::mainDlg(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mainDlg), cM(this)
{
    ui->setupUi(this);
    connect(ui->fileTree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(fileItemDblClicked(QTreeWidgetItem*,int)));
    refreshPortList();    
    lexer = new QsciLexerLua();
    newFileCounter = 1;
}

mainDlg::~mainDlg()
{    
    int i;
    delete ui;
    for(i = 0; i < filesInfo.count(); i++)
        delete filesInfo[i];
    delete lexer;
}

void mainDlg::on_btnRefresh_clicked()
{
    refreshPortList();
}

void mainDlg::on_btnConnect_clicked()
{
    if(!cM.isConnected())
        connectSerial();
    else
        disconnectSerial();
}


void mainDlg::on_btnConsoleSend_clicked()
{    
    cM.addCmd(new scConsoleIO(this));
}

void mainDlg::on_btnConsoleClear_clicked()
{
    //ui->lstConsole->clear();
    cM.addCmd(new scGetFileSystems(this));
}

void mainDlg::on_actSalir_triggered()
{
    close();
}

void mainDlg::on_actNuevo_triggered()
{
    stFileInfo * fi =  new stFileInfo;
    fi->fileName = "";
    fi->isLocal = true;
    fi->isSaved = false;
    filesInfo.push_back(fi);
    addNewSrcTab();
    ui->srcTabs->setTabText(ui->srcTabs->count()-1, "Nuevo " + QString::number(this->newFileCounter));
    this->newFileCounter++;
    ui->srcTabs->setCurrentIndex(ui->srcTabs->count()-1);

}
