/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "scconsoleio.h"
#include "maindlg.h"
#include "ui_maindlg.h"
#include <QString>

scConsoleIO::scConsoleIO(mainDlg* dlg):serialCmd(dlg)
{
}

QByteArray scConsoleIO::dataToSend(void)
{
  QByteArray toSend;
  sendCmd = this->mainWnd()->ui->leConsole->text() +"\r\n";
  toSend = sendCmd.toUtf8();
  this->mainWnd()->ui->leConsole->clear();
  return toSend;
}

void scConsoleIO::processResponse(QByteArray data)
{
   int i;
   QString line;
   QStringList lines = QString(data).split("\r\n", QString::SkipEmptyParts);
   QListWidgetItem * item;
   for(i = 0; i < lines.count(); i++)
   {
       item = new QListWidgetItem();
       if(i == 0)
       {
         line = "< " + lines[i];
         item->setBackgroundColor(QColor::fromRgb(240, 200, 200));
       }
       else
       {
         line = "> " + lines[i];
         item->setBackgroundColor(QColor::fromRgb(200, 200, 240));
       }
       item->setText(line);
       this->mainWnd()->ui->lstConsole->addItem(item);
   }
   this->mainWnd()->ui->lstConsole->scrollToBottom();
}

scConsoleIO::~scConsoleIO(void)
{

}
