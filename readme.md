# LUA en la CIAA

Este es un proyecto tendiente al desarrollo e implementación de una estructura de 
proyectos que permita el uso del lenguaje script lua (https://www.lua.org/) en la 
CIAA y EDU-CIAA. En muchos aspectos se tomó como referencia el trabajo realizado 
en el proyecto eLUA (http://www.eluaproject.net/), realizando modificaciones acorde
a la plataforma de hardware utilizada.

En este proyecto no se encontrará una única aplicación, en vez de eso, al compilar
el proyecto, obtendrá una carpeta coteniendo un conjunto de librería así como headers
que permiten la utilización de diferentes módulos (por ejemplo sistema de archivos o
acceso a hardware).

Durante el desarrollo del proyecto, con el fin de verificar algunas características 
y a modo de referencia para proyectos futuros, encontrará en la carpeta ejemplos 
casos de uso de algunas funcionalidades.

Más allá del caso particular de un ejemplo, el proyecto tiene las siguientes 
características fundamentales:

1. Permite ejecutar el motor de script de lua en cualquiera de los dos cores de la CIAA
2. Soporta el uso de 2 sistemas de archivos. Uno de solo lectura en memoria de programa y otro 
de lectura y escritura en EEPROM, RAM o Memoria externa.
3. Shell interactivo. Permite realizar operaciones básicas tales como interactuar 
el motor de script o realizar operaciones sobre el sistema de archivos.

## Descripción de la estructura de carpetas

Como se mencionó antes, más que una aplicación, este proyecto involucra el desarrollo de 
múltiples aplicaciones que se dan soporte entre sí. Es por esto que a continuación se
presenta una descripción del contenido de algunas carpetas significativas.

### M4Loader
La mayoría de las aplicaciones desarrolladas para la CIAA y EDU-CIAA han sido 
desarrolladas para ejecutarse tanto el core M4 como en el M0. Pero por defecto el
núcleo M4 se iniciará primero. Por esto se ha incorporado una aplicación que permite
iniciar el núcleo secundario. 
En esta carpeta se tiene el programa que se ejecutará en el núcleo M4 e iniciará
el segundo núcleo. La aplicación enciende y apaga uno de los leds de modo intermitente
a través del núcleo M4, en lo que respecta a la inicialización del segundo núcleo
utiliza una imagen precargada en un dirección específica de flash.
Para cargar esta aplicación en la EDU-CIAA/CIAA, deberá ejecutarse dentro del
directorio raíz del proyecto:

$ make clean

$ make

$ make programLoader
