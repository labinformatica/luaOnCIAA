# IPC

En esta carpeta se encuentran los archivos para el soporte del IPC en el proyecto LuaOnCIAA.

## Lista de tareas

La siguiente es una lista aproximada de las tareas para esta librería:

- [x] Librería de administración de cola.
- [x] Integración con ejemplos.
- [x] Definición de espacio de memoria para la cola.
- [ ] Manejo completo de la cola en el espacio destinado.
    - [x] Estructura de manejo de Colas
    - [ ] Quitar Colas del Stack de los nucleos
- [ ] Manejo de diccionarios de funciones independientes de
 la aplicación