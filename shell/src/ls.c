/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <ls.h>
#include <config.h>
#include <stdio.h>
#include <string.h>
#include <chip.h>
#include <fs.h>

int ls(int argc, char *argv[])
{
  unsigned int i=0;
  struct stFileSystems * fs;
  fileEntry *file;
  dirEntry dir;
  int filesCnt = 0;
  unsigned int usedSize;
  unsigned int maxFs;
  maxFs =  getRegisteredFSCount();
  int sepCnt;
  char * fn;
  /*
   * Solo se tiene la invocación al comando, sin argumentos.
   *
   * */
  if(argc == 1)
  {
    for(i=0; i < maxFs; i++)
    {
      usedSize = 0;
      filesCnt = 0;
      fs = getRegisteredFS(i);
      printf("File System: %s\n", fs->fsName);
      printf("Contenido: \n");
      fs->openDir(fs, &dir);
      while((file = fs->readDir(&dir)) != 0)
      {
    	  if(file->status == FILE_OK)
    		  printf("\t %s [%lu]\n", file->fn, file->size);
    	  else
    		  printf("\t*%s [%lu]\n", file->fn, file->size);
        usedSize += file->size;
	    filesCnt++;
      }
      fs->closeDir(&dir);
      printf("%d archivos (%d bytes usados)\n", filesCnt, usedSize);
    }
    printf("Se utiliza \"*\" para indicar inconsistencias en el sistema de archivos.\n");
  }
  /*
   * Se tiene un argumento. Los argumentos reconocidos son:
   *  - "-fs" para listar solo los nombres de file systems
   *  - el nombre de un file system
   *  - el nombre de un archivo.
   * */
  else if(argc == 2)
  {
	  if(strcmp(argv[1], "-fs") == 0)
	  {
		    for(i=0; i < maxFs; i++)
		    {
		    	fs = getRegisteredFS(i);
		    	if(i != maxFs - 1)
		          printf("%s%s", fs->fsName, FIELDSEP);
		    	else
		          printf("%s%s", fs->fsName, EOT);
		    }
	  }
	  else
	  {
		  sepCnt = 0;
		  for(i = 0; i < strlen(argv[1]); i++)
			  if(argv[1][i] == '/')
		      {
				  sepCnt ++;
				  if (sepCnt == 2)
				  {
					  fn = &argv[1][i + 1];
					  argv[1][i] = '\0';
				  }
			  }
		  if(sepCnt == 1) // FileSystem
		  {
			    for(i=0; i < maxFs; i++)
			    {
			      fs = getRegisteredFS(i);
			      if(strcmp(fs->fsName, argv[1]) == 0)
			      {
			          fs->openDir(fs, &dir);
			          file = fs->readDir(&dir);

			          while(file != 0)
			          {
			             printf("%s%s%lu", file->fn, FIELDSEP, file->size);

			             file = fs->readDir(&dir);
			             if (file != 0)
			            	printf("%s",FIELDSEP);
			          }
			          printf("%s", EOT);
			          fs->closeDir(&dir);
			     }
			  }
		  }
		  else if(sepCnt == 2) // File
		  {
			    for(i=0; i < maxFs; i++)
			    {
			      fs = getRegisteredFS(i);
			      if(strcmp(fs->fsName, argv[1]) == 0)
			      {
			          fs->openDir(fs, &dir);
			          file = fs->readDir(&dir);

			          while(file != 0)
			          {
			             if (strcmp(file->fn, fn) == 0)
			            	 printf("%s%s%lu", file->fn, FIELDSEP, file->size);
			             file = fs->readDir(&dir);
			          }
			          printf("%s", EOT);
			          fs->closeDir(&dir);
			     }
			  }
		  }
	  }

  }
  return 0;
}
