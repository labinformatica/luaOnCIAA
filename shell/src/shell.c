/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <shell.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <serial_drv.h>
#include "ntshell.h"

#include <ls.h>
#include <cat.h>
#include <rm.h>
#include <format.h>
#include <freeMem.h>
#include <clearScr.h>
#include <cp.h>
#include <dateTime.h>
#include <loadRamFs.h>
#include <mount.h>

#if defined(CORE_M0)
#define prompt "educiaa-m0# "
#elif defined (CORE_M4)
#define prompt "educiaa-m4# "
#else
#define prompt "educiaa-??# "
#endif
#ifdef  USE_DYNAMIC_MOUNT
#define CMDCNT 13
#else
#define CMDCNT 11
#endif

int helpMsg (int argc, char *argv[]);
int startLua (int argc, char *argv[]);

const shellCommand commands[CMDCNT] = {
  {"help", "Muestra los comandos soportados y una referencia básica", helpMsg},
  {"ls", "Lista los sistemas de archivos montados y su contenido", ls},
  {"cat","Muestra el contenido de un archivo de texto", cat},
  {"cp","Copia archivos entre sistemas de archivos", cp},
  {"rm","Elimina un archivo del sistemas de archivos", rm},
  {"date","Consulta o establece la fecha actual", cmdDate},
  {"time","Consulta o establece la hora", cmdTime},
  {"free","Muestra información sobre la memoria libre", freeMem},
  {"clear","Borra el conenido de la terminal", clearScr},
  {"format","Da formato a un punto de montaje registrado", format},
#ifdef USE_FS_DYNAMIC_LOAD
  {"loadRamFs","Carga un sistemas de archivos en memoria RAM", loadRamFs},
#endif
#ifdef USE_DYNAMIC_MOUNT
  {"mount","Monta un dispositivo para acceso a los archivos", mount},
  {"umount","Desactiva un dispositivo montado anteriormente", umount},
#endif
  {"lua","Inicia una sesión interactiva lua", startLua}

};

int helpMsg (int argc, char *argv[])
{
	int i;
	printf("Referencia básica de comandos internos soportados por el entorno.\n");
	printf("Comando | Descripción\n");
	for(i=0; i < CMDCNT; i++)
		printf("%7s | %s \n", commands[i].cmdName, commands[i].desc);
	return 0;
}

void removeEmptySpaces(char *txt)
{
	int i = 0;
	int j;
	if (!*txt)
		return;
    /* Quitamos espacios del comienzo */
	while(txt[i] == ' ') i++;
	j = 0;
	while(txt[i] != '\0')
	{
		txt[j] = txt[i];
		i++;
		j++;
	}
	txt[j] = '\0';
	/*
	 * Habiendo quitado los espacios en blanco del comienzo, buscamos
	 * dobles espacios en la cadena
	 * */
	i = 1;
	while(txt[i] != '\0')
	{
		if((txt[i-1] == ' ') && (txt[i] == ' '))
		{
			j = i;
			while(txt[j] != '\0')
			{
				txt[j-1] = txt[j];
				j++;
			}
			txt[j-1] = '\0';
		}
		else
		  i++;
	}
	/* Quitamos los espacios finales*/
	j = strlen(txt) - 1;
	while(txt[j] == ' ')
		j--;
	txt[j+1] ='\0';
}

extern FILE * getStdin(void);
extern int lua_main (int, char **);

int startLua (int argc, char *argv[])
{
	int ret;
	fflush(getStdin());
	clearerr(getStdin());
	ret = lua_main(argc, argv);
	fflush(getStdin());
	clearerr(getStdin());
	return ret;
}

static int serial_read(uint8_t *buf, int cnt, void *extobj)
{
    int i = 0;
    unsigned int lastReaded=0;
    while (i < cnt)
    {
      lastReaded = plt_recv_default_usart(&buf[i], 1);
      i += lastReaded;
    }
    return cnt;
}

static int serial_write(const uint8_t *buf, int cnt, void *extobj)
{
    int i;
    for (i = 0; i < cnt; i++) {
        putchar(buf[i]);
    }
    return cnt;
}

static int user_callback(char *text, void *extobj)
{
  char *argv[64]={0};
  int argc = 1;
  char tmpLine[MAXSHELLLINE];
  int i;
  unsigned int mxLen;
  unsigned char executed = 0;
  removeEmptySpaces(text);
  strncpy(tmpLine, text, MAXSHELLLINE - 1);
  mxLen = strlen(tmpLine);
  argv[0] = &tmpLine[0];
  for(i = 1;(i < mxLen) && (argc < 64); i++)
  {
    if(tmpLine[i - 1] == ' ')
    {
      argv[argc] = &tmpLine[i];
      tmpLine[i - 1] = '\0';
      argc++;
    }
  }
  if(strlen(tmpLine) > 0)
  {
    i = 0;
    while((i < CMDCNT) && (!executed))
    {
      if(strcmp(commands[i].cmdName, tmpLine) == 0)
      {
         commands[i].doCmd(argc, argv);
         executed = 1;
      }
      i++;
    }
    if(!executed)
      printf("Comando no reconocido. Ingrese ayuda para más información\n");
  }
    return 0;
}

extern char msg[];

void startInteractiveShell(void)
{
    void *extobj = 0;
    ntshell_t nts;
    printf("\033[2J");
    printf("%s", msg);
    printf("\n\nShell interactivo básico para EDU-CIAA NXP.\n");
    printf("Ejemplo de aplicación - versión 0.1.0\n\n");
    ntshell_init(&nts, serial_read, serial_write, user_callback, extobj);
    ntshell_set_prompt(&nts, prompt);
    while (1) {
        ntshell_execute(&nts);
    }
}
