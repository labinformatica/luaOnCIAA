/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <string.h>
#include <mount.h>
#include <fs.h>
#include <spi_memory_drv.h>
#include <spi_drv.h>
#include <gpio_drv.h>

static devmount dev[] = {
		{"spi",0,0},
		{"i2c",1,0},
		{"sd",2,0}
};

static devmount pin_ID[] = {
		{"RXD1"    ,0,0 },
		{"TX_EN"   ,0,1 },
		{"RXD0"    ,1,15},
		{"CRS_DV"  ,1,16},
		{"SPI_MISO",1,3 },
		{"SPI_MOSI",1,4 },
		{"MDIO"    ,1,17},
		{"TXD0"    ,1,18},
		{"TXD1"    ,1,20},
		{"T_COL0"  ,1,5 },
		{"T_FIL0"  ,4,0 },
		{"T_F1"    ,4,1 },
		{"T_FIL2"  ,4,2 },
		{"T_FIL3"  ,4,3 },
		{"LCD1"    ,4,4 },
		{"LCD2"    ,4,5 },
		{"LCD3"    ,4,6 },
		{"LCD4"    ,4,10},
		{"LCD_RS"  ,4,8 },
		{"LCD_EN"  ,4,9 },
		{"GPIO0"   ,6,1 },
		{"GPIO1"   ,6,4 },
		{"GPIO2"   ,6,5 },
		{"GPIO3"   ,6,7 },
		{"GPIO4"   ,6,8 },
		{"GPIO5"   ,6,9 },
		{"GPIO6"   ,6,10},
		{"GPIO7"   ,6,11},
		{"GPIO8"   ,6,12},
		{"T_COL1"  ,7,4 },
		{"T_C2"    ,7,5 },
		{"MDC"     ,7,7 },
		{"232_TX"  ,2,3 },
		{"232_RX"  ,2,4 },
		{"CAN_RD"  ,3,1 },
		{"CAN_TD"  ,3,2 }
};
//Convierte Texto a nuemro y lo retorna
static uint32_t txtonmb (char *text){
	uint32_t cont,ret=0;
	uint16_t tam=strlen(text);
	uint8_t i;
	cont=1;
	for(i=tam;i>0;i--){
		if(text[i-1]>47 && text[i-1]<58){
			ret+=(text[i-1]-48)*cont;
			cont*=10;
		}else ret = 0xFFFFFFFF;
	}
	return ret;
}

static dv_mnt *dv_mod;
int mount(int argc, char *argv[]){
	uint16_t ret=0;
	uint16_t t;
	uint16_t id_m,ss_m;
	uint32_t speed_m,cap_m;

	if (argc < 2){
		printf("Error!!!  EL comando mount usa parametros \nmount -> dispositivo Escl_Sel tamaño velocidad");
		printf("\nmount spi LCD_EN\n");
	}else{
		t=(sizeof(dev)/sizeof(devmount));
		id_m=pars(argv[1],dev,t);
		if(id_m==0 || id_m==2){
			t=(sizeof(pin_ID)/sizeof(devmount));
			ss_m=pars(argv[2],pin_ID,t);
		}else if(id_m==1){ //bus i2c
			ss_m=txtonmb(argv[2]);
			speed_m=txtonmb(argv[3]);
		}

	}
return ret;
}


int umount(int argc, char *argv[]){
int ret =0;
	if (argc !=3){
		printf("Error\nEL comando umount usa parametros: Dispositivo, Esclavo");
		printf("\nEjemplo:mount spi LCD_EN\n");
	}else{
		printf("Peligro, si desmonta el dispositivo antes\nde guardar los cambios puede perder informacion o dejar\nel Filesistem corrupto.\n");
		printf("Presione \"s\" para confirmar:\n");
		//fflush(stdin);
	}


return ret;
}
