/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "loadRamFs.h"
#include <datBoxFs.h>
#include <serial_drv.h>

typedef struct
{
    uint32_t imageSize;
    uint32_t fileCount;
    uint32_t blockSize;
}fsParam;

static int serial_read(uint8_t *buf, int cnt)
{
    int i = 0;
    unsigned int lastReaded=0;
    while (i < cnt)
    {
      lastReaded = plt_recv_default_usart(&buf[i], 1);
      i += lastReaded;
    }
    return cnt;
}

void flushInputBuffer(void)
{
	unsigned char x = ' ';
	while(x != 's')
	  plt_recv_default_usart(&x, 1);

}

extern dbfsRoot dbfsRam;
extern uint32_t __start_RamFs;
extern uint32_t __size_RamFs;

int loadRamFs(int argc, char *argv[])
{
	fsParam param;
	uint8_t response = 0;
	uint32_t blockCount;
	int i;
	uint8_t ok = 1;
	flushInputBuffer();
	serial_read((uint8_t *)&param, sizeof(param));
	response = param.imageSize <= &__size_RamFs;
    plt_send_default_usart(&response, 1);

    dbfsRam.absAddr = &__start_RamFs;
    dbfsRam.blkSize = param.blockSize;
    dbfsRam.length = param.imageSize;
    dbfsRam.maxFiles = param.fileCount;
    free(dbfsRam.rwBuff);
    dbfsRam.rwBuff = malloc(param.blockSize);
    blockCount = param.imageSize/param.blockSize;
    for(i = 0; (i < blockCount) && response; i++)
    {
    	ok = 1;
    	serial_read(dbfsRam.absAddr + (i*param.blockSize), param.blockSize);
    	plt_send_default_usart(&ok, 1);
    }
    return 0;
}
