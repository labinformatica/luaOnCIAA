/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "dateTime.h"
#include <stdio.h>
#include <rtc_drv.h>

int cmdDate(int argc, char *argv[])
{
	int ret = 0;
	int d, m, a;
	if(argc == 1)
	{
		plt_rtc_gdate(&d, &m, &a);
		printf("%02d/%02d/%04d\n", d, m, a);
	}
	else if (argc == 2)
	{
		if (sscanf((const char *)argv[1], "%d/%d/%d", &d, &m, &a) == 3)
			plt_rtc_sdate(d, m, a);
		else
			printf("Error de formato. Por favor utilice como formato dd/mm/yyyy\n");
	}
	else
	{
		ret = -1;
	}
	return ret;
}

int cmdTime(int argc, char *argv[])
{
	int ret = 0;
	int h, m, s;
	if(argc == 1)
	{
		plt_rtc_gtime(&s, &m, &h);
		printf("%02d:%02d:%02d\n", h, m, s);
	}
	else if (argc == 2)
	{
		if (sscanf(argv[1], "%d:%d:%d", &h, &m, &s) == 3)
			plt_rtc_stime(s, m, h);
		else
			printf("Error de formato. Por favor utilice como formato hh:mm:ss\n");
	}
	else
	{
		ret = -1;
	}
	return ret;
}

