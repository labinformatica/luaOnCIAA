/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <format.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fs.h>

int format(int argc, char *argv[])
{
	unsigned int i=0;
	int ret = 0;
	char op;
	unsigned char find = 0;
	struct stFileSystems * fs;
	if(argc != 2)
	{
		printf("Error!\n El comando format toma 1 argumento, el nombre de la partición a formatear.");
		printf("Por ejemplo, si al ejecutar el comando ls se observara un punto de montaje /eeprom, ");
		printf("podría ejecutarse format /eeprom\n");
	}
	else
	{
		for(i=0; i < getRegisteredFSCount(); i++)
		{
			fs = getRegisteredFS(i);
			if (strcmp(fs->fsName, argv[1]) == 0)
			{
				if(fs->format)
				{
					printf("Atención!!!\nLa información contenida en la partición %s se perderá!\n", argv[1]);
					printf("Presione \"s\" para confirmar:");
					op = getchar();
					if(op == 's')
					{
						printf("Dando formato, aguarde por favor...");
						fs->format(fs);
						printf("Formato realizado!\n");
						ret = 1;
					}
					else
					  printf("Formato cancelado!\n");
				}
				else
					printf("Al dispositivo %s no es posible darle formato.\n", argv[1]);
				find = 1;
			}
		}
		if(!find)
		{
			printf("Error: No se ha encontrado registrado el punto de entrada %s\n", argv[1]);
		}
	}
	return ret;
}
