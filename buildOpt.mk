export GCCPATH=/home/sergio/ciaa/arm-eclipse/gcc/bin
export GCCPREFIX=arm-none-eabi
export BUILDTYPE=debug
export BUILDIR=build
export TEMPSYS=sysRoot
export OUTDIR=out

export TOOLDIR=tools
export LD:=$(GCCPATH)/$(GCCPREFIX)-ld
export AS:=$(GCCPATH)/$(GCCPREFIX)-as
export GCC:=$(GCCPATH)/$(GCCPREFIX)-gcc
export AR:=$(GCCPATH)/$(GCCPREFIX)-ar
export DUMP:=$(GCCPATH)/$(GCCPREFIX)-objdump
export COPY:=$(GCCPATH)/$(GCCPREFIX)-objcopy
export READELF:=$(GCCPATH)/$(GCCPREFIX)-readelf
export SIZE:=$(GCCPATH)/$(GCCPREFIX)-size
export RANLIB:=$(GCCPATH)/$(GCCPREFIX)-ranlib

export BASEPATH:=$(shell pwd)

ifeq ($(BUILDTYPE),debug)
  export OPT=-O0 -ggdb3 
else
  export OPT=-O2
endif

#FSLOAD=-DUSE_FS_DYNAMIC_LOAD
FSLOAD=""

export CFLAGSM0=-Wall $(OPT) -fdata-sections -ffunction-sections -mcpu=cortex-m0 -mthumb -mfloat-abi=soft -DCORE_M0 -Dchip_lpc43xx \
 -DNO_BOARD_LIB -D__USE_LPCOPEN $(FSLOAD) -D__MULTICORE_M0APP -DCORE_M0APP -DLUA_OPTIMIZE_MEMORY=2 -DLUA_32BITS -mno-unaligned-access -D__BUFSIZ__=128 -D_GNU_SOURCE\
 -I$(BASEPATH)/lpc43xx/config_43xx_m0app -I$(BASEPATH)/platform/inc -I$(BASEPATH)/$(TEMPSYS)/include -I$(BASEPATH)/lpc43xxx/inc 
#-nostdlib
export LDFLAGSM0=-static -fno-builtin -mcpu=cortex-m0 -mthumb -mfloat-abi=soft -Xlinker -Map=$(OUTDIR)/firmware-m0.map -Wl,-gc-sections -mno-unaligned-access \
-Tm0/ld/libs.ld -Tm0/ld/mem.ld -Tm0/ld/areas.ld -L$(BASEPATH)/$(TEMPSYS)/lib -L$(BASEPATH)/$(TEMPSYS)/arm-none-eabi/lib -lc -lm

export CFLAGSM4=-Wall $(OPT) -fdata-sections -ffunction-sections -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -I$(BASEPATH)/platform/inc\
 -DCORE_M4 -Dchip_lpc43xx $(FSLOAD) -DNO_BOARD_LIB -D__USE_LPCOPEN -D__LPC43XX__ -D__CODE_RED -DLUA_OPTIMIZE_MEMORY=2 -DLUA_32BITS -D_GNU_SOURCE\
 -I$(BASEPATH)/cortex-m4/inc -I$(BASEPATH)/lpc43xx/config_43xx -I$(BASEPATH)/lpc43xx/inc -I$(BASEPATH)/$(TEMPSYS)/include 
#-nostdlib
export LDFLAGSM4=-static -fno-builtin -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -Wl,-gc-sections -Xlinker   \
 -Tm4/ld/libs.ld -Tm4/ld/mem.ld -Tm4/ld/areas.ld -L$(BASEPATH)/$(TEMPSYS)/lib -L$(BASEPATH)/$(TEMPSYS)/arm-none-eabi/lib
