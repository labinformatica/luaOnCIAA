/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "sysinit.h"
#include <stdint.h>
#include "chip.h"

const uint32_t ExtRateIn = 0;
const uint32_t OscRateIn = 12000000;

void SystemInit(void)
{
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;
	extern void *isrVector;
	*pSCB_VTOR = (unsigned int) &isrVector;
#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif
	Chip_SystemInit();
	Chip_GPIO_Init(LPC_GPIO_PORT);

	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_RED, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_GRN, MD_PUP, FUNC4);
	Chip_SCU_PinMux(PORT_PIN_RGB,  PIN_RGB_BLU, MD_PUP, FUNC4);

	Chip_SCU_PinMux(PORT_PIN_LED1, PIN_LED1, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED2, PIN_LED2, MD_PUP, FUNC0);
	Chip_SCU_PinMux(PORT_PIN_LED3, PIN_LED3, MD_PUP, FUNC0);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED2, GPIO_PIN_LED2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_LED3, GPIO_PIN_LED3);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_RED);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_GRN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_RGB, GPIO_PIN_BLU);

}
