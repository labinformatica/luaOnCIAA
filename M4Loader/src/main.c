#include <chip.h>
#include <stdint.h>
#include <stdbool.h>
#include "sysinit.h"
#include <string.h>
#include "app_multicore_cfg.h"

#define TICKRATE_HZ 4

extern int M0Image_Boot(CPUID_T , uint32_t);

void TIMER1_IRQHandler(void)
{
	static bool On = false;

	if (Chip_TIMER_MatchPending(LPC_TIMER1, 1)) {
		Chip_TIMER_ClearMatch(LPC_TIMER1, 1);
		On = (bool) !On;
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, (bool) On);
	}
}

int main(void)
{
  uint32_t timerFreq;
  SystemCoreClockUpdate();
  SystemInit();
  M0Image_Boot(CPUID_M0APP, (uint32_t) 0x1b000000);
  Chip_TIMER_Init(LPC_TIMER1);
  Chip_RGU_TriggerReset(RGU_TIMER1_RST);
  while (Chip_RGU_InReset(RGU_TIMER1_RST)) {}

  /* Get timer 1 peripheral clock rate */
  timerFreq = Chip_Clock_GetRate(CLK_MX_TIMER1);

  /* Timer setup for match and interrupt at TICKRATE_HZ */
  Chip_TIMER_Reset(LPC_TIMER1);
  Chip_TIMER_MatchEnableInt(LPC_TIMER1, 1);
  Chip_TIMER_SetMatch(LPC_TIMER1, 1, (timerFreq / TICKRATE_HZ));
  Chip_TIMER_ResetOnMatchEnable(LPC_TIMER1, 1);
  Chip_TIMER_Enable(LPC_TIMER1);

  /* Enable timer interrupt */
  NVIC_EnableIRQ(TIMER1_IRQn);
  NVIC_ClearPendingIRQ(TIMER1_IRQn);
  Chip_GPIO_SetPinState(LPC_GPIO_PORT, GPIO_PORT_LED1, GPIO_PIN_LED1, (bool) true);

  while(1);
  return 0;
}
