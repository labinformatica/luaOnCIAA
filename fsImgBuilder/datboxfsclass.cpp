/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "datboxfsclass.h"
#include <QString>
#include <stdio.h>
#include <QFile>
#include <fcntl.h>
#include <datBoxFs.h>

//typedef void (*writeFunction)(struct stDbFsStruct *, uint32_t offset, uint8_t *readFrom, uint32_t len, void * baseAddr);

void fileWriteFunc(dbfsRoot *dbfs, uint32_t offset, uint8_t *readFrom, uint32_t len, void * baseAddr)
{
  baseAddr = baseAddr;
  fseek(dbfs->fileHandle, offset, SEEK_SET);
  fwrite(readFrom, len, 1, dbfs->fileHandle);
  fflush(dbfs->fileHandle);
}

//typedef void (*readFunction) (struct stDbFsStruct *, uint8_t *writeTo, uint32_t offset, uint32_t len, void * baseAddr);
void fileReadFunc(dbfsRoot *dbfs, uint8_t *writeTo, uint32_t offset, uint32_t len, void * baseAddr)
{
  baseAddr = baseAddr;
  fseek(dbfs->fileHandle, offset, SEEK_SET);
  fread(writeTo, len, 1, dbfs->fileHandle);
}

datBoxFsClass::datBoxFsClass()
{
    dbFsData.absAddr = 0;
    dbFsData.rwBuff = NULL;
    dbFsData.fcnRd = fileReadFunc;
    dbFsData.fcnWr = fileWriteFunc;
    dbFsData.fcnOpn = NULL;
    dbFsData.fcnCls = NULL;
    dbFsData.fileHandle =  NULL;
    this->fsImgFileName.clear();
    this->fsImgFileParam.clear();
}

bool datBoxFsClass::createFs(fsParam p, QString fn)
{
    bool ret = false;
    QString fnFsPar;
    QString fnFsImg;
    QFile filePar;
    QFile fileImg;
    uint32_t i;
    uint8_t buff = EMPTY_ENTRY;
    if (fn.right(4) == ".img")
    {
      fnFsImg = fn;
      fnFsPar = fn.left(fn.length() - 3) + "cfg";
    }
    else if(fn.right(4) == ".cfg")
    {
      fnFsPar = fn;
      fnFsImg = fn.left(fn.length() - 3) + "img";
    }
    else
    {
        fnFsImg = fn + ".img";
        fnFsPar = fn + ".cfg";
    }
    filePar.setFileName(fnFsPar);
    fileImg.setFileName(fnFsImg);
    filePar.open(QIODevice::WriteOnly | QIODevice::Truncate);
    fileImg.open(QIODevice::WriteOnly | QIODevice::Truncate);
    if(filePar.isOpen() && fileImg.isOpen())
    {
        filePar.write((char *)&p, sizeof(p));
        for(i = 0; i < p.imageSize; i++)
            fileImg.write((char *)&buff, 1);
        ret = true;
    }
    filePar.close();
    fileImg.close();
    return ret;
}

bool datBoxFsClass::loadFs(QString fn)
{
    bool ret = false;
    QFile filePar;
    fsParam p;

    if (fn.right(4) == ".img")
    {
        this->fsImgFileName = fn;
        this->fsImgFileParam = fn.left(fn.length() - 3) + "cfg";
    }
    else if(fn.right(4) == ".cfg")
    {
        this->fsImgFileParam = fn;
        this->fsImgFileName = fn.left(fn.length() - 3) + "img";
    }
    else
    {
        this->fsImgFileParam = fn+ ".cfg";
        this->fsImgFileName = fn + ".img";
    }
    if(dbFsData.rwBuff)
        delete dbFsData.rwBuff;

    filePar.setFileName(this->fsImgFileParam);
    filePar.open(QIODevice::ReadOnly);
    dbFsData.fileHandle = fopen(this->fsImgFileName.toStdString().c_str(), "rb+");
    ret = (dbFsData.fileHandle != NULL) && filePar.isOpen();
    if (ret)
    {
        filePar.read((char *)&p, sizeof(p));
        filePar.close();
        dbFsData.blkSize = p.blockSize;
        dbFsData.length = p.imageSize;
        dbFsData.maxFiles = p.fileCount;
        dbFsData.rwBuff = new uint8_t[p.blockSize];
    }
    else
    {
        this->fsImgFileParam.clear();
        this->fsImgFileName.clear();
    }
    return ret;
}

bool datBoxFsClass::addFile(QString fn)
{
    QFile f;
    QString fsFn = fn.right(fn.length() - (fn.lastIndexOf("/")+1));
    bool ret = true;
    int fd = 0;
    bool Ok = true;
    uint8_t rwBuff;
    struct stFileSystems fs;
    strcpy(fs.fsName, "/pcfs");
    fs.fsData = &dbFsData;
    f.setFileName(fn);
    f.open(QIODevice::ReadOnly);
    if(f.isOpen())
    {
        if(dbfsFileExists(&dbFsData, (char *)fsFn.toStdString().c_str()) != -1)
            ret = false;
        else
        {
           fd = dbfsOpen((char *)((QString("/pcfs/"))+fsFn).toStdString().c_str(), O_CREAT | O_TRUNC, 0, &fs);
           if (fd == -1)
               ret = false;
           else
           {
               while(!f.atEnd() && Ok)
               {
                   f.read((char *)&rwBuff, 1);
                   Ok = dbfsWrite(fd, &rwBuff, 1) == 1;
               }
               ret = Ok;
               dbfsClose(fd);
           }
        }
        f.close();
    }
    else
        ret = false;
    return ret;
}

bool datBoxFsClass::exportFile(QString fn, QString outPath)
{
  QFile toSave;
  int fd;
  uint8_t buffer;
  struct stFileSystems fs;
  strcpy(fs.fsName, "/pcfs");
  fs.fsData = &dbFsData;
  bool ret = false;
  if(dbfsFileExists(&dbFsData, (char *)fn.toStdString().c_str()) != -1)
  {
      toSave.setFileName(outPath + "/" + fn);
      toSave.open(QIODevice::WriteOnly | QIODevice::Truncate);
      if(toSave.isOpen())
      {
          fd = dbfsOpen((char *)(QString("/pcfs/") + fn).toStdString().c_str(),0, 0, &fs);
          if(fd != -1)
          {
              while(dbfsRead(fd, (uint8_t *)&buffer, 1))
                  toSave.write((const char *)&buffer, 1);
              ret = true;
          }

          dbfsClose(fd);
          toSave.close();
      }
  }
  return ret;
}

bool datBoxFsClass::removeFile(QString fn)
{
  bool ret = false;
  struct stFileSystems fs;
  strcpy(fs.fsName, "/pcfs");
  fs.fsData = &dbFsData;
  if(dbfsFileExists(&dbFsData, (char *)fn.toStdString().c_str()) != -1)
  {
      dbfsUnlink((char *)(QString("/pcfs/")+ fn).toStdString().c_str(),&fs);
      ret = true;
  }
  return ret;
}

bool datBoxFsClass::getFirstFile(QString &fn, uint32_t &size)
{    
    strcpy(fs.fsName, "/pcfs");
    fs.fsData = &dbFsData;
    bool ret = false;
    dbfsOpenDir(&fs, &de);
    fe = dbfsReadDir(&de);
    if (fe)
    {
        fn = QString(fe->fn);
        size = fe->size;
    }
    ret = fe != 0;
    return ret;
}

bool datBoxFsClass::getNextFile(QString &fn, uint32_t &size)
{
    bool ret;
    fe = dbfsReadDir(&de);
    if (fe)
    {
        fn = QString(fe->fn);
        size = fe->size;
    }
    ret = fe != 0;
    return ret;
}

bool datBoxFsClass::saveFsAs(QString newName)
{
    QString newFsImgFn;
    QString newFsParFn;
    bool ret = false;
    if(!this->fsImgFileName.isEmpty() && !this->fsImgFileParam.isEmpty())
    {
        if (newName.right(4) == ".img")
        {
            newFsImgFn = newName;
            newFsParFn = newName.left(newName.length() - 3) + "cfg";
        }
        else if(newName.right(4) == ".cfg")
        {
            newFsParFn = newName;
            newFsImgFn = newName.left(newName.length() - 3) + "img";
        }
        else
        {
            newFsParFn = newName + ".cfg";
            newFsImgFn = newName + ".img";
        }

        closeFs();
        ret = true;
        ret = ret && QFile::copy(this->fsImgFileName, newFsImgFn);
        ret = ret && QFile::copy(this->fsImgFileParam, newFsParFn);
        loadFs(newFsImgFn);
    }
    return ret;
}


void datBoxFsClass::closeFs(void)
{
  if (dbFsData.fileHandle)
      fclose(dbFsData.fileHandle);
  if(dbFsData.rwBuff)
  {      
      delete dbFsData.rwBuff;
      dbFsData.rwBuff = NULL;
  }
}

uint32_t datBoxFsClass::getMaxFileCount()
{
    return this->dbFsData.maxFiles;
}

uint32_t datBoxFsClass::getFileSystemSize()
{
    return this->dbFsData.length;
}

bool datBoxFsClass::sendFsParam(QIODevice *io)
{
    fsParam imgPar;
    unsigned char * toSend = (unsigned char *)& imgPar;
    unsigned char reciv = 0x00;
    unsigned char start = 's';
    imgPar.blockSize = this->dbFsData.blkSize;
    imgPar.fileCount = this->dbFsData.maxFiles;
    imgPar.imageSize = this->dbFsData.length;
    if(io->bytesAvailable() > 0)
        io->readAll();
    io->write((const char *)&start, 1);
    io->write((const char *)toSend, sizeof(imgPar));
    io->read((char *)&reciv, 1);
    return reciv != 0x00;
}

bool datBoxFsClass::sendFsImage(QIODevice *io)
{
    unsigned char *buffer;
    unsigned char reciv = 0x01;
    buffer = new unsigned char[this->dbFsData.blkSize];
    unsigned int blockCount;
    unsigned int i;
    if(io->bytesAvailable() > 0)
        io->readAll();
    fseek(this->dbFsData.fileHandle, 0, SEEK_END);
    blockCount = ftell(this->dbFsData.fileHandle)/this->dbFsData.blkSize;
    fseek(dbFsData.fileHandle, 0, SEEK_SET);

    for(i = 0; (i < blockCount) && (reciv != 0); i++)
    {
        fread(buffer, this->dbFsData.blkSize, 1, this->dbFsData.fileHandle);
        io->write((const char *)buffer, this->dbFsData.blkSize);
        io->read((char *)&reciv, 1);
    }
    delete []buffer;
    return (reciv != 0x00);
}


datBoxFsClass::~datBoxFsClass()
{
    if (dbFsData.fileHandle)
      fclose(dbFsData.fileHandle);
    if(dbFsData.rwBuff)
        delete dbFsData.rwBuff;
}
