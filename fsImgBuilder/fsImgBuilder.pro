#-------------------------------------------------
#
# Project created by QtCreator 2018-03-29T20:19:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fsImgBuilder
TEMPLATE = app

INCLUDEPATH +=../fs/datBoxFs/inc
INCLUDEPATH +=../fs/inc
INCLUDEPATH +=../platform/inc

include(qextserialport-1.2rc/src/qextserialport.pri)

DEFINES += PCDEV

SOURCES += main.cpp\
        maindlg.cpp \
    fsparam.cpp \
    ../fs/datBoxFs/src/datBoxFs.c \
    ../fs/src/fs.c \
    datboxfsclass.cpp \
    portsetup.cpp

HEADERS  += maindlg.h \
    fsparam.h \
    ../fs/datBoxFs/inc/datBoxFs.h \
    datboxfsclass.h \
    portsetup.h

FORMS    += maindlg.ui \
    fsparam.ui \
    portsetup.ui

RESOURCES += \
    imagenes.qrc
