/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "maindlg.h"
#include "ui_maindlg.h"

#include "fsparam.h"
#include "ui_fsparam.h"

#include "portsetup.h"
#include "ui_portsetup.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <qextserialenumerator.h>
#include <qextserialport.h>

void mainDlg::updateFileList()
{
    QTreeWidgetItem *fileEntry;
    uint32_t fileCount = 0;
    QString fileName;
    uint32_t fileSize;
    uint32_t fileSizeAcc = 0;
    ui->fileTree->clear();
    if(dbFs.getFirstFile(fileName, fileSize))
    {
        fileEntry = new QTreeWidgetItem(ui->fileTree);
        fileEntry->setIcon(0, QIcon(":/icons/file"));
        fileEntry->setText(0, fileName);
        fileEntry->setText(1, QString::number(fileSize));
        ui->fileTree->addTopLevelItem(fileEntry);
        fileCount = 1;
        fileSizeAcc = fileSize;
        while(dbFs.getNextFile(fileName, fileSize))
        {
            fileEntry = new QTreeWidgetItem(ui->fileTree);
            fileEntry->setIcon(0, QIcon(":/icons/file"));
            fileEntry->setText(0, fileName);
            fileEntry->setText(1, QString::number(fileSize));
            ui->fileTree->addTopLevelItem(fileEntry);
            fileCount ++;
            fileSizeAcc += fileSize;
        }
    }
    ui->labInfo->setText("Archivos: " + QString::number(fileCount) + "/" + QString::number(dbFs.getMaxFileCount()) + " | " + QString::number(fileSizeAcc) + "B / " + QString::number(dbFs.getFileSystemSize()) + "B ");
}


mainDlg::mainDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mainDlg)
{
    ui->setupUi(this);
}

mainDlg::~mainDlg()
{
    delete ui;
}

void mainDlg::on_btnExit_clicked()
{
    close();
}

void mainDlg::on_btnNew_clicked()
{
    bool ok_blkSize;
    bool ok_fileCount;
    bool ok_imgSize;
    bool ret;
    fsParam p;
    fsParamDlg dlg(this);
    if (dlg.exec() == QDialog::Accepted)
    {
        p.blockSize = dlg.ui->leBlkSize->text().toUInt(&ok_blkSize);
        ok_blkSize = ok_blkSize && p.blockSize > 0;
        if(!ok_blkSize)
            QMessageBox::critical(this, "Error en los parámetros", "El tamaño de bloque debe ser un número entero positivo");

        p.fileCount = dlg.ui->leCntFiles->text().toUInt(&ok_fileCount);
        ok_fileCount = ok_fileCount && p.fileCount > 0;
        if(!ok_fileCount)
            QMessageBox::critical(this, "Error en los parámetros", "La cantidad máxima de archivos a contener en el sistema de archivos debe ser un número entero positivo");

        p.imageSize = dlg.ui->leFsSize->text().toUInt(&ok_imgSize);
        ok_imgSize = ok_imgSize && p.imageSize > 0;
        if (!ok_imgSize)
            QMessageBox::critical(this, "Error en los parámetros", "El tamaño del sistema de archivos debe ser un valor entero positivo");

        ok_imgSize = ok_imgSize && ((p.imageSize % p.blockSize) == 0);
        if (!ok_imgSize)
            QMessageBox::critical(this, "Error en los parámetros", "El tamaño del sistema de archivos debe ser múltiple entero del tamaño del bloque utilizado");
        ret = ok_blkSize && ok_fileCount && ok_imgSize;
        if (ret)
            ret = dbFs.createFs(p, dlg.fsName);
        if(!ret)            
            QMessageBox::critical(this, "Error", "No ha sido posible crear el sistema de archivos solicitado");
        else
        {
            if(dbFs.loadFs(dlg.fsName))
            {
                ui->btnAdd->setEnabled(true);
                ui->btnExport->setEnabled(true);
                ui->btnRemove->setEnabled(true);
                ui->btnFsSend->setEnabled(true);
                updateFileList();
            }
            else
                QMessageBox::critical(this, "Error", "Error cargando el sistema de archivos");

        }
    }
}

void mainDlg::on_btnLoad_clicked()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, "Nombre del sistema de archivos",QApplication::applicationDirPath(), "Archivos de imágenes(*.img);;Archivos de parámetros(*.par);;Todos los archivos (*.*)");
    if (!fileName.isNull())
    {
        if (!dbFs.loadFs(fileName))
            QMessageBox::critical(this, "Error", "Error cargando el archivo de imagen del sistema de archivos");
        else
        {
            ui->btnAdd->setEnabled(true);
            ui->btnExport->setEnabled(true);
            ui->btnRemove->setEnabled(true);
            updateFileList();
        }
    }
}


void mainDlg::on_btnAdd_clicked()
{
    int i;
    bool Ok = true;
    QStringList files = QFileDialog::getOpenFileNames(
                            this,
                            "Seleccione los archivos a agregar al sistema de archivos",
                            QApplication::applicationDirPath(),
                            "Todos los archivos (*.*)");
    for(i = 0; (i < files.count()) && Ok; i++)
        Ok = Ok && dbFs.addFile(files.at(i));
    if(!Ok)
        QMessageBox::critical(this, "Error agregando archivos", "Alguno de los archivos seleccionado no se ha podido agregar.");
    updateFileList();
}

void mainDlg::on_btnRemove_clicked()
{
    QString fn;
    int i;
    bool Ok = true;
    QList<QTreeWidgetItem *> selected = ui->fileTree->selectedItems();
    for(i = 0; (i< selected.count()) && Ok; i++)
    {
        fn = selected.at(i)->text(0);
        Ok = Ok && dbFs.removeFile(fn);
    }
    if(!Ok)
        QMessageBox::critical(this, "Error", "Cuando menos uno de los archivos indicados no pudo ser eleminado");
    updateFileList();
}

void mainDlg::on_btnExport_clicked()
{
    QString fn;
    QString outFolder = QFileDialog::getExistingDirectory(this, "Exportar los archivos", QApplication::applicationDirPath());
    int i;
    bool Ok = true;
    if (!outFolder.isNull())
    {
      QList<QTreeWidgetItem *> selected = ui->fileTree->selectedItems();
      for(i = 0; (i< selected.count()) && Ok; i++)
      {
        fn = selected.at(i)->text(0);
        Ok = Ok && dbFs.exportFile(fn, outFolder);
      }
      if(!Ok)
          QMessageBox::critical(this, "Error", "Cuando menos uno de los archivos seleccionados no pudo ser exportado");
    }
}

void mainDlg::on_btnFsSend_clicked()
{
    bool ok;
    QextSerialPort * port;
    portSetup dlgPortSetup(this);
    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();
    dlgPortSetup.ui->cbSerialPort->clear();
    foreach(QextPortInfo info, ports)
        dlgPortSetup.ui->cbSerialPort->addItem(info.physName);
    dlgPortSetup.ui->cbSerialPort->setCurrentIndex(0);
    if(dlgPortSetup.exec() == QDialog::Accepted)
    {
        port = new QextSerialPort();
        port->setTimeout(1000);
        port->setPortName(dlgPortSetup.ui->cbSerialPort->currentText());
        port->setBaudRate(BAUD115200);
        port->setDataBits(DATA_8);
        port->setFlowControl(FLOW_OFF);
        port->setParity(PAR_NONE);
        port->setStopBits(STOP_1);
        port->open(QIODevice::ReadWrite);
        port->write("loadRamFs\r\n");
        ok = dbFs.sendFsParam(port);
        if (ok)
            ok = ok & dbFs.sendFsImage(port);
        port->close();
        delete port;
        if (!ok)
            QMessageBox::critical(this, "Error", "Se ha producido un error durante el envío del archivo");
    }

}
