/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef DATBOXFSCLASS_H
#define DATBOXFSCLASS_H

#include <QString>
#include <datBoxFs.h>
#include <QIODevice>

typedef struct
{
    uint32_t imageSize;
    uint32_t fileCount;
    uint32_t blockSize;
}fsParam;


void fileWriteFunc(dbfsRoot *dbfs, uint32_t offset, uint8_t *readFrom, uint32_t len, void * baseAddr);
void fileReadFunc(dbfsRoot *dbfs, uint8_t *writeTo, uint32_t offset, uint32_t len, void * baseAddr);

class datBoxFsClass
{

private:
    QString fsImgFileName;
    QString fsImgFileParam;
    dbfsRoot dbFsData;
    struct stFileSystems fs;
    dirEntry de;
    fileEntry *fe;
public:
    datBoxFsClass();
    bool createFs(fsParam p, QString fn);
    bool loadFs(QString fn);
    bool addFile(QString fn);
    bool exportFile(QString fn, QString outPath);
    bool removeFile(QString fn);
    bool getFirstFile(QString &fn, uint32_t &size);
    bool getNextFile(QString &fn, uint32_t &size);
    bool saveFsAs(QString newName);
    void closeFs(void);
    uint32_t getMaxFileCount();
    uint32_t getFileSystemSize();
    bool sendFsParam(QIODevice *io);
    bool sendFsImage(QIODevice *io);
    ~datBoxFsClass();
};

#endif // DATBOXFSCLASS_H
