/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <config.h>
#include <system_init.h>
#include <serial_drv.h>
#include <eeprom_drv.h>
#include <rtc_drv.h>
#include <tmr_drv.h>
#include <string.h>
#include <spi_memory_drv.h>

const uint32_t ExtRateIn = 0;
const uint32_t OscRateIn = 12000000;
static void setup_pins();

void plt_system_init(void)
{
  #if defined(CORE_M4)
    // Asumo que solo se debe configurar el sistema de clock si se inicia desde el procesador principal
	SystemCoreClockUpdate();
	Chip_SystemInit();
    fpuInit();
  #endif
  plt_configure_default_usart(defaultSerialId, defaultBaud, PLT_USART_DATA_8, PLT_USART_PARITY_NONE, PLT_USART_STOP_BIT_1);
  Chip_UART_SendBlocking(DEFAULT_UART_LPC, CLRSCR, strlen(CLRSCR));
  #if defined (MsgAtBoot)
    Chip_UART_SendBlocking(DEFAULT_UART_LPC, BOOTMSG, strlen(BOOTMSG));
  #endif
  plt_eeprom_init();
  plt_rtc_init();
  plt_sys_tmr_init();
  setup_pins();
  init_flash_memory();
}




 static void setup_pins(){
  	Chip_GPIO_Init(LPC_GPIO_PORT);
  	Chip_SCU_PinMux(0x02, 0x00, SCU_MODE_PULLUP, 0x04); //RGB R
  	Chip_SCU_PinMux(0x02, 0x01, SCU_MODE_PULLUP, 0x04); //RGB G
  	Chip_SCU_PinMux(0x02, 0x02, SCU_MODE_PULLUP, 0x04); //RGB B
  	Chip_SCU_PinMux(0x02, 0x0A, SCU_MODE_PULLUP, 0x00); //Led 1
  	Chip_SCU_PinMux(0x02, 0x0B, SCU_MODE_PULLUP, 0x00); //Led 2
  	Chip_SCU_PinMux(0x02, 0x0C, SCU_MODE_PULLUP, 0x00); //Led 3
    Chip_SCU_PinMux(0x01, 0x00, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, 0x00); //Tecl 1
    Chip_SCU_PinMux(0x01, 0x01, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, 0x00); //Tecl 2
    Chip_SCU_PinMux(0x01, 0x02, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, 0x00); //Tecl 3
    Chip_SCU_PinMux(0x01, 0x06, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS, 0x00); //Tecl 4
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x00, 0x0E); //Led 1
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x01, 0x0B); //Led 2
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x01, 0x0C); //Led 3
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x05, 0x00); //RGB R
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x05, 0x01); //RGB G
  	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0x05, 0x02); //RGB B
  	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 0x00, 0x04);  //Tecl 1
  	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 0x00, 0x08);  //Tecl 2
  	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 0x00, 0x09);  //Tecl 3
  	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 0x01, 0x09);  //Tecl 4
  	}
