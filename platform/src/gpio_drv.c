/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "gpio_drv.h"
#include "gpio_conf.h"
#include <chip.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

//port: numero puerto  Tpin: pin del puerto   IO_D: 0-INPUT  mode: modos
uint8_t PinToR(uint8_t Ep,uint8_t Ep1,uint8_t *Rprt,uint8_t *Rpin){
uint8_t error=64;
	*Rprt=error;
	*Rpin=error;
	switch (Ep){
	case 0:
		if(Ep1>1)return 0;
		*Rprt=pin0[Ep1].port;
		*Rpin=pin0[Ep1].pin;
		break;
	case 1:
		if(Ep1>20)return 0;
		*Rprt=pin1[Ep1].port;
		*Rpin=pin1[Ep1].pin;
		break;
	case 2:
		if(Ep1>13)return 0;
		*Rprt=pin2[Ep1].port;
		*Rpin=pin2[Ep1].pin;
		break;
	case 3:
		if(Ep1>8)return 0;
		*Rprt=pin3[Ep1].port;
		*Rpin=pin3[Ep1].pin;
		break;
	case 4:
		if(Ep1>10)return 0;
		*Rprt=pin4[Ep1].port;
		*Rpin=pin4[Ep1].pin;
		break;
	case 5:
		if(Ep1>7)return 0;
		*Rprt=pin5[Ep1].port;
		*Rpin=pin5[Ep1].pin;
		break;
	case 6:
		if(Ep1>12)return 0;
		*Rprt=pin6[Ep1].port;
		*Rpin=pin6[Ep1].pin;
		break;
	case 7:
		if(Ep1>7)return 0;
		*Rprt=pin7[Ep1].port;
		*Rpin=pin7[Ep1].pin;
		break;
	case 9:
		if(Ep1>6)return 0;
		*Rprt=pin9[Ep1].port;
		*Rpin=pin9[Ep1].pin;
		break;
	default:
		return 0;
	}
	return 1;
}

uint8_t Pin_FUNC (uint8_t port, uint8_t Tpin,uint8_t IO_D,uint16_t mode)
{
uint8_t prt = 65; 
uint8_t pin = 65;
uint8_t func = 0;

if((Tpin>15 && port < 5)|| (Tpin > 18)) return 0xff;
 switch (port){
   case 0x00:
   //  func=0x00;         
       prt = Port0[Tpin].port;
       pin = Port0[Tpin].pin;
   break;
   case 0x01:
   //  func=0x00;         
       prt = Port1[Tpin].port;
       pin = Port1[Tpin].pin;
   break;
   case 0x02:
   //    func=0x00;         
       prt = Port2[Tpin].port;
       pin = Port2[Tpin].pin;
   break;
   case 0x03:
   //  func=0x00;         
       prt = Port3[Tpin].port;
       pin = Port3[Tpin].pin;
   break;
   case 0x04:
   //  func=0x00;         
       prt = Port4[Tpin].port;
       pin = Port4[Tpin].pin;
   break;
   case 0x05:
       func=0x04;         
       prt = Port5[Tpin].port;
       pin = Port5[Tpin].pin;
   break;
   default:
     return 0xff;
   }
 if(prt<63 && pin <63){
   if(IO_D == 0x00){
     Chip_SCU_PinMux(prt, pin, mode, func); // Input   SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS
   }else{
     Chip_SCU_PinMux(prt, pin, mode, func); //Output   MD_PUP
   }
  return 0x00;
 }
 return 0xff;
}

int16_t gpio_IO (uint8_t port,uint8_t pin,int16_t op )
{ 
  unsigned long pio = 1 << pin;
  int ret = 1;
  switch( op )
  { 
    case PIN_NOPULL:
        if(Pin_FUNC (port, pin,0x00, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS )==0x00){
           Chip_GPIO_SetDir(LPC_GPIO_PORT, port, pio, 0);
        }else {
           fprintf(stderr,"Error en la configuracion I/O\n");
        }
    break;
    case PIN_PULLUP:
      if(Pin_FUNC (port, pin,0x00, SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS )==0x00){
         Chip_GPIO_SetDir(LPC_GPIO_PORT, port, pio, 0);
      }else {
         fprintf(stderr,"Error en la configuracion I/O\n");
      }
    break;         

    case PIN_PULLDOWN:
      if(Pin_FUNC (port, pin,0x00, SCU_MODE_PULLDOWN | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS )==0x00){
         Chip_GPIO_SetDir(LPC_GPIO_PORT, port, pio, 0);
      }else {
         fprintf(stderr,"Error en la configuracion I/O\n");
     }
    break;

    case PIN_OUTPUT:                             
      if(Pin_FUNC (port, pin,0x01, SCU_MODE_PULLUP)==0x00){      
         Chip_GPIO_SetDir(LPC_GPIO_PORT, port, pio, 1);
      }else {
         fprintf(stderr,"Error en la configuracion I/O\n");
      }      
    break;

    case PIN_INPUT:                               
      if(Pin_FUNC (port, pin,0x00, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS )==0x00){
         Chip_GPIO_SetDir(LPC_GPIO_PORT, port, pio, 0);
      }else {
         fprintf(stderr,"Error en la configuracion I/O\n");
      }
    break;

    case PIN_GETVAL:        
      ret = ( Chip_GPIO_ReadValue(LPC_GPIO_PORT, port) & pio ) ? 1 : 0;
    break;

    case PIN_SET:
      Chip_GPIO_SetValue(LPC_GPIO_PORT, port, pio);
    break;

    case PIN_CLEAR:
      Chip_GPIO_ClearValue (LPC_GPIO_PORT, port, pio);
    break;

/**********************************************************************/
    case PORT_SETVAL:
      Chip_GPIO_SetValue(LPC_GPIO_PORT, port, pin);
    break;

    case PORT_OUTPUT:
    	Chip_GPIO_SetPortDIR(LPC_GPIO_PORT, port, pin, TRUE);
    break;

    case PORT_INPUT:
      Chip_GPIO_SetPortDIR(LPC_GPIO_PORT, port, pin, FALSE);
    break;

    case PORT_GET:
      ret = Chip_GPIO_ReadValue(LPC_GPIO_PORT, port);
    break;

    default:
      return 0;
  }
  return ret;
}

//Devuelve la posision de texto buscado, en la estructura (texto a buscar, estruct, size)
uint16_t pars (char *text, pin_id *d,uint8_t camp)
{
  uint8_t i,j;
  uint16_t dat;
  for(j=0;j<camp;j++)
  {
    dat=1;
	for(i=0;i<MAXCHAR;i++)
	{
	  if(text[i]!=d[j].module[i])
	  {
		  dat=0xFFFF;
		  break;
	  }
	  if(text[i]==0)
		  break;
	}
	if(dat==1)
	{
		dat=j;
		break;
	}
  }
  return dat;
}

/*Configura un pin como salida y le asigna un valor, usado en SPI como slave select
 *
 * Para buscar un Id en tabla se pasa (p=nombre como parametro,v=estado del pin a configurar,conf=1)
 * Si queremos escibir un estado de un pin con un Id buscado (p=NULL, )
 */
uint16_t PIO_SET(const int8_t *p,uint8_t v,uint8_t conf){
	uint16_t id_m,t;
	uint8_t gpin,gport;
    if(conf>1){//Pin Seteado anteriormente
    	id_m=conf-1;
    	if(PinToR(pin_ID[id_m].port,pin_ID[id_m].pin,&gport,&gpin)==0)
    		return 0xFFFF; //Error en la configuracion pasada
		if(v==0) (void)gpio_IO(gport,gpin,PIN_CLEAR);
		else (void)gpio_IO(gport,gpin,PIN_SET);
    }else{
    	t=(sizeof(pin_ID)/sizeof(pin_id));
    	id_m=pars(p,pin_ID,t);
    	if (id_m==0xFFFF)
    		return 0xFFFF; //error en el pin buscado
    	PinToR(pin_ID[id_m].port,pin_ID[id_m].pin,&gport,&gpin);
    	if(conf==0){//pone a 1 o 0 el pin
    		if(v==0) (void)gpio_IO(gport,gpin,PIN_CLEAR);
    		else (void)gpio_IO(gport,gpin,PIN_SET);
    	}else{ //Configura el pin como OUT
    		(void)gpio_IO(gport,gpin,PIN_OUTPUT);
    		Chip_GPIO_SetPinState(LPC_GPIO_PORT, pin_ID[id_m].port, pin_ID[id_m].pin, (bool) TRUE);
    	}
    }
	return id_m; //Id del pin buscado
}

void get_pin_port(uint16_t pos,uint8_t *port,uint8_t *pin){
	PinToR(pin_ID[pos].port,pin_ID[pos].pin,port,pin);
}
