/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <chip.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "chip.h"
#include <i2c_drv.h>



static I2CM_XFER_T i2c_ref[2];
static int int_en[2];
static uint8_t txbf[2];
//-------------------------------------------------------------------------------------------------------
void I2C0_IRQHandler(void)
{
	Chip_I2CM_XferHandler(LPC_I2C0, &i2c_ref[0]);
}

void I2C1_IRQHandler (void)
{
	Chip_I2CM_XferHandler(LPC_I2C1, &i2c_ref[1]);
}

static int Handle_IRQ_i2c(uint8_t devAddr, uint8_t *txBuf, uint16_t txSz,uint8_t *rxBuf, uint16_t rxSz,I2C_ID_T id)
{
	i2c_ref[id].slaveAddr = devAddr;
	i2c_ref[id].options = 0;
	i2c_ref[id].status = 0;
	i2c_ref[id].txSz = txSz;
	i2c_ref[id].rxSz = rxSz;
	i2c_ref[id].txBuff = txBuf;
	i2c_ref[id].rxBuff = rxBuf;
	Chip_I2CM_Xfer(id == I2C0 ? LPC_I2C0 : LPC_I2C1, &i2c_ref[id]);
	while (i2c_ref[id].status == I2CM_STATUS_BUSY) {
		__WFI();
	}
	return ((txSz+rxSz) - i2c_ref[id].rxSz);
}


//-------------------------------------------------------------------------------------------------------


void i2c_init(I2C_ID_T id)
{
	if (id == I2C1) {//I2C1 P2.3 P2.4
		Chip_SCU_PinMuxSet(0x2, 3, (SCU_MODE_ZIF_DIS | SCU_MODE_INBUFF_EN | SCU_MODE_FUNC1));
		Chip_SCU_PinMuxSet(0x2, 4, (SCU_MODE_ZIF_DIS | SCU_MODE_INBUFF_EN | SCU_MODE_FUNC1));
	}
	else {//I2C0 P9.3 P9.4
		Chip_SCU_I2C0PinConfig(I2C0_STANDARD_FAST_MODE);
	}
    Chip_I2C_Init(id);

}
void set_i2c(I2C_ID_T id,int poll,int speed){
	Chip_I2C_SetClockRate(id, speed);
	if (!poll) {
		//mode_poll &= ~(1 << id);
		Chip_I2C_SetMasterEventHandler(id, Chip_I2C_EventHandler);
		NVIC_EnableIRQ(id == I2C0 ? I2C0_IRQn : I2C1_IRQn);
        int_en[id]=1;
	}
	else {
		//mode_poll |= 1 << id;
		NVIC_DisableIRQ(id == I2C0 ? I2C0_IRQn : I2C1_IRQn);
		Chip_I2C_SetMasterEventHandler(id, Chip_I2C_EventHandlerPolling);
        int_en[id]=0;
	}
}

int i2c_pl_wite(u_char mode,I2C_ID_T port,u_char addr,u_char *buf,u_int len){
	if(len==0) return 1;
	if(mode == MASTER){
		if (int_en[port] == 0)
			return Chip_I2C_MasterSend(port, addr, buf, len);
		else
			return Handle_IRQ_i2c(addr,buf,len,txbf,2,port);}
	else return 1;
}


int i2c_pl_read(u_char mode,I2C_ID_T port,u_char addr,u_char *buf,u_int len){
	if(len==0)return 1;
	if(mode == MASTER){
		if (int_en[port] == 0)
			return Chip_I2C_MasterRead(port, addr, buf, len);
		else
			return Handle_IRQ_i2c(addr,txbf,2,buf,len,port);}
	else return 1;
}

