/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <eeprom_drv.h>
#include <stdint.h>
#include <chip.h>
#include <fs.h>
#include <datBoxFs.h>

void plt_eeprom_init(void)
{
  Chip_EEPROM_Init(LPC_EEPROM);
  Chip_EEPROM_SetAutoProg(LPC_EEPROM,EEPROM_AUTOPROG_AFT_1WORDWRITTEN);
}

void plt_eeprom_read (struct stDbFsStruct *unUsed, uint8_t * ptr, uint32_t offset, uint32_t size, void * baseAddr)
{
  uint32_t i = 0;
  uint8_t *addrData;
  addrData = (uint8_t *)(offset + baseAddr);
  uint8_t * dstData;
  dstData = (uint8_t *)ptr;
  for(i = 0; i < size; i++)
    dstData[i] = addrData[i];
}

void plt_eeprom_write(struct stDbFsStruct * unUsed, uint32_t offset, uint8_t * ptr, uint32_t size, void * baseAddr)
{
  uint32_t i = 0;
  uint8_t *addrData;
  uint8_t *dstData;
  dstData = (uint8_t *)ptr;
  addrData = (uint8_t *)(offset + baseAddr);
  for(i = 0; i < size/4; i++)
  {
	  ((uint32_t*)addrData)[i] = ((uint32_t *)dstData)[i];
	  Chip_EEPROM_WaitForIntStatus(LPC_EEPROM, EEPROM_INT_ENDOFPROG);
  }
}

void plt_eeprom_erase(uint32_t addr, uint32_t size)
{
  uint32_t i = 0;
  uint8_t *addrData;
  addrData = (uint8_t *)addr;
  for(i = 0; i < size; i++)
  {
    addrData[i] = 0xFF;
  }
  Chip_EEPROM_EraseProgramPage(LPC_EEPROM);
}



