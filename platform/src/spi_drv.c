/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "spi_drv.h"
#include "chip.h"

uint8_t init=0;


void spi_read(uint8_t *buffer, uint32_t len){
	Chip_SSP_ReadFrames_Blocking(LPC_SSP1,buffer, len);
}

void spi_write(const uint8_t *buffer, uint16_t len){
	Chip_SSP_WriteFrames_Blocking(LPC_SSP1, buffer, len);
}


void plt_conf_spi(uint16_t bit,uint16_t mode,uint32_t speed, uint8_t nmode, SPI_conf *spi)
{
	uint8_t ssp_mode;
	SPI_conf conf;
//Respalda los registros configurados previamente
	conf.CR0=LPC_SSP1->CR0;
	conf.CR1=LPC_SSP1->CR1;
	conf.CPSR=LPC_SSP1->CPSR;
	if(mode==0)
		Chip_SSP_Set_Mode(LPC_SSP1, SSP_MODE_MASTER);
	else
		Chip_SSP_Set_Mode(LPC_SSP1, SSP_MODE_SLAVE);
	switch(nmode){
	case 0:
		ssp_mode=SSP_CLOCK_MODE0;
		break;
	case 1:
		ssp_mode=SSP_CLOCK_MODE1;
		break;
	case 2:
		ssp_mode=SSP_CLOCK_MODE2;
		break;
	case 3:
		ssp_mode=SSP_CLOCK_MODE3;
		break;
	}
	Chip_SSP_SetFormat(LPC_SSP1, bit, SSP_FRAMEFORMAT_SPI, ssp_mode);
	Chip_SSP_SetBitRate(LPC_SSP1,speed);
    Chip_SSP_Enable(LPC_SSP1);

//Salva los registros de la configiracion actual
	spi->CR0=LPC_SSP1->CR0;
	spi->CR1=LPC_SSP1->CR1;
	spi->CPSR=LPC_SSP1->CPSR;
//Restaura los registros
	LPC_SSP1->CR0=conf.CR0;
	LPC_SSP1->CR1=conf.CR1;
	LPC_SSP1->CPSR=conf.CPSR;
}
//--------------------------------------------------------------------------------

uint16_t plt_spi_read(SPI_conf *spi,uint8_t *buff,uint16_t count){
	SPI_conf conf;
	uint16_t ret;
	conf.CR0=LPC_SSP1->CR0;
	conf.CR1=LPC_SSP1->CR1;
	conf.CPSR=LPC_SSP1->CPSR;
	LPC_SSP1->CR0=spi->CR0;
	LPC_SSP1->CR1=spi->CR1;
	LPC_SSP1->CPSR=spi->CPSR;
	ret=Chip_SSP_ReadFrames_Blocking(LPC_SSP1,buff, count);
	LPC_SSP1->CR0=conf.CR0;
	LPC_SSP1->CR1=conf.CR1;
	LPC_SSP1->CPSR=conf.CPSR;
	return ret;
}


uint16_t plt_spi_write(SPI_conf *spi,uint8_t *buff,uint16_t count){
	SPI_conf conf;
	uint16_t ret;
	conf.CR0=LPC_SSP1->CR0;
	conf.CR1=LPC_SSP1->CR1;
	conf.CPSR=LPC_SSP1->CPSR;
	LPC_SSP1->CR0=spi->CR0;
	LPC_SSP1->CR1=spi->CR1;
	LPC_SSP1->CPSR=spi->CPSR;
	ret=Chip_SSP_WriteFrames_Blocking(LPC_SSP1, buff, count);
	LPC_SSP1->CR0=conf.CR0;
	LPC_SSP1->CR1=conf.CR1;
	LPC_SSP1->CPSR=conf.CPSR;
	return ret;

}
//--------------------------------------------------------------------------------
void plt_spi_init(void){/* PF.4 => SCK1  P1.4 => MOSI1  P1.3 => MISO1 */
	if(init==0){
		Chip_SCU_PinMuxSet(0xF, 4, (SCU_PINIO_FAST | SCU_MODE_FUNC0));//SCL
	//	Chip_SCU_PinMuxSet(0x1, 19, (SCU_PINIO_FAST | SCU_MODE_FUNC1));//SCL
		Chip_SCU_PinMuxSet(0x1, 20, (SCU_PINIO_FAST | SCU_MODE_FUNC1));//SSEL1
		Chip_SCU_PinMuxSet(0x1, 4, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5));//MOSI
		Chip_SCU_PinMuxSet(0x1, 3, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5));//MISO
		Chip_SSP_Init(LPC_SSP1);
	    init=1;
	}
}

void spi_mem_init(void){
	plt_spi_init();
	Chip_SSP_Set_Mode(LPC_SSP1, SSP_MODE_MASTER);
	Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0);
	Chip_SSP_SetBitRate(LPC_SSP1,15000000); //15Mhz
    Chip_SSP_Enable(LPC_SSP1);
    NVIC_EnableIRQ(SSP1_IRQn);
}

//----------   SPI ISR---------------------
#define SSP_DATA_BYTES(databits)            (((databits) > SSP_BITS_8) ? 2:1)
void SSP1_IRQHandler(void)
{
	Chip_SSP_Int_Disable(LPC_SSP1);	/* Disable SSP interrupt */
}
