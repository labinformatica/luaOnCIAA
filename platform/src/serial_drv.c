/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "serial_drv.h"
#include "config.h"
#include "chip.h"
#include <stdint.h>

static unsigned char rxBuff[defaultSerialRxBuffSz];
static unsigned char txBuff[defaultSerialTxBuffSz];
static RINGBUFF_T txRing, rxRing;

static LPC_USART_T* pltUsartList[4] = 
{ 
	LPC_USART0, 
	LPC_UART1, 
	LPC_USART2, 
	LPC_USART3 
};
typedef struct
{
  unsigned char txPort;
  unsigned int txPin;
  unsigned char txFunc;
  unsigned char rxPort;
  unsigned int rxPin;
  unsigned char rxFunc;
} tusartPinCfg;

static tusartPinCfg pltUsartPins [4] = 
{// txPort | txPin | txFunc | rxPort | rxPin | rxFunc
 {       9,      5,   FUNC7,       9,      6,   FUNC7}, // usart0 -> RS-485 port
 {       3,      4,   FUNC4,       3,      5,   FUNC4}, // usart1 -> P3.4 y P3.5 (no conectados
 {       7,      1,   FUNC6,       7,      2,   FUNC6}, // usart2 -> USB Adapter
 {       2,      3,   FUNC2,       2,      4,   FUNC2}  // usart3 -> Rs232 port
};

const unsigned int UARTIRQs[4] = {24, 25, 26, 27};

unsigned int plt_configure_usart(unsigned int id, 
				 unsigned int baud, 
			         tplt_usart_data_bits dBits, 
				 tplt_usart_parity parity, 
                                 tplt_usart_stop_bits stopBits)
{
  unsigned int ret = RETURN_OK;
  unsigned int cfg = 0;
  switch( dBits )
  {
    case PLT_USART_DATA_5:
      cfg |= UART_LCR_WLEN5;
      break;

    case PLT_USART_DATA_6:
      cfg |= UART_LCR_WLEN6;
      break;

    case PLT_USART_DATA_7:
      cfg |= UART_LCR_WLEN7;
      break;

    case PLT_USART_DATA_8:
      cfg |= UART_LCR_WLEN8;
      break;

    default:
      ret = RETURN_FAIL;
      break;
  }

  if( stopBits == PLT_USART_STOP_BIT_2 )
    cfg |= UART_LCR_SBS_2BIT;
  else if( stopBits == PLT_USART_STOP_BIT_1 )
    cfg |= UART_LCR_SBS_1BIT;
  else
    ret = RETURN_FAIL;

  switch( parity )
  {
    case PLT_USART_PARITY_NONE:
      cfg |= UART_LCR_PARITY_DIS;
      break;

    case PLT_USART_PARITY_ODD:
      cfg |= UART_LCR_PARITY_ODD | UART_LCR_PARITY_EN;
      break;

    case PLT_USART_PARITY_EVEN:
      cfg |= UART_LCR_PARITY_EVEN | UART_LCR_PARITY_EN;
      break;

    case PLT_USART_PARITY_MARK:
      cfg |= UART_LCR_PARITY_F_1 | UART_LCR_PARITY_EN;
      break;

    case PLT_USART_PARITY_SPACE:
      cfg |= UART_LCR_PARITY_F_0 | UART_LCR_PARITY_EN;
      break;

    default:
      ret = RETURN_FAIL;
      break;
  }

  // TO DO: utilizar el valor retornado para verificar el éxito de las funciones invocadas.

  Chip_UART_Init(pltUsartList[ id ]);
  Chip_UART_ConfigData(pltUsartList[ id ], cfg);
  Chip_UART_SetBaud(pltUsartList[id], baud);
  Chip_UART_SetupFIFOS(pltUsartList[id], UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3);
  Chip_SCU_PinMux(pltUsartPins[id].txPort, pltUsartPins[id].txPin, MD_PDN,              pltUsartPins[id].txFunc); 
  Chip_SCU_PinMux(pltUsartPins[id].rxPort, pltUsartPins[id].rxPin, MD_PLN|MD_EZI|MD_ZI, pltUsartPins[id].rxFunc);
  Chip_UART_TXEnable(pltUsartList[id]);
  return ret;
}

unsigned int plt_configure_default_usart(unsigned int id, 
				 unsigned int baud, 
			         tplt_usart_data_bits dBits, 
				 tplt_usart_parity parity, 
                                 tplt_usart_stop_bits stopBits)
{
  unsigned int ret = plt_configure_usart(id, baud, dBits, parity, stopBits);
  Chip_UART_TXDisable(pltUsartList[id]);
  RingBuffer_Init(&rxRing, rxBuff, 1, defaultSerialRxBuffSz);
  RingBuffer_Init(&txRing, txBuff, 1, defaultSerialTxBuffSz);
  Chip_UART_SetupFIFOS(pltUsartList[id], (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
  Chip_UART_IntEnable(pltUsartList[id], (UART_IER_RBRINT | UART_IER_RLSINT));
  NVIC_EnableIRQ(UARTIRQs[id]);  
  Chip_UART_TXEnable(pltUsartList[id]);
  return ret;
}

void defaultUsartISR (void)
{
  Chip_UART_IRQRBHandler(DEFAULT_UART_LPC, &rxRing, &txRing);
}

unsigned int plt_send_default_usart(
			   const uint8_t *data,
			    unsigned int dataLen)
{
	unsigned int transmited = 0;
	unsigned int toInsert;
	unsigned int freeSpc;
	while(transmited < dataLen)
	{
      freeSpc = RingBuffer_GetFree(&txRing);
      while(!freeSpc)
      {
    	  freeSpc = RingBuffer_GetFree(&txRing);
      }
	  toInsert = ((freeSpc > dataLen) ? dataLen : freeSpc);
      Chip_UART_SendRB(DEFAULT_UART_LPC, &txRing, &data[transmited], toInsert);
      transmited += toInsert;
	}

	return transmited;
}

unsigned int plt_recv_default_usart(
				uint8_t *data,
			    unsigned int maxData)
{
  return Chip_UART_ReadRB(DEFAULT_UART_LPC, &rxRing, data, maxData);
}


