/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <tmr_drv.h>
#include <config.h>
#include <chip.h>
#include <stdint.h>
#include <time.h>

static LPC_TIMER_T *tmrId[4] =
{
  LPC_TIMER0,
  LPC_TIMER1,
  LPC_TIMER2,
  LPC_TIMER3
};

static uint32_t tmrClk[4] =
{
   CLK_MX_TIMER0,
   CLK_MX_TIMER1,
   CLK_MX_TIMER2,
   CLK_MX_TIMER3
};

static uint32_t tmrRst[4] =
{
		RGU_TIMER0_RST,
		RGU_TIMER1_RST,
		RGU_TIMER2_RST,
		RGU_TIMER3_RST
};

static uint32_t tmrIRQ[4] =
{
		12,
		13,
		14,
		15
};
// TIMER1_IRQn
static uint64_t clockCounter=0;
//static uint32_t clockspi;

void SYSTEM_TMR_ISR(void)
{
	if (Chip_TIMER_MatchPending(tmrId[SYSTEM_TMR], SYSTEM_TMR))
	{
		Chip_TIMER_ClearMatch(tmrId[SYSTEM_TMR], SYSTEM_TMR);
		clockCounter++;
//		clockspi++;
//		if(clockspi<100)
//		{
//			Chip_GPIO_SetValue(LPC_GPIO_PORT, 0x1, 1 << 11);
//		}else{
//			Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0x1, 1 << 11);
//			if(clockspi>200)clockspi=0;
//		}
	}
}


static uint64_t STick=0;
void SysTick_Handler(void)
{
	STick++;
}

uint64_t getSysTick(void){
	return STick; //1 tick = 1ms
}
uint64_t getTick(void){
	return clockCounter; //1 tick = 1ms
}

void plt_sys_tmr_init(void)
{
	uint32_t tmrFrec;
	Chip_TIMER_Init(tmrId[SYSTEM_TMR]);
	Chip_RGU_TriggerReset(tmrRst[SYSTEM_TMR]);
	while (Chip_RGU_InReset(tmrRst[SYSTEM_TMR])) {}

	tmrFrec = Chip_Clock_GetRate(tmrClk[SYSTEM_TMR]);
	Chip_TIMER_Reset(tmrId[SYSTEM_TMR]);
	Chip_TIMER_MatchEnableInt(tmrId[SYSTEM_TMR], SYSTEM_TMR);

	Chip_TIMER_SetMatch(tmrId[SYSTEM_TMR], SYSTEM_TMR, (tmrFrec / CLOCKS_PER_SEC_1));

	Chip_TIMER_ResetOnMatchEnable(tmrId[SYSTEM_TMR], SYSTEM_TMR);
	Chip_TIMER_Enable(tmrId[SYSTEM_TMR]);
	NVIC_EnableIRQ(tmrIRQ[SYSTEM_TMR]);
	NVIC_ClearPendingIRQ(tmrIRQ[SYSTEM_TMR]);

#if defined (CORE_M4)
	SysTick_Config(SystemCoreClock / TICKRATE_HZ);
#endif
}

uint32_t plt_sys_tmr_read(void)
{
	return clockCounter;
}

uint64_t plt_sys_tmr_getSegCounter(void)
{
	return clockCounter/CLOCKS_PER_SEC;
}
