/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "lpc4337_ipc.h"
#include <chip.h>
#include <ipc_msg.h>
#include <lpc_types.h>
#include <stdint.h>

//#include <stdlib.h>
//#include <stdio.h>
//#include "ipc.h"

//Defino la siguiente enumeración para trabajar el vector de colas
typedef enum CPUID {
	CPUID_MIN,
	CPUID_M4 = CPUID_MIN,
	CPUID_M0APP,
	CPUID_M0SUB,
	CPUID_MAX
} CPUID_T;

//#define CORE_M4
#ifdef CORE_M4

#include <core_cm4.h>
#define CPUID_CURR CPUID_M4


//#endif

//Funcion: 0
void conmutar_led_m4(tipo_dir dir)
{
	Chip_GPIO_SetPinToggle(LPC_GPIO_PORT,dir.puerto, dir.pin);

	return;
}

//Funcion: 1
void estado_led_m4(tipo_dir dir)
{
	bool a;
	tipo_msg_m0 msg;
//
//	a=Chip_GPIO_GetPinState(LPC_GPIO_PORT,dir.puerto, dir.pin);
	if(((1<<dir.pin)&(LPC_GPIO_PORT->SET[dir.puerto]))==0)
//	if(((1<<dir.pin)&(Chip_GPIO_GetMaskedPortValue(LPC_GPIO_PORT,dir.puerto)))==0)
		a=false;
	else
		a=true;

	msg.cpu = CPUID_M0APP;
	msg.funId=0;
	msg.verif=a;

	IPC_pushMsg(msg.cpu, &msg);

	return;
}

uint16_t *contador = (uint16_t *)0x10089FFE;

//Funcion: 2
void iniciar_contador_m4(tipo_dir dir)
{
	*contador = 0;
#ifdef CORE_M4
	NVIC_EnableIRQ(TIMER1_IRQn);
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
#endif

	return;
}

//Funcion: 3
void valor_contador_m4(tipo_dir dir)
{
	tipo_msg_m0 msg;

	msg.cpu = CPUID_M0APP;

	msg.funId = *contador;

//#ifdef CORE_M4
	if( NVIC_GetActive(TIMER1_IRQn)==0 )
		msg.verif = false; //la cuenta esta detenida
	else
		msg.verif = true; //la cuenta esta activa
//#endif

	IPC_pushMsg(msg.cpu, &msg);

	return;
}

//Funcion: 4
void pausar_contador_m4(tipo_dir dir)
{
#ifdef CORE_M4
	NVIC_DisableIRQ(TIMER1_IRQn);
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
#endif

	return;
}

//Funcion: 5
void reiniciar_contador_m4(tipo_dir dir)
{
#ifdef CORE_M4
	NVIC_EnableIRQ(TIMER1_IRQn);
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
#endif
	return;
}

//Funcion: 6
void restaurar_contador_m4(tipo_dir dir)
{
//#ifdef CORE_M4
	NVIC_DisableIRQ(TIMER1_IRQn);
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
//#endif

	*contador = 0;

//#ifdef CORE_M4
	NVIC_EnableIRQ(TIMER1_IRQn);
	NVIC_ClearPendingIRQ(TIMER1_IRQn);
//#endif

	return;
}


#endif
//void (*funcion[])(tipo_dir)={conmutar_led_m4, estado_led_m4};

//void M0CORE_IRPHandler(void)
//{
//	tipo_msg_m4 msg;
//
//	Chip_CREG_ClearM0AppEvent();
//
//	if(IPC_tryPopMsg(&msg)!=QUEUE_VALID) return;
//
//	funcion[msg.funId](msg.carga);
//
//	return;
//}


#ifdef CORE_M0

#include <core_cm0.h>
#define CPUID_CURR CPUID_M0

#endif

void conmutar_led_m0(uint8_t puerto, uint8_t pin)
{
	tipo_msg_m4 msg;

	msg.cpu=CPUID_M4;
	msg.funId = 0;
	msg.carga.puerto = puerto;
	msg.carga.pin = pin;

	IPC_tryPushMsg(msg.cpu, &msg);

	return;
}


bool estado_led_m0(uint8_t puerto, uint8_t pin)
{
	tipo_msg_m4 msg;
	tipo_msg_m0 msg_m0;

	msg.cpu = CPUID_M4;
	msg.funId = 1;
	msg.carga.puerto = puerto;
	msg.carga.pin = pin;

	IPC_pushMsg(msg.cpu, &msg);

	IPC_popMsg(&msg_m0);

	return msg_m0.verif;
}

static tipo_msg_m0 cola[CANT_MENSAJES];

void inicializar_cola_m0(void)
{
	IPC_initMsgQueue(cola, sizeof(tipo_msg_m0), CANT_MENSAJES);
	return;
}

bool verificar_cola_m0(void)
{
	int cant;
	cant = IPC_msgPending(CPUID_M0APP);
	if(cant == QUEUE_ERROR)
		return false;
	else
		return true;
}

int mensajes_pendientes_m0(void)
{
	return IPC_msgPending(CPUID_M0APP);
}


void hacer_pop(void)
{
	tipo_msg_m0 msg;
	IPC_popMsg(&msg);
	return;
}

void iniciar_contador_m0(void)
{
	tipo_msg_m4 msg;

	msg.cpu = CPUID_M4;
	msg.funId = 2;
	msg.carga.puerto = 0;
	msg.carga.pin = 0;

	IPC_pushMsg(msg.cpu, &msg);

	return;
}

uint16_t valor_contador_m0(void)
{
	tipo_msg_m4 msg;
	tipo_msg_m0 msg_m0;

	msg.cpu = CPUID_M4;
	msg.funId = 3;
	msg.carga.puerto = 0;
	msg.carga.pin = 0;

	IPC_pushMsg(msg.cpu, &msg);

	IPC_popMsg(&msg_m0);

	return msg_m0.funId;
}

void pausar_contador_m0(void)
{
	tipo_msg_m4 msg;

	msg.cpu = CPUID_M4;
	msg.funId = 4;
	msg.carga.puerto = 0;
	msg.carga.pin = 0;

	IPC_pushMsg(msg.cpu, &msg);

	return;
}

void reiniciar_contador_m0(void)
{
	tipo_msg_m4 msg;

	msg.cpu = CPUID_M4;
	msg.funId = 5;
	msg.carga.puerto = 0;
	msg.carga.pin = 0;

	IPC_pushMsg(msg.cpu, &msg);

	return;
}

void restaurar_contador_m0(void)
{
	tipo_msg_m4 msg;

	msg.cpu = CPUID_M4;
	msg.funId = 6;
	msg.carga.puerto = 0;
	msg.carga.pin = 0;

	IPC_pushMsg(msg.cpu, &msg);

	return;
}
