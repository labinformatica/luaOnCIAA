/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <chip.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "adc_drv.h"


ADC_CLOCK_SETUP_T adc_clock;

/* Configuracion del ADC correspondiente e inicializacion del canal con valores Default */
unsigned char CONF_adcCHNL (unsigned char adc, FunctionalState state){

  if(adc == 0 && state == ENABLE){

	  Chip_ADC_Init(LPC_ADC0, &adc_clock);						// Inicializa el ADC0

	  Chip_ADC_SetBurstCmd( LPC_ADC0, DISABLE );

	  Chip_ADC_SetSampleRate( LPC_ADC0, &adc_clock, ADC_MAX_SAMPLE_RATE );

	  Chip_ADC_EnableChannel( LPC_ADC0,ADC_CH1, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC0, ADC_CH1, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC0, ADC_CH2, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC0, ADC_CH2, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC0, ADC_CH3, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC0, ADC_CH3, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC0, ADC_CH4, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC0, ADC_CH4, DISABLE );

	  return 0;
  }
  else if (adc == 0 && state == DISABLE){
	  Chip_ADC_DeInit( LPC_ADC0 );
	  return 0;
  }

  if(adc == 1 && state == ENABLE){
	  Chip_ADC_Init(LPC_ADC1, &adc_clock);						// Inicializa el ADC0

	  Chip_ADC_SetBurstCmd( LPC_ADC1, DISABLE );

	  Chip_ADC_SetSampleRate( LPC_ADC1, &adc_clock, ADC_MAX_SAMPLE_RATE );

	  Chip_ADC_EnableChannel( LPC_ADC1,ADC_CH1, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC1, ADC_CH1, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC1, ADC_CH2, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC1, ADC_CH2, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC1, ADC_CH3, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC1, ADC_CH3, DISABLE );

	  Chip_ADC_EnableChannel( LPC_ADC1, ADC_CH4, DISABLE );
	  Chip_ADC_Int_SetChannelCmd( LPC_ADC1, ADC_CH4, DISABLE );

	  return 0;
  }
  else if (adc == 1 && state == DISABLE){
  	  Chip_ADC_DeInit( LPC_ADC1 );

  	  return 0;
  }
  return 0xFF;
}

/* Configuracion de Sample Rate y Resolucion */
unsigned char CONF_adcRATE (unsigned char adc, uint32_t rate, unsigned char resol){

	if(adc == 0){												// Configura Sample Rate y Resolucion del ADC0
		Chip_ADC_SetSampleRate(LPC_ADC0, &adc_clock, rate);
		Chip_ADC_SetResolution(LPC_ADC0, &adc_clock, resol);
		return 0x00;
	}
	if(adc == 1){												// Configura Sample Rate y Resolucion del ADC1
		Chip_ADC_SetSampleRate(LPC_ADC1, &adc_clock, rate);
		Chip_ADC_SetResolution(LPC_ADC1, &adc_clock, resol);
		return 0x00;
	}
	return 0xFF;
}

/* Devuelve el dato leido al puntero data. */
uint16_t READ_adcVAL (unsigned char adc, uint8_t channel){

	uint16_t analogValue = 0;

	if(adc == 0){

		Chip_ADC_EnableChannel(LPC_ADC0, channel, ENABLE);
		Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

		 while(
		      (Chip_ADC_ReadStatus(LPC_ADC0, channel, ADC_DR_DONE_STAT) != SET)
		);

		Chip_ADC_ReadValue( LPC_ADC0, channel, &analogValue );

		Chip_ADC_EnableChannel( LPC_ADC0, channel, DISABLE );

		return analogValue;
	}
	else if(adc == 1){

		Chip_ADC_EnableChannel(LPC_ADC1, channel, ENABLE);
		Chip_ADC_SetStartMode(LPC_ADC1, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

		while(
				(Chip_ADC_ReadStatus(LPC_ADC1, channel, ADC_DR_DONE_STAT) != SET)
		);

		Chip_ADC_ReadValue( LPC_ADC1, channel, &analogValue );

		Chip_ADC_EnableChannel( LPC_ADC1, channel, DISABLE );

		return analogValue;
	}
	return 0xFF;
}



