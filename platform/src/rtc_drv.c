/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <chip.h>
#include "rtc_drv.h"
#include "rtc_18xx_43xx.h"
#include "config.h"

void plt_rtc_init(void)
{
	if(Chip_REGFILE_Read(LPC_REGFILE ,0)!=RTC_const)
	{
		Chip_RTC_Init(LPC_RTC);
		Chip_REGFILE_Write(LPC_REGFILE ,0,RTC_const);
	}
	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

void plt_rtc_stime(int s, int m, int h)
{
	RTC_TIME_T dateTime;
	Chip_RTC_GetFullTime(LPC_RTC, &dateTime);
	dateTime.time[RTC_TIMETYPE_SECOND] = s;
	dateTime.time[RTC_TIMETYPE_MINUTE] = m;
	dateTime.time[RTC_TIMETYPE_HOUR] = h;
	Chip_RTC_SetFullTime(LPC_RTC, &dateTime);
}

void plt_rtc_sdate(int d, int m, int y)
{
	RTC_TIME_T dateTime;
	Chip_RTC_GetFullTime(LPC_RTC, &dateTime);
	dateTime.time[RTC_TIMETYPE_DAYOFMONTH] = d;
	dateTime.time[RTC_TIMETYPE_MONTH] = m;
	dateTime.time[RTC_TIMETYPE_YEAR] = y;
	Chip_RTC_SetFullTime(LPC_RTC, &dateTime);
}

void plt_rtc_gtime(int *s, int *m, int *h)
{
	RTC_TIME_T dateTime;
	Chip_RTC_GetFullTime(LPC_RTC, &dateTime);
	*s = dateTime.time[RTC_TIMETYPE_SECOND];
	*m = dateTime.time[RTC_TIMETYPE_MINUTE];
	*h = dateTime.time[RTC_TIMETYPE_HOUR];
}

void plt_rtc_gdate(int *d, int *m, int *y)
{
	RTC_TIME_T dateTime;
	Chip_RTC_GetFullTime(LPC_RTC, &dateTime);
	*d = dateTime.time[RTC_TIMETYPE_DAYOFMONTH];
	*m = dateTime.time[RTC_TIMETYPE_MONTH];
	*y = dateTime.time[RTC_TIMETYPE_YEAR];
}
