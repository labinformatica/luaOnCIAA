/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <spi_memory_drv.h>
#include <chip.h>
#include <spi_drv.h>
#include <datBoxFs.h>
#include <gpio_drv.h>
#include <spi_drv.h>
#include <stdlib.h>

#define buffsize 256 //la pagina es de 255 Byte
#define PIN_SS "LCD_EN"
#define cache 4096

static uint8_t buf_tx[10],cache_write;
static uint8_t id_pin=0;
static uint8_t *cache_spi;//[cache];
static uint32_t sector_buffer;
//---------------------------------------------------------------------------------------------

static void Gpio_SC(uint8_t p){
switch (p){
	case 0:
		(void)PIO_SET(PIN_SS,0,1+id_pin);
    break;
	case 2:
		id_pin=PIO_SET(PIN_SS,0,1);
	    break;
	default:
		(void)PIO_SET(PIN_SS,1,1+id_pin);
	break;
 }
}

//--------------------------------------------------------------------------------
#include <defFilesRom.h>
#include <datBoxFs.h>

#define blkSzFLASH 256
unsigned char rwBuffFLASH[blkSzFLASH];
dbfsRoot dbfsFLASH =
{
  .absAddr = (void *)0x00000000,
  .length = 0x100000, //1MB
  .rwBuff = rwBuffFLASH,
  .fcnOpn = NULL,
  .fcnWr = plt_spiflash_write,
  .fcnRd = plt_spiflash_read,
  .fcnCls = plt_spiflash_close,
  .maxFiles = 32,
  .blkSize = blkSzFLASH

};

struct stFileSystems fsFLASH =
{
		.fsName = "/flash",
	    .openDir = dbfsOpenDir,
	    .readDir = dbfsReadDir,
	    .closeDir = dbfsCloseDir,
	    .openFile = dbfsOpen,
	    .readFile = dbfsRead,
	    .writeFile = dbfsWrite,
	    .seekFile = dbfsSeek,
	    .closeFile = dbfsClose,
	    .unlink = dbfsUnlink,
		.format = dbfsFormat,
	    .stat = NULL,
	    .fsData = &dbfsFLASH
};
void init_flash_memory(void){
	uint16_t Id;
	spi_mem_init(); //inicializa SPI a 15Mhz
	Gpio_SC(2); //LCD_EN=1 --> SPI->CS
	get_ID(buf_tx);
	if((buf_tx[0]!=0xFF)&&(buf_tx[0]!=0x00)){
		printf("\nMemoria FLASH SPI encontrada en SS = LCD_EN");
		printf("\nJEDEC ID: %d %d %d\nFabricante ",buf_tx[0],buf_tx[1],buf_tx[2]);
		switch(buf_tx[0]){
		case 0xEF:
			printf("Winbond");
			break;
		case 0xC2:
			printf("Macronix");
			break;
		default:
			printf("Desconocido");
			break;
		}
		printf("\nCapacidad ");
		switch(buf_tx[2]){
		case 0x15:
			printf(" 2 Mbyte");
			dbfsFLASH.length=0x200000;
			break;
		case 0x30:
			printf("4 Mbyte");
			dbfsFLASH.length=0x400000;
			break;
		default:
			printf("Desconocido");
			dbfsFLASH.length=0x100000; //1 Mbyte
			break;
		}
		cache_spi = malloc(cache);
		sector_buffer=0xFFFFFFFF;
		for(Id=0;Id<4096;Id++)
			cache_spi[Id]=0xFF;
		cache_write=0;
	 registerFS(&fsFLASH);
	 printf("\nMemoria montada en /flash .... [OK]\n");
	}
}

//-------------------------------------------------------------------------------------------------------------
static void handle_sector_erase(uint32_t addr, uint32_t size){
	uint32_t sector;
	uint32_t temp,i;
	    sector=(addr&0x0000FFF);
		if((sector+size)<0x1001){ // El dato entra dentro del secotr?
			cache_write=1;
			for(i=0;i<size;i++)
				cache_spi[sector+i]=0xFF;
		}else{
			temp=0x100-((sector+size)-0x1000);
			for(i=0;i<temp;i++)
				cache_spi[sector+i]=0xFF;
			sector_erase(sector_buffer);
			for(i=0;i<0x1000;i++){
				if(cache_spi[i]!=0xFF){
					write_page(cache_spi,sector_buffer,0x1000);
					break;
				}
			}
			cache_write=0;
			sector_buffer+=0x1000;
			read_page(cache_spi,sector_buffer,0x1000);
			sector=size-temp;
			for(i=0;i<sector;i++)
				cache_spi[i]=0xFF;
		}
}
static void handle_write_pagina(uint32_t addr, uint8_t *ptr, uint32_t size){
uint32_t sector;
uint32_t temp,i;
    sector=(addr&0x00000FFF);
	if((sector+size)<0x1001){//el dato a escribir entra dentro del sector?
		for(i=0;i<size;i++){
			cache_spi[sector+i]=ptr[i];
			if(ptr[i]!=0xFF)cache_write=1;
		}
		}else{
			temp=0x100-((sector+size)-0x1000);
			for(i=0;i<temp;i++)
			cache_spi[sector+i]=ptr[i];
		sector_erase(sector_buffer);
		for(i=0;i<0x1000;i++){
			if(cache_spi[i]!=0xFF){
				write_page(cache_spi,sector_buffer,0x1000);
				break;
			}
		}
		cache_write=0;
		sector_buffer+=0x1000;
		read_page(cache_spi,sector_buffer,0x1000);
		sector=size-temp;
		for(i=0;i<sector;i++){
			cache_spi[i]=ptr[temp+i];
			if(ptr[temp+i]!=0xFF)cache_write=1;
		}
	}
}
//-------------------------------------------------------------------------------------------------------


void plt_spiflash_read (struct stDbFsStruct *unUsed, uint8_t * ptr, uint32_t offset, uint32_t size, void * baseAddr){
	uint32_t addr_t,offs,i;
	if(offset>0x001FFFFF) return;//direccion fuera de rango
	addr_t=(offset&0x00000FFF);//recupera direccion bloque
	if(sector_buffer == (offset&0x001FF000)){
		if((size+addr_t)<0x1001){
			for(i=0;i<size;i++)
				ptr[i]=cache_spi[addr_t+i];
			return;
		}else{

			offs=0x100-((size+addr_t)-0x1000);
			for(i=0;i<offs;i++)
				ptr[i]=cache_spi[addr_t+i];
			read_page(&ptr[offs],(offset+offs),(size-offs));
			return;
		}
	}
	read_page(ptr,offset,size);
}
//----------------------------------------------------------------------------------------
void plt_spiflash_write(struct stDbFsStruct * unUsed, uint32_t offset, uint8_t * ptr, uint32_t size, void * baseAddr){
uint16_t i;
	if(sector_buffer == (offset&0x001FF000)){
		handle_write_pagina(offset,ptr,size);
	}else{
		if(sector_buffer != (offset&0x001FF000)){
			sector_erase(sector_buffer);
			for(i=0;i<0x1000;i++){
				if(cache_spi[i]!=0xFF){
					write_page(cache_spi,sector_buffer,0x1000);
					break;
				}
			}
			cache_write=0;
			sector_buffer=(offset&0x001FF000);
			read_page(cache_spi,sector_buffer,0x00001000);//recupera el sector completo
			handle_write_pagina(offset,ptr,size);
		}
	}
}

//---------------------------------------------------------------------------------------
void plt_spiflash_erase(uint32_t addr, uint32_t size){
uint16_t i;
	if(sector_buffer==0xFFFFFFFF){
		sector_buffer=(addr&0x001FF000);
		read_page(cache_spi,sector_buffer,0x1000);//recupera el sector completo
		handle_sector_erase(addr,size);
		return;
	}
	if(sector_buffer == (addr&0x001FF000)){
		handle_sector_erase(addr,size);
		return;
	}
	if(sector_buffer != (addr&0x001FF000)){
		sector_erase(sector_buffer);
		for(i=0;i<0x1000;i++){
			if(cache_spi[i]!=0xFF){
					write_page(cache_spi,sector_buffer,0x1000);
					break;
				}
			}
		cache_write=0;
		sector_buffer=(addr&0x001FF000);
		read_page(cache_spi,sector_buffer,0x1000);//recupera el sector completo
		handle_sector_erase(addr,size);
	}
}

void plt_spiflash_close(uint32_t dummy){
	uint16_t i;
	for(i=0;i<0x1000;i++){
		if(cache_spi[i]!=0xFF){
			cache_write=0;
			sector_erase(sector_buffer);
			write_page(cache_spi,sector_buffer,0x1000);
			return;
		}
	}
}


//-------------------------------------------------------------------
//----------              Comandos MEMORIA FLASH   ------------------
//-------------------------------------------------------------------
uint8_t r_status (void){
    uint8_t ret;
	Gpio_SC(0);
	buf_tx[0]=CMD_STATUS;
	spi_write(buf_tx, 1);
	spi_read(&ret, 1);
	Gpio_SC(1);
  	return ret;
}
void w_status (uint8_t stat){
	Gpio_SC(0);
	buf_tx[0]=CMD_WEN;
	buf_tx[1]=CMD_WSTATUS;
	buf_tx[2]=stat;
  	spi_write(buf_tx, 3);
	Gpio_SC(1);
}
uint8_t set_protection (uint8_t bprt){
	uint8_t stat=r_status();
	stat&=0x11100011;
	stat|=(bprt<<2);
	w_status(stat);
	return stat;
}
uint8_t get_protection(void){
	 uint8_t stat;
	 stat=r_status();
	 stat = (stat>>2) & 0x07;
	 return(stat);
}

void read_page(uint8_t *buffer,uint32_t addr,uint32_t len){
	uint32_t i,addrs;
	do{
		i=(r_status()&0x01);
	}while(i==1);
	if(len<=256){
	Gpio_SC(0);
	buf_tx[0]=CMD_FREAD;
	buf_tx[1]=(addr>>16)&0xff;
	buf_tx[2]=(addr>>8)&0xff;
	buf_tx[3]=addr&0xff;
  	spi_write(buf_tx, 5);
  	spi_read(buffer, len);
  	Gpio_SC(1);}
	else{
		addrs=addr;
		for(i=0;i<len;i+=0x100){
		Gpio_SC(0);
		buf_tx[0]=CMD_FREAD;
		buf_tx[1]=(addrs>>16)&0xff;
		buf_tx[2]=(addrs>>8)&0xff;
		buf_tx[3]=addrs&0xff;
	  	spi_write(buf_tx, 5);
	  	spi_read(&buffer[i], 0x100);
		addrs+=0x100;
		Gpio_SC(1);
		}
	}
}


void write_page(uint8_t *buffer,uint32_t addr,uint32_t len){
	uint32_t i,addrs,st;
	do{
		st=(r_status()&0x01);
	}while(st==1);
	Gpio_SC(0);
	buf_tx[0]=CMD_WEN;
	spi_write(buf_tx, 1);
	Gpio_SC(1);
	if(len<=256){
		Gpio_SC(0);
		buf_tx[0]=CMD_PPRGM;
		buf_tx[1]=(addr>>16)&0xff;
		buf_tx[2]=(addr>>8)&0xff;
		buf_tx[3]=addr&0xff;
		spi_write(buf_tx, 4);
		spi_write(buffer,len);
		Gpio_SC(1);}
	else{
		addrs=addr;
		for(i=0;i<len;i+=0x100){
			Gpio_SC(0);
			buf_tx[0]=CMD_PPRGM;
			buf_tx[1]=(addrs>>16)&0xff;
			buf_tx[2]=(addrs>>8)&0xff;
			buf_tx[3]=addrs&0xff;
			spi_write(buf_tx, 4);
			spi_write(&buffer[i],0x100);
			addrs+=0x100;
			Gpio_SC(1);
			do{
				st=(r_status()&0x01);
			}while(st==1);
			Gpio_SC(0);
			buf_tx[0]=CMD_WEN;
			spi_write(buf_tx, 1);
			Gpio_SC(1);
		}
	}
}

void sector_erase(uint32_t sector){
uint16_t i;
do{
	i=(r_status()&0x01);
}while(i==1);
	Gpio_SC(0);
	buf_tx[0]=CMD_WEN;
  	spi_write(buf_tx, 1);
  	Gpio_SC(1);
  	Gpio_SC(0);
	buf_tx[0]=CMD_SERS;
	buf_tx[1]=(sector>>16)&0xff;
	buf_tx[2]=(sector>>8)&0xff;
	buf_tx[3]=sector&0xff;
	spi_write(buf_tx, 4);
  	Gpio_SC(1);
}
void erase_all(void){
uint8_t i;
do{
	i=(r_status()&0x01);
}while(i==1);
	Gpio_SC(0);
	buf_tx[0]=CMD_WEN;
  	spi_write(buf_tx, 1);
  	Gpio_SC(1);
  	Gpio_SC(0);
  	buf_tx[1]=CMD_CHIP_E;
	spi_write(buf_tx, 1);
  	Gpio_SC(1);
}

void get_ID(uint8_t *buf){
	Gpio_SC(0);
	buf_tx[0]=CMD_IDREAD;
	spi_write(buf_tx, 1);
	spi_read(buf, 3);
	Gpio_SC(1);
}
