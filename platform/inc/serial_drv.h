/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef serial_drv_h
#define serial_drv_h
#include "config.h"
#include <stdint.h>

// Declaraciones de tipos utilizados en la configuración

typedef enum {
	PLT_USART_STOP_BIT_1, 
	PLT_USART_STOP_BIT_2
} tplt_usart_stop_bits;

typedef enum {
	PLT_USART_PARITY_NONE, 
	PLT_USART_PARITY_ODD, 
	PLT_USART_PARITY_EVEN, 
	PLT_USART_PARITY_MARK, 
	PLT_USART_PARITY_SPACE
} tplt_usart_parity;


typedef enum {
	PLT_USART_DATA_5,
	PLT_USART_DATA_6,
	PLT_USART_DATA_7,
	PLT_USART_DATA_8
} tplt_usart_data_bits;


unsigned int plt_configure_usart(unsigned int id, 
				 unsigned int baud, 
			         tplt_usart_data_bits dBits, 
				 tplt_usart_parity parity, 
                                 tplt_usart_stop_bits stopBits);

unsigned int plt_configure_default_usart(unsigned int id, 
				 unsigned int baud, 
			         tplt_usart_data_bits dBits, 
				 tplt_usart_parity parity, 
                                 tplt_usart_stop_bits stopBits);

unsigned int plt_send_default_usart(
		const uint8_t *data,
			    unsigned int dataLen);

unsigned int plt_recv_default_usart(
		uint8_t *data,
			    unsigned int maxData);

#endif

