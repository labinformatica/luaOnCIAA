/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef spi_memory_drv_h
#define spi_memory_drv_h
#include <stdint.h>
#include <datBoxFs.h>

//-----------------------------------------------------------------------------------------------------------------
#define CMD_WEN 		0x06 //w_enable
#define CMD_WDIS 		0x04 //w_dis
#define CMD_STATUS 		0x05 //R_reg
#define CMD_STATUS2		0x35
#define CMD_WSTATUS		0x01 //
#define CMD_PPRGM 		0x02 //page program

#define CMD_SERS 		0x20 //borra 04k
#define CMD_BERS	 	0x52 //borra 32k
#define CMD_BERS_64K	0xD8 //borra 64k
#define CMD_CHIP_E		0x60 //borra chip entero
#define CMD_ESUSPND	    0x75
#define CMD_ERESUME	    0x7A
#define CMD_PDOWN		0xB9
#define CMD_WAKEUP		0xAB
#define CMD_READ		0x03 //read
#define CMD_FREAD		0x0B //Fast_R
#define CMD_IDREAD		0x9F //R_ID
#define CMD_ID			0x4B

void init_flash_memory(void);
void get_ID(uint8_t *buf);
void sector_erase(uint32_t sector);
void write_page(uint8_t *buffer,uint32_t addr,uint32_t len);
void read_page(uint8_t *buffer,uint32_t addr,uint32_t len);
void plt_spiflash_read (struct stDbFsStruct *unUsed, uint8_t * ptr, uint32_t offset, uint32_t size, void * baseAddr);
void plt_spiflash_write(struct stDbFsStruct * unUsed, uint32_t offset, uint8_t * ptr, uint32_t size, void * baseAddr);
void plt_spiflash_erase(uint32_t addr, uint32_t size);
void plt_spiflash_close(uint32_t dummy);
void sync_spimem_cache(void);


#endif
