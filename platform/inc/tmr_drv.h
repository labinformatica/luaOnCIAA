/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PLATFORM_INC_TMR_DRV_H_
#define PLATFORM_INC_TMR_DRV_H_

#include <config.h>
#include <stdint.h>

#define CLOCKS_PER_SEC_1 1000

#define TICKRATE_HZ 1000 // 1ms

#if defined(CORE_M0)
  #if   SYSTEM_TMR == 0
    #define SYSTEM_TMR_ISR M0_TIMER0_IRQHandler
  #elif SYSTEM_TMR == 1
    #error Timer1 does not support interrupt on M0
  #elif SYSTEM_TMR == 2
    #error Timer2 does not support interrupt on M0
  #elif SYSTEM_TMR == 3
    #define SYSTEM_TMR_ISR M0_TIMER3_IRQHandler
  #else
    #error SYSTEM_TMR must be 0 or 3 on M0
  #endif
#elif defined (CORE_M4)
  #if   SYSTEM_TMR == 0
    #define SYSTEM_TMR_ISR TIMER0_IRQHandler
  #elif SYSTEM_TMR == 1
    #define SYSTEM_TMR_ISR TIMER1_IRQHandler
  #elif SYSTEM_TMR == 2
    #define SYSTEM_TMR_ISR TIMER2_IRQHandler
  #elif SYSTEM_TMR == 3
    #define SYSTEM_TMR_ISR TIMER3_IRQHandler
  #else
    #error SYSTEM_TMR must be in range [0, 3]
  #endif
#else
  #error You must define the constants CORE_M0 or CORE_M4 to configure on wich core the application will be executed.
#endif

void plt_sys_tmr_init(void);
uint64_t getSysTick(void);
uint32_t plt_sys_tmr_read(void);
uint64_t plt_sys_tmr_getSegCounter(void);

#endif /* PLATFORM_INC_TMR_DRV_H_ */
