/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __LPCGPIOC_H__
#define __LPCGPIOC_H__
#include "gpio_drv.h"
//--------------------------------------------------------------------------
//				Definicion de Puerto
//--------------------------------------------------------------------------

//-------------Definiciones-------------------
//----REG-> PIN
const Port_T Port0[]= {
 {0,0},  /*  CON2_04   ENET_RXD1  */
 {0,1},  /*  CON2_06   ENET_TXEN  */
 {1,15}, /*  CON2_09   ENET_RXD0  */
 {1,16}, /*  CON2_10   ENET_CRS_DV*/
 {1,0},  /*  TEC1      TEC_1      */
 {6,6},
 {3,6},
 {2,7},
 {1,1},  /*  TEC2      TEC_2      */
 {1,2},  /*  TEC3      TEC_3      */
 {1,3},  /*  CON2_18   SPI_MISO   */
 {1,4},  /*  CON2_21   SPI_MOSI   */
 {1,17}, /*  CON2_12   ENET_MDIO  */
 {1,18}, /*  CON2_14   ENET_TXD0  */
 {2,10}, /*  LED1      LED1       */
 {1,20}  /*  CON2_16   ENET_TXD1  */
};

const Port_T Port1[] = {
 {1,7},
 {1,8},
 {1,9},
 {1,10},
 {1,11},
 {1,12},
 {1,13},
 {1,14},
 {1,5},   /* CON1_39   T_COL0 */
 {1,6},   /* TEC4      TEC_4  */
 {2,9},
 {2,11},  /* LED2      LED2   */
 {2,12},  /* LED3      LED3   */
 {2,13},
 {3,4},
 {4,5}};

const Port_T Port2[] = {
 {4,0},   /* CON1_33   T_FIL0 */
 {4,1},   /* CON1_36   T_FIL1 */
 {4,2},   /* CON1_37   T_FIL2 */
 {4,3},   /* CON1_35   T_FIL3 */
 {4,4},   /* CON2_30   LCD1   */
 {4,5},   /* CON2_28   LCD2   */
 {4,6},   /* CON2_26   LCD3   */
 {5,7},
 {6,12},  /* CON2_40   GPIO8  */
 {5,0},
 {5,1},
 {5,2},
 {5,3},
 {5,4},
 {5,5},
 {5,6}};


const Port_T Port3[] = {
 {6,1},	   /*CON2_29   GPIO0    */
 {6,2},
 {6,3},
 {6,4},    /*CON2_32   GPIO1   03 */
 {6,5},    /*CON2_31   GPIO2    */
 {6,9},    /*CON2_36   GPIO5    */
 {6,10},   /*CON2_35   GPIO6    */
 {6,11},   /*CON2_38   GPIO7   07 */
 {7,0},
 {7,1},
 {7,2},
 {7,3},
 {7,4},    /*CON1_31   T_COL1  12 */
 {7,5},    /*CON1_34   T_COL2  13 */
 {7,6},
 {7,7}};   /*CON2_08   ENET_MDC 15 */

const Port_T Port4[] = {
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {64,64},
 {9,6},  /* RS 485_RXD      11 */
 {64,64},
 {64,64},
 {64,64},
 {64,64}};

const Port_T Port5[] = {
 {2,0},  /*LEDR      LED0_R    */
 {2,1},  /*LEDG      LED0_G    */
 {2,2},  /*LEDB      LED0_B    */
 {2,3},  /*CON1_25   RS232_TXD */
 {2,4},  /*CON1_23   RS232_RXD */
 {2,5},
 {2,6},
 {2,8},
 {3,1},  /*CON1_27   CAN_RD    */
 {3,2},  /*CON1_29   CAN_TD    */
 {3,7},
 {3,8},
 {4,8},  /*CON2_24   LCD_RS     */
 {4,9},  /*CON2_23   LCD_EN     */
 {4,10}, /*CON2_22   LCD4      */
 {6,7},  /*CON2_34   GPIO3     */
 {6,8},  /*CON2_33   GPIO4     */
 {64,64},
 {9,5}}; /*RS485_TXD           */


//Pin to REG[pos]
const Port_T pin0[]={
//Registro, posicion
 {0,0},  /*0  CON2_04   ENET_RXD1  */
 {0,1}   /*1  CON2_06   ENET_TXEN  */
};



const Port_T pin1[]={
//Registro, posicion
{0,4},   /*0 TEC_1        */
{0,8},   /*1 TEC_2        */
{0,9},   /*2 TEC_3        */
{0,10},  /*3 SPI_MISO     */
{0,11},  /*4 SPI_MOSI     */
{1,8},   /*5 T_COL0 	  */
{1,9},	 /*6 TEC_4 	  	  */
{1,0},	 /*7			  */
{1,1},	 /*8			  */
{1,2},	 /*9			  */
{1,3},	 /*10			  */
{1,4},	 /*11			  */
{1,5},	 /*12			  */
{1,6},	 /*13			  */
{1,7},	 /*14			  */
{0,2}, 	 /*15  ENET_RXD0  */
{0,3},	 /*16  ENET_CRS_DV*/
{0,12},	 /*17  ENET_MDIO  */
{0,13},	 /*18  ENET_TXD0  */
{64,64}, /*19 			  */
{0,15}   /*20  ENET_TXD1  */
};

const Port_T pin2[]={
//Registro, posicion
{5,0},	 /*0 LEDR  */
{5,1},	 /*1 LEDG  */
{5,2},	 /*2 LEDB  */
{5,3},	 /*3 232_TX*/
{5,4},	 /*4 232_RX*/
{5,5},	 /*5 	   */
{5,6},	 /*6       */
{0,7},	 /*7       */
{5,7},	 /*8  	   */
{1,10},	 /*9  	   */
{0,14},	 /*10 LED1 */
{1,11},	 /*11 LED2 */
{1,12},	 /*12 LED3 */
{1,13}	 /*13      */
};

const Port_T pin3[]={
//Registro, posicion
{64,64}, /*0	 	*/
{5,8}, 	 /*1 CAN_RD	*/
{5,9},   /*2 CAN_RD	*/
{64,64}, /*3		*/
{1,14},  /*4		*/
{64,64}, /*5		*/
{0,6},	 /*6		*/
{5,10},	 /*7		*/
{5,11}	 /*8		*/
};

const Port_T pin4[]={
//Registro, posicion
{2,0},  /*0  T_FIL0	*/
{2,1},  /*1  T_FIL1	*/
{2,2},  /*2  T_FIL2	*/
{2,3},  /*3  T_FIL3	*/
{2,4},  /*4  LCD1	*/
{2,5},  /*5  LCD2	*/
{2,6},  /*6  LCD3	*/
{64,64},/*7			*/
{5,12}, /*8  LCD_RS	*/
{5,13}, /*9  LCD_EN	*/
{5,14}  /*10 LCD4	*/
};

const Port_T pin5[]={
//Registro, posicion
{2, 9},/*0 */
{2,10},/*1 */
{2,11},/*2 */
{2,12},/*3 */
{2,13},/*4 */
{2,14},/*5 */
{2,15},/*6 */
{2, 7} /*7 */
};


const Port_T pin6[]={
//Registro, posicion
{64,64},/*0 		*/
{3,0}, 	/*1  GPIO0	*/
{3,1}, 	/*2 		*/
{3,2}, 	/*3 		*/
{3,3}, 	/*4  GPIO1	*/
{3,4}, 	/*5  GPIO2	*/
{0,5}, 	/*6 		*/
{5,15},	/*7  GPIO3	*/
{5,16},	/*8  GPIO4	*/
{3,5}, 	/*9  GPIO5	*/
{3,6}, 	/*10 GPIO6 	*/
{3,7}, 	/*11 GPIO7	*/
{2,8}  	/*12 GPIO8	*/
};


const Port_T pin7[]={
//Registro, posicion
{3,8}, /*0 			*/
{3,9}, /*1 			*/
{3,10}, /*2 		*/
{3,11}, /*3 		*/
{3,12}, /*4 T_COL1	*/
{3,13}, /*5 T_COL2	*/
{3,14}, /*6 		*/
{3,15} /*7 ENET_MDC	*/
};

const Port_T pin9[]={
//Registro, posicion
{64,64},/*0			 */
{64,64},/*1			 */
{64,64},/*2			 */
{64,64},/*3			 */
{64,64},/*4			 */
{5,18}, /*5 485_TX	 */
{4,11}  /*6 485_RX	 */
};

#include "gpio_drv.h"

static pin_id pin_ID[] = {
		{"RXD1"    ,0,0 },
		{"TX_EN"   ,0,1 },
		{"RXD0"    ,1,15},
		{"CRS_DV"  ,1,16},
		{"SPI_MISO",1,3 },
		{"SPI_MOSI",1,4 },
		{"MDIO"    ,1,17},
		{"TXD0"    ,1,18},
		{"TXD1"    ,1,20},
		{"T_COL0"  ,1,5 },
		{"T_FIL0"  ,4,0 },
		{"T_F1"    ,4,1 },
		{"T_FIL2"  ,4,2 },
		{"T_FIL3"  ,4,3 },
		{"LCD1"    ,4,4 },
		{"LCD2"    ,4,5 },
		{"LCD3"    ,4,6 },
		{"LCD4"    ,4,10},
		{"LCD_RS"  ,4,8 },
		{"LCD_EN"  ,4,9 },
		{"GPIO0"   ,6,1 },
		{"GPIO1"   ,6,4 },
		{"GPIO2"   ,6,5 },
		{"GPIO3"   ,6,7 },
		{"GPIO4"   ,6,8 },
		{"GPIO5"   ,6,9 },
		{"GPIO6"   ,6,10},
		{"GPIO7"   ,6,11},
		{"GPIO8"   ,6,12},
		{"T_COL1"  ,7,4 },
		{"T_C2"    ,7,5 },
		{"MDC"     ,7,7 },
		{"232_TX"  ,2,3 },
		{"232_RX"  ,2,4 },
		{"CAN_RD"  ,3,1 },
		{"CAN_TD"  ,3,2 }
};

#endif
