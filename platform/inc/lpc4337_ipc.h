/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef __LPCIPC_H__
#define __LPCIPC_H__
#include <stdint.h>
#include <lpc_types.h>


#define SHARED_MEM_IPC 0x10088000
#define CANT_MENSAJES 16


//Defino tipo de dato para direccion de puerto y pin

typedef struct una_dir{
	uint8_t puerto;
	uint8_t pin;
}tipo_dir;

//Defino el tipo de mensaje para el M4
typedef struct un_msg_m4{
	uint16_t cpu;
	uint16_t funId;
	tipo_dir carga;
} tipo_msg_m4;

//Defino el tipo de mensaje para el M0
typedef struct un_msg_m0{
	uint16_t cpu;
	uint16_t funId;
	bool verif;
}tipo_msg_m0;



void conmutar_led_m4(tipo_dir);

void estado_led_m4(tipo_dir);

void iniciar_contador_m4(tipo_dir);

void valor_contador_m4(tipo_dir);

void pausar_contador_m4(tipo_dir);

void reiniciar_contador_m4(tipo_dir);

void restaurar_contador_m4(tipo_dir);

#ifdef CORE_M4
//uint16_t contador = 0;
#endif


void conmutar_led_m0(uint8_t,uint8_t);

bool estado_led_m0(uint8_t,uint8_t);

void inicializar_cola_m0(void);

bool verificar_cola_m0(void);

int mensajes_pendientes_m0(void);

void hacer_pop(void);

void iniciar_contador_m0(void);

uint16_t valor_contador_m0(void);

void pausar_contador_m0(void);

void reiniciar_contador_m0(void);

void restaurar_contador_m0(void);




#endif
