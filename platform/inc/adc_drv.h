/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __LPCADC_H__
#define __LPCADC_H__
#include <stdint.h>

/**
 * Descrip:	Inicializa el ADC y lo setea a valores default.
 * param:	adc		: 0 o 1 (ADC0 o ADC1).
 * param:	state	: ENABLE - DISABLE.
 * return	0 si los datos son validos, 1 si son invalidos.
 * Nota	    La configuracion default del ADC es 400kS/s - 10bits
 */
unsigned char CONF_adcCHNL (unsigned char adc, FunctionalState state);

/**
 * Descrip:	Configura sample rate y resolucion.
 * param:	adc		: 0 o 1 (ADC0 o ADC1).
 * param:	rate	: Sample rate
 * param:	resol	: Resolucion (3 a 10 bits).
 * return:	0 si los datos son validos, 1 si son invalidos.
 * Nota:    Max Sample Rate - 400KS/s | Max Resolucion - 10bits
 */
unsigned char CONF_adcRATE (unsigned char adc, uint32_t rate, unsigned char resol);

/**
 * Descrip:	Devuelve el dato leido al puntero data.
 * param:	adc		: 0 o 1 (ADC0 o ADC1).
 * param:	channel	: 1 - 3
 * return:	1 si la lectura fue exitosa, 0 si hubo errores.
 * Nota:
 */
uint16_t READ_adcVAL (unsigned char adc, uint8_t channel);

#endif
