/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef __LPCGPIO_H__
#define __LPCGPIO_H__
#include <stdint.h>
#include <stdio.h>

typedef struct {
uint8_t port;
uint8_t pin;
}Port_T; 

typedef enum{
 PIN_PULLUP,
 PIN_PULLDOWN,
 PIN_NOPULL,
 PIN_SET,
 PIN_CLEAR,
 PIN_OUTPUT,
 PIN_INPUT,
 PIN_GETVAL,
 PORT_SETVAL,
 PORT_OUTPUT,
 PORT_INPUT,
 PORT_GET
}PORT_MUX;

//Platform BUS MATRIX CONFIGURATION function 
#define MAXCHAR 10

struct P_dev{
 char module[MAXCHAR];
 uint8_t port;
 uint8_t pin;
};

typedef struct P_dev pin_id;

uint16_t pars (char *text, pin_id *d,uint8_t camp);
uint16_t PIO_SET(const int8_t *p,uint8_t v,uint8_t conf);
uint8_t Pin_FUNC (uint8_t port, uint8_t Tpin,uint8_t IO_D,uint16_t mode);
int16_t gpio_IO (uint8_t port,uint8_t pin,int16_t op );
uint8_t PinToR(uint8_t Ep,uint8_t Ep1,uint8_t *Rprt,uint8_t *Rpin);
void get_pin_port(uint16_t pos,uint8_t *port,uint8_t *pin);

#endif
