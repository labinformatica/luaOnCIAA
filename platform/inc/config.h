/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef config_h
#define config_h

#define RETURN_OK	1
#define RETURN_FAIL	0

/* Identificador del puerto serie a utilizar para 
   la entrada salida por defecto                  */

#define defaultSerialId	2
#define defaultBaud 115200

/* Tamaño del buffer de entrada utilizado para la cola circular
   de entrada de datos
*/
#define defaultSerialRxBuffSz 128

/* Tamaño del buffer de entrada utilizado para la cola circular
   de salida de datos
*/
#define defaultSerialTxBuffSz 512

/*
 * Mostrar mensaje durante la inicialización?
 * */
#define MsgAtBoot

/*
 * Constante de comprobacion de RTC habilitado
 * */
#define RTC_const 0xAA13

/*
 * Boot message
 * */
#if defined(CORE_M0)
#define BOOTMSG "\n\rInicializando Cortex-M0...\n\r"
#elif defined (CORE_M4)
#define BOOTMSG "\n\rInicializando Cortex-M4...\n\r"
#else
#define BOOTMSG "\n\rInicializando Unknown...\n\r"
#endif
/*
 *  Largo máximo del nombre de un comando cuando se utiliza shell.
 */
#define MAXCMDLEN 32

/*
 *  Largo máximo de la línea de shell
 */
#define MAXSHELLLINE 128

/*
 *  Tamaño máximo del nombre de un punto de montaje
 */
#define MAXMOUNTPOINTNAME 32


/*
 *  Cantidad de puntos de montaje que se prevee utilizar
 */

#define MAXMOUNTPOINT 10

/*
 * Establece la cantidad máxima de archivos que pueden estar abiertos simultáneamente.
 * (3 ya se utilizan para stdin, stdout y stderr)
 */

#define MAXFILEDESCRIPTORS 10

/*
 *  Separador de campos utilizado en la salida de comandos diseñados para ser interpretados
 *  por un agente remoto.
 * */

#define FIELDSEP "|"

/*
 *  Secuencia utilizada para indicar el fin de una transmisión.
 *
 * */

#define EOT "\n"

/*
 * Timer utilizado como base de tiempo del sistema. Este timer será utilizado para
 * generar las señales de cuenta asociadas a la función clock de C y los timmers
 * virtuales (aún no implementado....)
 */
#define SYSTEM_TMR 0

  #if defined(CORE_M0)
    #if defaultSerialId == 0
      #define defaultUsartISR M0_USART0_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART0
      #define DEFAULT_UART_IRQ 24
    #elif defaultSerialId == 1 
      #error Can not use USART1 as default io using Cortex M0 processor
    #elif defaultSerialId == 2
      #define defaultUsartISR M0_USART2_OR_C_CAN1_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART2
      #define DEFAULT_UART_IRQ 26
    #elif defaultSerialId == 3
      #define defaultUsartISR M0_USART3_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART3
      #define DEFAULT_UART_IRQ 27
    #endif
  #elif defined (CORE_M4)
    #if defaultSerialId == 0
      #define defaultUsartISR UART0_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART0
      #define DEFAULT_UART_IRQ 24
    #elif defaultSerialId == 1 
      #define defaultUsartISR UART1_IRQHandler
      #define DEFAULT_UART_LPC LPC_UART1
      #define DEFAULT_UART_IRQ 25
    #elif defaultSerialId == 2
      #define defaultUsartISR UART2_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART2
      #define DEFAULT_UART_IRQ 26
    #elif defaultSerialId == 3
      #define defaultUsartISR UART3_IRQHandler
      #define DEFAULT_UART_LPC LPC_USART3
      #define DEFAULT_UART_IRQ 27
    #endif
  #elif PCDEV
    #pragma message "PC version definition used for file system"
  #else
    #error You must define the constants CORE_M0 or CORE_M4 to configure on wich core the application will be executed.
  #endif
#endif
