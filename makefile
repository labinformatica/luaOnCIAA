-include buildOpt.mk
-include modulos.mk
-include openocd.mk
appCFLAGSM0:=-I$(BASEPATH)/$(TEMPSYS)/include -D__BUFSIZ__=128   
appCFLAGSM4:=-I$(BASEPATH)/$(TEMPSYS)/include -D__BUFSIZ__=128

appLIBSM0:=-Wl,--start-group -llua-m0 -lplatform-m0 -lluahal-m0 -llpcopen-m0 -lshell-m0 -lipc-m0 -ldbFS-m0 -lc -lm -lgcc -lfs-m0 -lromfs-m0 -Wl,--end-group
appLIBSM4:=-Wl,--start-group -llua-m4 -lplatform-m4 -lluahal-m4 -llpcopen-m4 -lshell-m4 -lipc-m4 -ldbFS-m4 -lc -lm -lgcc -lfs-m4 -lromfs-m4 -Wl,--end-group

all: libs buildExamples loaderM4
	@echo "Build"

luaM0:
	@make -s -C lua/ CFLAGS="$(CFLAGSM0) $(addprefix -D,$(EN_LUA)) -DLINE  $(appCFLAGSM0) -I." LDFLAGS="$(LDFLAGSM0)" LIBS="$(appLIBSM0)" CORE=m0
	
luaM4:
	@make -s -C lua/ CFLAGS="$(CFLAGSM4) $(addprefix -D,$(EN_LUA)) -DLINE  $(appCFLAGSM4) -I." LDFLAGS="$(LDFLAGSM4)" LIBS="$(appLIBSM4)" CORE=m4

loaderM4:
	@make -s -C M4Loader/ CFLAGS="$(CFLAGSM4) $(appCFLAGSM4) -I." LDFLAGS="$(LDFLAGSM4)" LIBS="$(appLIBSM4)" CORE=m4 

programLoader:
	$(OPENOCD_BIN) -f openocd/lpc4337.cfg -c "init" -c "halt 0" -c "flash write_image erase unlock M4Loader/out/firmware-m4.bin 0x1A000000 bin" -c "reset run" -c "shutdown"

luaComp:
	@make -s -C lua/ -f makefilePC CFLAGS="-DLUA_32BITS" 

buildExamples: platform-m0 platform-m4 
	@make -C ejemplos/ CFLAGS="$(CFLAGSM0) $(appCFLAGSM0)" LDFLAGS="$(LDFLAGSM0)" LIBS="$(appLIBSM0)" CORE=m0
	@make -C ejemplos/ CFLAGS="$(CFLAGSM4) $(appCFLAGSM4)" LDFLAGS="$(LDFLAGSM4)" LIBS="$(appLIBSM4)" CORE=m4

platform-m0:
	@make -s -C platform moduleName=libplatform-m0 CFLAGS="$(CFLAGSM0) -I$(BASEPATH)/$(TEMPSYS)/include" LDFLAGS="$(LDFLAGSM0)" CORE=m0
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R platform/inc/* $(BASEPATH)/$(TEMPSYS)/include/

platform-m4:
	@make -s -C platform moduleName=libplatform-m4 CFLAGS="$(CFLAGSM4) -I$(BASEPATH)/$(TEMPSYS)/include" LDFLAGS="$(LDFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R platform/inc/* $(BASEPATH)/$(TEMPSYS)/include/

libs: lpcopen luaM4 luaM0 luaComp $(ENABLEMOD)

lpcopen: lpcOpenM0 lpcOpenM4

basefs:
	@make -s -C fs moduleName=libfs-m0 CFLAGS="$(CFLAGSM0)" CORE=m0
	@make -s -C fs moduleName=libfs-m4 CFLAGS="$(CFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R fs/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/

romfs:
	@make -s -C fs/rom moduleName=libromfs-m0 CFLAGS="$(CFLAGSM0)" CORE=m0
	@make -s -C fs/rom moduleName=libromfs-m4 CFLAGS="$(CFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R fs/rom/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/

dbFS:
	@make -s -C fs/datBoxFs moduleName=libdbFS-m0 CFLAGS="$(CFLAGSM0)" CORE=m0
	@make -s -C fs/datBoxFs moduleName=libdbFS-m4 CFLAGS="$(CFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R fs/datBoxFs/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/
	
basicShell:
	@make -s -C shell moduleName=libshell-m0 CFLAGS="$(CFLAGSM0)" CORE=m0
	@make -s -C shell moduleName=libshell-m4 CFLAGS="$(CFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R shell/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/

lua_Mod:
	@make -s -C luaMod moduleName=libluahal-m0 CFLAGS="$(CFLAGSM0) $(addprefix -D,$(EN_LUA))" CORE=m0
	@make -s -C luaMod moduleName=libluahal-m4 CFLAGS="$(CFLAGSM4) $(addprefix -D,$(EN_LUA))" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R luaMod/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/

ipc:
	@make -s -C IPC moduleName=libipc-m0 CFLAGS="$(CFLAGSM0)" CORE=m0
	@make -s -C IPC moduleName=libipc-m4 CFLAGS="$(CFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp -R  IPC/inc/*.h $(BASEPATH)/$(TEMPSYS)/include/

lpcOpenM0:
	@make -s -C lpc43xx moduleName=liblpcopen-m0 CFLAGS="$(CFLAGSM0)" LDFLAGS="$(LDFLAGSM0)" CORE=m0
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp lpc43xx/inc/* $(BASEPATH)/$(TEMPSYS)/include/
	@cp -R lpc43xx/config_43xx $(BASEPATH)/$(TEMPSYS)/include/
	@cp -R lpc43xx/config_43xx_m0sub $(BASEPATH)/$(TEMPSYS)/include/
	@cp -R lpc43xx/config_43xx_m0app $(BASEPATH)/$(TEMPSYS)/include/

lpcOpenM4:
	@make -s -C lpc43xx moduleName=liblpcopen-m4 CFLAGS="$(CFLAGSM4)" LDFLAGS="$(LDFLAGSM4)" CORE=m4
	@mkdir -p $(BASEPATH)/$(TEMPSYS)/include
	@cp lpc43xx/inc/* $(BASEPATH)/$(TEMPSYS)/include/	
	@cp -R lpc43xx/config_43xx $(BASEPATH)/$(TEMPSYS)/include/
	@cp -R lpc43xx/config_43xx_m0sub $(BASEPATH)/$(TEMPSYS)/include/
	@cp -R lpc43xx/config_43xx_m0app $(BASEPATH)/$(TEMPSYS)/include/

newlib: newLibBuildDir 
	cd $(BASEPATH)/$(BUILDIR)/newlib && ../../newlib/configure \
		--target=arm-none-eabi --enable-interwork --enable-multilib --disable-libssp --disable-nls  \
		--disable-newlib-supplied-syscalls --without-mpc --without-mpfr --without-gmp --prefix=$(BASEPATH)/$(TEMPSYS)
	cd $(BASEPATH)/$(BUILDIR)/newlib && make CFLAGS_FOR_TARGET="-D__BUFSIZ__=64" 
#	cd $(BASEPATH)/$(BUILDIR)/newlib && make CFLAGS_FOR_TARGET="-D__BUFSIZ__=64 -ffunction-sections -fdata-sections -DPREFER_SIZE_OVER_SPEED -D__OPTIMIZE_SIZE__ -Os -fomit-frame-pointer" 
#	cd $(BASEPATH)/$(BUILDIR)/newlib && make check
#	cd $(BASEPATH)/$(BUILDIR)/newlib && make pdf
#	cd $(BASEPATH)/$(BUILDIR)/newlib && make install-pdf
	cd $(BASEPATH)/$(BUILDIR)/newlib && make install

newLibBuildDir:
	@mkdir -p $(BASEPATH)/$(BUILDIR)/newlib
clean:
	@rm -Rf $(BUILDIR)
	@rm -Rf $(TEMPSYS)
	@make -s -C ejemplos/ clean
	@make -s -C M4Loader/ clean

