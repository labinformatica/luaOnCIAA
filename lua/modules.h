#ifndef _MODULES_H
#define _MODULES_H

#include "lrodefs.h"

typedef struct luaL_Reg_adv {
  const char *name;
  lua_CFunction func;
} luaL_Reg_adv;

#define PUT_IN_SECTION(s) __attribute__((used,unused,section(s)))

// Macros for register a library
#define LIB_PASTER(x,y) x##y
#define LIB_EVALUATOR(x,y) LIB_PASTER(x,y)
#define LIB_CONCAT(x,y) LIB_EVALUATOR(x,y)

#define LIB_TOSTRING_PASTER(x) #x
#define LIB_TOSTRING_EVALUATOR(x) LIB_TOSTRING_PASTER(x)
#define LIB_TOSTRING(x) LIB_TOSTRING_EVALUATOR(x)

#define LIB_USED(fname) LIB_CONCAT(CONFIG_LUA_RTOS_LUA_USE_,fname)
#define LIB_SECTION(fname, section) LIB_CONCAT(section,LIB_USED(fname))


#endif
