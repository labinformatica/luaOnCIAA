/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <config.h>
#include <fs.h>
#include <string.h>
#include <sys/errno.h>

#ifdef __cplusplus
 extern "C"{
#endif

 extern int  errno;

fileDesc fileDescriptorTable[MAXFILEDESCRIPTORS];

static struct stFileSystems *registeredFS[MAXMOUNTPOINT] = {0};

static unsigned int registeredFSCount = 0;

struct stFileSystems *getRegisteredFS(unsigned int fsId)
{
  return registeredFS[fsId];
}

unsigned char registerStdFD(struct stFileSystems *fs, int fd)
{
	unsigned char ret = 0;
	if(fd < MAXFILEDESCRIPTORS)
	{
		fileDescriptorTable[fd].fs = fs;
		fileDescriptorTable[fd].inUse = 1;
		ret = 1;
	}
	return ret;
}

unsigned char registerFS(struct stFileSystems *fs)
{
  unsigned char ret = 0;
  if(registeredFSCount < MAXMOUNTPOINT)
  {
    registeredFS[registeredFSCount] = fs;
    registeredFSCount++;
    ret = 1;
  }
  return ret;
}

unsigned int getRegisteredFSCount(void)
{
  return registeredFSCount;
}

int getFirstFreeFD(void)
{
  int i;
  int ret = -1;
  for(i=0; (i<MAXFILEDESCRIPTORS) && (ret == -1);i++)
  {
    if(!fileDescriptorTable[i].inUse)
    {
       fileDescriptorTable[i].inUse = 1;
       ret = i;
    }
  }
  if (ret == -1)
    errno = ENFILE;
  return ret;
}

void freeFD(int fd)
{
  fileDescriptorTable[fd].inUse = 0;
}

#ifdef __cplusplus
 }
#endif
