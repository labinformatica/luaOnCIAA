/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef FSFUNCTIONS_H_
#define FSFUNCTIONS_H_
#include <fs.h>
#include <reent.h>

unsigned int romFsGetFileCount(unsigned char * romFs);
unsigned int romFsGetUsedSpace(unsigned char * romFs);
unsigned int romFsGetFreeSpace(unsigned char * romFs);
unsigned char romFsFileExists(unsigned char * romFs, char *fn);
void romFsOpenDir(struct stFileSystems *romFs, dirEntry *dir);
fileEntry* romFsReadDir(dirEntry *dir);
void romFsCloseDir(dirEntry *dir);

// Funciones para "pegar" el acceso a los datos desde newlib
int romFsOpen(char *, int, int, struct stFileSystems *);
int romFsRead (int, uint8_t *, int);
long romFsSeek(int, long, int);
int romFsClose(int);

typedef struct
{
	char *name;
	unsigned int fileLen;
	unsigned int pos;
	unsigned char *dataFile;
}romFsFileData;


#endif /* FS_ROM_INC_FSFUNCTIONS_H_ */
