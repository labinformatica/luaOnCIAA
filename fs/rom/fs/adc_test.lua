-- Inicialización de puertos
-- y conversor (ADC0, Ch1)
function init()
  adc.setup(0,1)
  gpio.setup(gpio.LED_1, gpio.OUT)
  gpio.setup(gpio.LED_2, gpio.OUT)
  gpio.setup(gpio.LED_3, gpio.OUT)
  gpio.write(gpio.LED_1, 0)
  gpio.write(gpio.LED_2, 0)
  gpio.write(gpio.LED_3, 0)
end

-- Evalua un argumento y 
-- enciende un led segun su valor
function eval(val)
  if (val<=300) then
    gpio.write(gpio.LED_1,1)
    gpio.write(gpio.LED_2,0)
    gpio.write(gpio.LED_3,0)
  elseif (val>300) and (val<800) then
    gpio.write(gpio.LED_1,0)
    gpio.write(gpio.LED_2,1)
    gpio.write(gpio.LED_3,0)
  elseif (val>=800) then
    gpio.write(gpio.LED_1,0)
    gpio.write(gpio.LED_2,0)
    gpio.write(gpio.LED_3,1)
  end
  tmr.delay(1000)
end

-- Inicio de la Aplicacion
print("Prueba del ADC, canal 1")
init()
salir = false
while not salir do
  dato = adc.read(0, 1)
  io.write("ADC Ch. 1: ", dato, "\n")	
  eval(dato)  
  print("Continuar? \"fin\" para")
  print(" terminar")
  salir = (io.read() == "fin")
end
