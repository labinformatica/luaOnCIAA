/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "romFiles.h"
#include "romfs.h"
#include <stdio.h>

extern const unsigned char romFiles[romFiles_SZ];

int main(int argc, char * argv[])
{
	unsigned int i;
	unsigned int totalUsed = 0;
	dirEntry dir;
	fileEntry *file;

	printf("File count: %d\n", romfsGetFileCount(romFs));
	printf("Used size: %d\n", romfsGetUsedSpace(romFs));
	printf("Free size: %d\n", romfsGetFreeSpace(romFs));

	romfsOpenDir(romFs, &dir);
	while((file = romfsReadDir(&dir)) != 0)
	{
		printf("\t-> %s [%d]\n", file->fn, file->size);
		totalUsed += file->size;
	}
    printf("Total used: %d\n", totalUsed);
	if(romfsFileExists(romFs, "xxx.txt"))
		printf("Existe: xxx.txt\n");
	else
		printf("No existe: xxx.txt\n");

	if(romfsFileExists(romFs, "romFileSystemTop.c"))
		printf("Existe: romFileSystemTop.c\n");
	else
		printf("No existe: romFileSystemTop.c\n");


	return 0;
}
