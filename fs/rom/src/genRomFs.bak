#include  <stdio.h>
#include  <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

typedef struct 
{
  unsigned int fileNameLen;
  char fileName[255];
  unsigned int dataOffset;
  unsigned int dataLen;
}romFileEntry;


int parseArguments(int argc, char *argv[], char **fsFolder, char **outFolder, char **fsName)
{
  int ret = -3;
  int i = 1;
  while(i < argc) 
  {
    if((strcmp(argv[i], "--fsPath")==0) && ((i+1)<argc))
    {
      *fsFolder = argv[i+1];
      ret ++;
    }
    else if((strcmp(argv[i], "--outPath")== 0) && ((i+1)<argc))
    { 
      *outFolder= argv[i+1];
      ret ++;
    }
    else if((strcmp(argv[i], "--fsName")== 0) && ((i+1)<argc))
    { 
      *fsName= argv[i+1];
      ret ++;
    }
    i++;
  }
  return !ret;
}

int createOutFolder(char * folderName)
{
  int retTop, retInc, retSrc;
  char fn[1024];
  struct stat fileStatTop;
  struct stat fileStatInc;
  struct stat fileStatSrc;

  if(stat(folderName, &fileStatTop) == -1)
    mkdir(folderName, 0777);
  retTop = stat(folderName, &fileStatTop);

  strcpy(fn, folderName);
  strcat(fn, "/inc");
  if(stat(fn, &fileStatInc) == -1)
    mkdir(fn, 0777);
  retInc = stat(fn, &fileStatInc);

  strcpy(fn, folderName);
  strcat(fn, "/src");
  if(stat(fn, &fileStatSrc) == -1)
    mkdir(fn, 0777);
  retSrc = stat(fn, &fileStatSrc);

  return  (retTop == 0) && (S_ISDIR(fileStatTop.st_mode)) &&
          (retInc == 0) && (S_ISDIR(fileStatInc.st_mode)) && 
          (retSrc == 0) && (S_ISDIR(fileStatSrc.st_mode));
} 

int main(int argc, char *argv[])
{
  char * outFolder;
  char * fsFolder;
  char * fsName;
  DIR *topDir;
  struct dirent *fsEntry;
  char readFN[1024];
  char fsSrcFN[1024];

  unsigned int fileCounter = 0;  /* File counter */
  unsigned int romFsSize = 0;
  int i, j;
  unsigned int filePos;
  unsigned char fileData;
  FILE * readFile;
  FILE * fsSrc;
  unsigned int lineBreakCounter = 0;
  unsigned char firstValue;
  unsigned char *pData;
  romFileEntry *files;
  if(parseArguments(argc, argv, &fsFolder, &outFolder, &fsName))
  {
    if(!createOutFolder(outFolder))
    {
      printf("I can't create the output folder (%s)\nSorry!\n", outFolder);
      return -1;
    }

    topDir = opendir(fsFolder);
    if(topDir)
    {
       fileCounter = 0;
       // Contamos cuantos archivos hay en el directorio en cuestión para dimensionar las estructuras.
       while((fsEntry = readdir(topDir)) != NULL)
       { 
          if(fsEntry->d_type == DT_REG)
            fileCounter++;
       }
      files = (romFileEntry *)malloc(fileCounter * sizeof(romFileEntry));
      // Reiniciamos la lectura del directorio
      rewinddir(topDir);
      i = 0;
      romFsSize = 4; // por el tamaño de una variable entera;
      while((fsEntry = readdir(topDir)) != NULL)
      { 
         if((fsEntry->d_type == DT_REG) && (i < fileCounter))
         {
        	// Contamos el tamaño utilizado para almacenar la longitud del nombre del archivo.
        	romFsSize ++;
            // Cargamos solo el nombre del archivo y su longitud
            strcpy(readFN, fsFolder); 
            strcat(readFN, "/"); 
            strcat(readFN, fsEntry->d_name);
            romFsSize += strlen(fsEntry->d_name) + 1;

            files[i].fileNameLen = strlen(fsEntry->d_name);
            strcpy(files[i].fileName, fsEntry->d_name);
            readFile = fopen(readFN, "rb");
            fseek(readFile, 0, SEEK_END);
            files[i].dataLen = ftell(readFile);
            fclose(readFile);
            i++;
            // sumamos el offset
            romFsSize += 4;
            // sumamos el espacio ocupado por el tamaño del archivo
            romFsSize += 4;
         }
      }
      // Abrimos el archivo donde se constituirá el file system.
      strcpy(fsSrcFN, outFolder); 
      strcat(fsSrcFN, "/src/");          
      strcat(fsSrcFN, fsName);
      strcat(fsSrcFN, ".c");          
      fsSrc = fopen(fsSrcFN, "wt");
      if(!fsSrc)
      {
         printf("I can't write the output file (%s)\n", fsSrcFN);
         return -1;
      }
      fprintf(fsSrc, "#include \"%s.h\"\n\n", fsName);
      fprintf(fsSrc, "const unsigned char %s[%s_SZ] = {\n", fsName, fsName);
      pData = (unsigned char *)&fileCounter;
      for(i=0; i<sizeof(fileCounter); i++)
         fprintf(fsSrc, "0x%02x, ", (unsigned int)pData[i]);
      fprintf(fsSrc, "\n");

      for(i = 0; i < fileCounter; i++)
      {
        fprintf(fsSrc, "0x%02x, ", (unsigned int)(files[i].fileNameLen & 0xFF));
        for(j = 0; j < strlen(files[i].fileName); j++)
        	fprintf(fsSrc, "0x%02x, ", (unsigned int)(files[i].fileName[j] & 0xFF));
        fprintf(fsSrc, "0x00, ");
        fprintf(fsSrc, " /* File # %d */\n ", i+1);
        // Guardamos el desplazamiento desde el comienzo del archivo donde empiezan los datos
        pData = (unsigned char *)&romFsSize;
        for(j=0; j<sizeof(romFsSize); j++)
           fprintf(fsSrc, "0x%02x, ", (unsigned int)pData[j]);
        // Guardamos el tamaño del archivo
        pData = (unsigned char *)&files[i].dataLen;
        for(j=0; j<sizeof(files[i].dataLen); j++)
           fprintf(fsSrc, "0x%02x, ", (unsigned int)pData[j]);
        fprintf(fsSrc, "\n");
        // Incrementamos el contador de desplazamiento para el siguiente archivo
        romFsSize += files[i].dataLen;
      }

      // Habiendo almacenado los nombres de los archivos, tamaños y desplazamientos,
      // ahora guardamos el contenido de los archivos
      lineBreakCounter = 0;
      for(i = 0; i < fileCounter; i++)
      {
          strcpy(readFN, fsFolder);
          strcat(readFN, "/");
          strcat(readFN, files[i].fileName);
          readFile = fopen(readFN, "rb");
          for(filePos = 0; filePos < files[i].dataLen; filePos++)
          {
        	  fread(&fileData, 1, 1, readFile);
        	  fprintf(fsSrc, "0x%02x, ", (unsigned int)fileData);
        	  lineBreakCounter++;
        	  if(lineBreakCounter == 15)
        	  {
        		  lineBreakCounter = 0;
        		  fprintf(fsSrc, "\n");
        	  }
          }

          fclose(readFile);
      }
      fprintf(fsSrc, "\n};\n");
      fclose(fsSrc);
      free(files);
      closedir(topDir);
      strcpy(fsSrcFN, outFolder);
      strcat(fsSrcFN, "/inc/");
      strcat(fsSrcFN, fsName);
      strcat(fsSrcFN, ".h");
      fsSrc = fopen(fsSrcFN, "wt");
      if(!fsSrc)
      {
         printf("I can't write the output file (%s)\n", fsSrcFN);
         return -1;
      }
      fprintf(fsSrc, "#ifndef %s_h\n", fsName);
      fprintf(fsSrc, "#define %s_h\n\n", fsName);
      fprintf(fsSrc, "#define %s_SZ %u\n\n", fsName,romFsSize);
      fprintf(fsSrc, "#endif\n");
      fclose(fsSrc);
    }
    else
    {
      printf("I can't open the folder with file system (%s)\n", fsFolder);
      return -1;
    }
  }
  else
  {
    printf("This application generate sources files to build a read only file system.\n");
    printf("You must use 2 arguments to invoke this tool:\n");
    printf("--fsPath: path to the folder with the files to include in the file system. Only files are included folders or directorys are omited\n");
    printf("--outPath: path where the output files (source files) are generated.\n");
    printf("--fsName: name for the source file, header file and C symbol.\n");
  }

  return 0;
}

