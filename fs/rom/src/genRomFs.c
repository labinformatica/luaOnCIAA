/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include  <stdio.h>
#include  <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

typedef struct 
{
  unsigned int fileNameLen;
  char fileName[255];
  unsigned int dataOffset;
  unsigned int dataLen;
}romFileEntry;


int parseArguments(int argc, char *argv[], char **fsFolder, char **outFolder, char **fsName)
{
  int ret = -3;
  int i = 1;
  while(i < argc) 
  {
    if((strcmp(argv[i], "--fsPath")==0) && ((i+1)<argc))
    {
      *fsFolder = argv[i+1];
      ret ++;
    }
    else if((strcmp(argv[i], "--outPath")== 0) && ((i+1)<argc))
    { 
      *outFolder= argv[i+1];
      ret ++;
    }
    else if((strcmp(argv[i], "--fsName")== 0) && ((i+1)<argc))
    { 
      *fsName= argv[i+1];
      ret ++;
    }
    i++;
  }
  return !ret;
}

int createOutFolder(char * folderName)
{
  int retTop, retInc, retSrc;
  char fn[1024];
  struct stat fileStatTop;
  struct stat fileStatInc;
  struct stat fileStatSrc;

  if(stat(folderName, &fileStatTop) == -1)
    mkdir(folderName, 0777);
  retTop = stat(folderName, &fileStatTop);

  strcpy(fn, folderName);
  strcat(fn, "/inc");
  if(stat(fn, &fileStatInc) == -1)
    mkdir(fn, 0777);
  retInc = stat(fn, &fileStatInc);

  strcpy(fn, folderName);
  strcat(fn, "/src");
  if(stat(fn, &fileStatSrc) == -1)
    mkdir(fn, 0777);
  retSrc = stat(fn, &fileStatSrc);

  return  (retTop == 0) && (S_ISDIR(fileStatTop.st_mode)) &&
          (retInc == 0) && (S_ISDIR(fileStatInc.st_mode)) && 
          (retSrc == 0) && (S_ISDIR(fileStatSrc.st_mode));
} 

int main(int argc, char *argv[])
{
  char * outFolder;
  char * fsFolder;
  char * fsName;
  DIR *topDir;
  struct dirent *fsEntry;
  char readFN[1024];
  char fsSrcFN[1024];

  unsigned int fileCounter = 0;  /* File counter */
  unsigned int romFsSize = 0;
  int i, j, k;
  unsigned int filePos;
  unsigned char fileData;
  FILE * readFile;
  FILE * fsSrc;
  unsigned int lineBreakCounter = 0;
  unsigned char firstValue;
  unsigned char *pData;
  unsigned int fileLength;
  unsigned char dataByte;

  if(parseArguments(argc, argv, &fsFolder, &outFolder, &fsName))
  {
    if(!createOutFolder(outFolder))
    {
      printf("No ha sido posible crear el directorio %s\nPerdón!\n", outFolder);
      return -1;
    }

    topDir = opendir(fsFolder);
    if(topDir)
    {
       fileCounter = 0;
       // Contamos cuantos archivos hay en el directorio en cuestión para dimensionar las estructuras.
       while((fsEntry = readdir(topDir)) != NULL)
       { 
          if(fsEntry->d_type == DT_REG)
            fileCounter++;
       }


      // Abrimos el archivo donde se constituirá el file system.
      strcpy(fsSrcFN, outFolder); 
      strcat(fsSrcFN, "/src/");          
      strcat(fsSrcFN, fsName);
      strcat(fsSrcFN, ".c");          
      fsSrc = fopen(fsSrcFN, "wt");
      if(!fsSrc)
      {
         printf("No he podido abrir el archivo de datos (%s).\n", fsSrcFN);
         return -1;
      }
      fprintf(fsSrc, "#include \"%s.h\"\n\n", fsName);
      fprintf(fsSrc, "const unsigned char %s[%s_SZ] = {\n", fsName, fsName);
      // Escribimos la cantidad de archivos que se detectaron.
      pData = (unsigned char *)&fileCounter;
      for(i=0; i<sizeof(fileCounter); i++)
        fprintf(fsSrc, "0x%02x, ", (unsigned int)pData[i]);
      fprintf(fsSrc, "/* Cantidad de archivos detectados */\n");

      // Reiniciamos la lectura del directorio
      rewinddir(topDir);
      i = 0;
      romFsSize = 4; // por el tamaño de una variable entera;
      while((fsEntry = readdir(topDir)) != NULL)
      { 
         if((fsEntry->d_type == DT_REG) && (i < fileCounter))
         {
            // Cargamos solo el nombre del archivo y su longitud
            strcpy(readFN, fsFolder); 
            strcat(readFN, "/"); 
            strcat(readFN, fsEntry->d_name);
            romFsSize += strlen(fsEntry->d_name) + 1;
            // Grabamos el nombre del archivo incluyendo "\0"
            for(k=0; k <= strlen(fsEntry->d_name); k++)
               fprintf(fsSrc, "0x%02x, ", (unsigned int)fsEntry->d_name[k]);
            fprintf(fsSrc, "/* Nombre del archivo %d */\n", i + 1);

            readFile = fopen(readFN, "rb");
            fseek(readFile, 0, SEEK_END);
            
	    fileLength = ftell(readFile);
            pData = (unsigned char *)&fileLength;
            for(k=0; k<sizeof(fileCounter); k++)
              fprintf(fsSrc, "0x%02x, ", (unsigned int)pData[k]);
            fprintf(fsSrc, "/* Tamaño del archivo %d */\n", i + 1);
            romFsSize += 4;

            fseek(readFile, 0, SEEK_SET);
            for(k=0;k<fileLength;k++)
            {
               fread(&dataByte, 1, 1, readFile);
               fprintf(fsSrc, "0x%02x, ", (unsigned int)dataByte); 
	    }
            fprintf(fsSrc, "/* Datos del archivo %d */\n", i + 1);
            romFsSize += fileLength;
            fclose(readFile);
            i++;
         }
      }
     
      fprintf(fsSrc, "\n}	;\n");
      fclose(fsSrc);

      closedir(topDir);
     
      strcpy(fsSrcFN, outFolder);
      strcat(fsSrcFN, "/inc/");
      strcat(fsSrcFN, fsName);
      strcat(fsSrcFN, ".h");
      fsSrc = fopen(fsSrcFN, "wt");
      if(!fsSrc)
      {
         printf("I can't write the output file (%s)\n", fsSrcFN);
         return -1;
      }
      fprintf(fsSrc, "#ifndef %s_h\n", fsName);
      fprintf(fsSrc, "#define %s_h\n\n", fsName);
      fprintf(fsSrc, "#define %s_SZ %u\n\n", fsName,romFsSize);
      fprintf(fsSrc, "#endif\n");
      fclose(fsSrc);
    }
    else
    {
      printf("I can't open the folder with file system (%s)\n", fsFolder);
      return -1;
    }
  }
  else
  {
    printf("This application generate sources files to build a read only file system.\n");
    printf("You must use 2 arguments to invoke this tool:\n");
    printf("--fsPath: path to the folder with the files to include in the file system. Only files are included folders or directorys are omited\n");
    printf("--outPath: path where the output files (source files) are generated.\n");
    printf("--fsName: name for the source file, header file and C symbol.\n");
  }

  return 0;
}

