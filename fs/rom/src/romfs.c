/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <romfs.h>
#include <string.h>

// Warning! In the next code, there are a access to unsigned int vars using pointers to char. This is because using other pointer type
// causes access to unaligned memory access on Cortex M0 with Gcc. 

// Ok
unsigned int romFsGetFileCount(unsigned char * romFs)
{
   return (romFs[3] << 24) | (romFs[2] << 16) | (romFs[1] << 8) | (romFs[0]);
}

// Ok
unsigned int romFsGetUsedSpace(unsigned char * romFs)
{
	unsigned int fileSize = 0;
	unsigned int accSize = 0;
	unsigned int offSet = 4;
	unsigned int fCount;
	unsigned int i;
	fCount = (romFs[3] << 24) | (romFs[2] << 16) | (romFs[1] << 8) | (romFs[0]);
	for(i=0; i < fCount; i++)
	{
		offSet += strlen((char *)&romFs[offSet]) + 1;
		fileSize = (romFs[offSet + 3] << 24) | (romFs[offSet + 2] << 16) | (romFs[offSet + 1] << 8) | (romFs[offSet]);
		accSize += fileSize;
		offSet += 4;
		offSet += fileSize;
	}
	return accSize;
}
// Ok
unsigned int romFsGetFreeSpace(unsigned char * romFs)
{
	return 0;
}

unsigned char romFsFileExists(unsigned char * romFs, char *fn)
{
	unsigned char finded = 0;
	unsigned int fileSize = 0;
	unsigned int offSet = 4;
	unsigned int fCount;
	unsigned int i;
	fCount = (romFs[3] << 24) | (romFs[2] << 16) | (romFs[1] << 8) | (romFs[0]);
	for(i=0; (i < fCount) && (!finded); i++)
	{
		finded = strcmp((char *)&romFs[offSet], fn) == 0;
		offSet += strlen((char *)&romFs[offSet]) + 1;
		fileSize = (romFs[offSet + 3] << 24) | (romFs[offSet + 2] << 16) | (romFs[offSet + 1] << 8) | (romFs[offSet]);
		offSet += 4;
		offSet += fileSize;
	}
	return finded;
}

void romFsOpenDir(struct stFileSystems *romFs, dirEntry *dir)
{
  dir->idxEntry = 0;
  dir->fs = romFs;
  dir->lastOffset = 4;
}

fileEntry* romFsReadDir(dirEntry *dir)
{
  unsigned char *pSize;
  fileEntry* ret = 0;
  if(dir->idxEntry < romFsGetFileCount(dir->fs->fsData))
  {
	  dir->fe.fn = &(((char *)dir->fs->fsData)[dir->lastOffset]);
	  dir->fe.status = FILE_OK;
	  dir->fe.memOffset = (uint32_t)&(((char *)dir->fs->fsData)[dir->lastOffset]);
	  dir->lastOffset += strlen(&(((char *)dir->fs->fsData)[dir->lastOffset])) + 1;
	  // Acessing as unsigned int * causes hardware fault (!?) on cortex-m0 with gcc.
      pSize = (unsigned char *)(dir->fs->fsData + dir->lastOffset);
 	  dir->fe.size = (pSize[3] << 24) | (pSize[2] << 16) | (pSize[1] << 8) | (pSize[0]);
	  dir->lastOffset += 4;
	  dir->lastOffset += dir->fe.size;
	  dir->idxEntry++;
	  ret = &(dir->fe);
  }
  return ret;
}

void romFsCloseDir(dirEntry *dir)
{

}

// Funciones de acceso a archivos
extern fileDesc fileDescriptorTable[MAXFILEDESCRIPTORS];

int romFsOpen(char *name, int flags, int mode, struct stFileSystems *fs)
{
  dirEntry dir;
  fileEntry *file;
  romFsFileData *dataFile;
  unsigned char found = 0;
  int fd = getFirstFreeFD();
  if (fd >= 0)
  {
    fileDescriptorTable[fd].fs = fs;
    romFsOpenDir(((struct stFileSystems*)fs), &dir);
    while(((file = romFsReadDir(&dir)) != 0) && (!found))
    {
    	if(strcmp(file->fn, &name[strlen(((struct stFileSystems*)fs)->fsName)+1]) == 0)
    	{
    		dataFile = malloc(sizeof(romFsFileData));
    		dataFile->name = file->fn;
    		dataFile->fileLen = file->size;
    		dataFile->dataFile = (unsigned char *)file->memOffset + strlen(file->fn) + 1 + 4;
    		dataFile->pos = 0;
    		fileDescriptorTable[fd].fileData = dataFile;
    		found = 1;
    	}
    }
    romFsCloseDir(&dir);
    if (!found)
    {
    	freeFD(fd);
    	fd = -1;
    }
  }
  return fd;
}

int romFsRead (int fd, uint8_t *buf, int len)
{
  int i;
  int ret = 0;
  romFsFileData *dataFile;
  if (fd >= 0)
  {
	dataFile = fileDescriptorTable[fd].fileData;
	if(dataFile->pos < dataFile->fileLen)
	{
      for(i=0; (i < len) && (dataFile->pos < dataFile->fileLen); i++)
      {
	    buf[i] = dataFile->dataFile[dataFile->pos];
	    dataFile->pos++;
	    ret ++;
      }
	}
  }
  return ret;
}

long romFsSeek(int fd, long off, int from)
{
	romFsFileData *dataFile;
	long ret = -1;
	if (fd >= 0)
	{
		dataFile = fileDescriptorTable[fd].fileData;
		switch(from)
		{
		  case SEEK_SET:
			  dataFile->pos = off>dataFile->fileLen?dataFile->fileLen:off;
			  break;
	      case SEEK_CUR:
	    	  if((dataFile->pos + off < dataFile->fileLen) && ((dataFile->pos + off >= 0)))
	    		  dataFile->pos = dataFile->pos + off;
			break;
		  case SEEK_END:
			  if((dataFile->fileLen + off >= 0) && ((dataFile->fileLen + off <= dataFile->fileLen)))
			  	  dataFile->pos = dataFile->fileLen + off;
			break;
		}
		ret = dataFile->pos;;
	}
	return ret;
}

int romFsClose(int fd)
{
	if (fd >= 0)
	{
		free((romFsFileData*)fileDescriptorTable[fd].fileData);
		freeFD(fd);
	}
	return 0; // No es necesario hacer nada
}
