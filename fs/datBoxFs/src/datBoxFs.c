﻿/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <datBoxFs.h>
#include <string.h>
#include <fs.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef __cplusplus
 extern "C"{
#endif
/*
 * Copia la dirección de un puntero (from) a otro (to). Se utiliza esta función en vez de
 * hacer la asignación de modo directo para resolver el bug de acceso a memoria que se da
 * en el CortexM0 cuando la memoria alojada no tienen alineación de 4 bytes.
 * */

//void copyPtr(uint32_t *to, uint32_t * from)
//{
//
//	int i;
//	for (i = 0; i < sizeof(uint32_t *); i++)
//		((unsigned char *) to)[i] = ((unsigned char *) from)[i];
//}


/*
 *
 * Actualiza una entrada de un archivo dado (nombre y primer bloque asociado).
 */

void dbfsSetFileEntryFromId(dbfsRoot *fs, int id, dbfsFileEntry *fe)
{
	unsigned int blkNro;
	uint32_t addrOff;
	unsigned int blkOff;
	blkNro = ((id * sizeof(dbfsFileEntry)) / fs->blkSize);
	blkOff = ((id * sizeof(dbfsFileEntry)) % fs->blkSize);
	addrOff = (blkNro * fs->blkSize);
    fs->fcnRd(fs, fs->rwBuff, addrOff, fs->blkSize, fs->absAddr);
	memcpy(&fs->rwBuff[blkOff], fe, sizeof(dbfsFileEntry));
    fs->fcnWr(fs, addrOff, fs->rwBuff, fs->blkSize, fs->absAddr);
}

/*
 * Completa los campos de una estructuras dbfsFileEntry recibida por su dirección de memoria,
 * según el valor del id indicado.
 */
void dbfsGetFileEntryFromId(dbfsRoot *fs, int id, dbfsFileEntry *fe)
{
	unsigned int blkNro;
	uint32_t blkAddr;
	unsigned int blkOff;
	blkNro = ((id * sizeof(dbfsFileEntry)) / fs->blkSize);
	blkOff = ((id * sizeof(dbfsFileEntry)) % fs->blkSize);
	blkAddr = (blkNro * fs->blkSize);
    fs->fcnRd(fs, fs->rwBuff, blkAddr, fs->blkSize, fs->absAddr);
	*fe = *((dbfsFileEntry *) &fs->rwBuff[blkOff]);
}

/*
 * Retorna el tamaño del archivo.
 */

uint32_t dbfsGetFileSize(dbfsRoot *fs, int id, uint8_t *status)
{
	unsigned int blkNro;
	uint32_t blkAddr;
	unsigned int blkOff;
	dbfsFileEntry *fe;
	uint32_t size;
	uint8_t i;
	uint32_t blkCnt = 1;
	uint32_t maxSize;
	// Identificamos el bloque asociado al archivo a partir de su Id
	blkNro = ((id * sizeof(dbfsFileEntry)) / fs->blkSize);
	blkOff = ((id * sizeof(dbfsFileEntry)) % fs->blkSize);
	blkAddr = (blkNro * fs->blkSize);
    fs->fcnRd(fs, fs->rwBuff, blkAddr, fs->blkSize, fs->absAddr);
	fe = ((dbfsFileEntry *) &fs->rwBuff[blkOff]);
	// Obtenemos el punto de entrada de los bloques de datos.
	blkAddr = fe->firstBlock;
	while (blkAddr != USED_ENTRY)
	{
		blkCnt++;
        fs->fcnRd(fs, fs->rwBuff, blkAddr, fs->blkSize, fs->absAddr);
        copyPtr(&blkAddr, (uint32_t *)&fs->rwBuff[(fs->blkSize - sizeof(uint32_t))]);
        if(checkEmpty(blkAddr)!=0){
        	*status = FILE_CORRUPT;
        	return 0;
        }
	}

	size = 0;
	for(i = 4; i >= 1; i--)
	{
		size = size << 8;
        size = size | fs->rwBuff[fs->blkSize - sizeof(uint32_t) - i];
	}
    *status = FILE_OK;
	maxSize = (blkCnt * (fs->blkSize - sizeof(uint32_t))) - 4;
	if (size > maxSize)
	{
		size = maxSize;
		*status = FILE_SIZE_ERROR;
	}
	return size;
}


uint8_t checkEmpty(uint32_t toTest)
{
	uint8_t ret = 1;
    uint8_t i;
    for (i = 0; (i < sizeof(uint32_t)) && ret; i++)
    {
        ret = ret && (((uint32_t) toTest >> (i * 8)) & 0xFF) == EMPTY_ENTRY;
    }
	return ret;
}

void dbfsFormat(struct stFileSystems *genFs)
{
	unsigned int i;
	dbfsRoot *dbfs = (dbfsRoot *)genFs->fsData;
	unsigned int maxIterations = dbfs->length / dbfs->blkSize;
	uint32_t p = 0;
	for (i = 0; i < dbfs->blkSize; i++)
		dbfs->rwBuff[i] = (unsigned char) (EMPTY_ENTRY);
	for (i = 0; i < maxIterations; i++) {
        dbfs->fcnWr(dbfs, p, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
		p = p + dbfs->blkSize;
	}
}

/* Si encuentra el archivo retorna el número de bloque de entrada con base en cero. A partir de este valor,
 *  y considerando el tamaño de cada bloque así como el de la estructura se puede obtener la dirección del bloque
 *  y el desplazamiento sobre el mismo.
 *  Si no existe retorna un valor negativo.
 */

int32_t dbfsFileExists(dbfsRoot *fs, char *fn)
{
    uint32_t i, j;
	uint32_t k;
	dbfsFileEntry *pFe;
	int ret = -1;
	int entryNro = 0;
	unsigned int entrySize = fs->maxFiles * sizeof(dbfsFileEntry);
	unsigned int fe4Blk = fs->blkSize / sizeof(dbfsFileEntry);
	for (k = 0; (k < entrySize) && (ret < 0); k = k + fs->blkSize) {
        fs->fcnRd(fs, fs->rwBuff, k, fs->blkSize, fs->absAddr);
        for (i = 0; (i < fe4Blk) && (ret < 0); i++)
        {
			pFe = (dbfsFileEntry*) &fs->rwBuff[i * sizeof(dbfsFileEntry)];
            if(!checkEmpty(pFe->firstBlock))
            {
              j = 0;
              while ((pFe->fn[j] == fn[j]) && pFe->fn[j] && fn[j])
				j++;
              if ((fn[j] == pFe->fn[j]) && (fn[j] == '\0'))
				ret = entryNro;
            }
            entryNro++;
		}
	}
	return ret;
}

/*
 *  Trunca un archivo existente. Libera los bloques asociados al mismo pero
 *  y actualiza el punto de entrada en el file system.
 */

void dbfsTruncFile(dbfsFileState *fState)
{
    uint32_t j;
	dbfsFileEntry fe;
	uint32_t strBlk;
	uint32_t nxtBlk;
	dbfsGetFileEntryFromId(fState->dbfs, fState->entryId, &fe);
	/* El primer bloque no debe marcarse como libre, siempre debe ser marcado
	 * como ocupado.
	 * */
	strBlk = fe.firstBlock;
    fState->dbfs->fcnRd(fState->dbfs, fState->dbfs->rwBuff, strBlk, fState->dbfs->blkSize, fState->dbfs->absAddr);
    copyPtr(&nxtBlk, (uint32_t *)&fState->dbfs->rwBuff[fState->dbfs->blkSize - sizeof(uint32_t)]);
    for(j=0; j< sizeof(uint32_t);j++)
		fState->dbfs->rwBuff[fState->dbfs->blkSize - 1 - j] = USED_ENTRY;
	for(j=0; j< sizeof(uint32_t);j++)
        fState->dbfs->rwBuff[fState->dbfs->blkSize - sizeof(uint32_t) - 1 - j] = 0x00;
    fState->dbfs->fcnWr(fState->dbfs, strBlk, fState->dbfs->rwBuff, fState->dbfs->blkSize, fState->dbfs->absAddr);
	strBlk = nxtBlk;
	while (!checkEmpty(strBlk) && (strBlk != USED_ENTRY))
	{
        fState->dbfs->fcnRd(fState->dbfs, fState->dbfs->rwBuff, strBlk, fState->dbfs->blkSize, fState->dbfs->absAddr);
        copyPtr(&nxtBlk, (uint32_t *)&fState->dbfs->rwBuff[fState->dbfs->blkSize - sizeof(uint32_t)]);
        for (j = 0; j < sizeof(uint32_t); j++)
			fState->dbfs->rwBuff[fState->dbfs->blkSize - (1 + j)] = EMPTY_ENTRY;
        fState->dbfs->fcnWr(fState->dbfs, strBlk, fState->dbfs->rwBuff, fState->dbfs->blkSize, fState->dbfs->absAddr);
		strBlk = nxtBlk;
	}
}

// Retorna el desplazamiento en el buffer asociado a la entrada del archivo creado
int dbfsCreateFile(dbfsRoot *fs, const char *fn)
{
	unsigned int i, j, k;
	int ret = -1;
	dbfsFileEntry * pFe;
	unsigned int entrySize = fs->maxFiles * sizeof(dbfsFileEntry);
	unsigned int fe4Blk = fs->blkSize / sizeof(dbfsFileEntry);
	int entryNro = 0;
	uint32_t firstBlock = dbfsGetFreeBlock(fs, USED_ENTRY);
	if (firstBlock == invalidAddr)
		return -1;
	for (k = 0; (k < entrySize) && (ret < 0); k = k + fs->blkSize)
	{
        fs->fcnRd(fs, fs->rwBuff, k, fs->blkSize, fs->absAddr);
		for (i = 0; (i < fe4Blk) && (ret < 0); i++)
		{
			pFe = (dbfsFileEntry*) &fs->rwBuff[i * sizeof(dbfsFileEntry)];
			if (checkEmpty(pFe->firstBlock))
			{
				pFe->firstBlock = firstBlock;
				strcpy(pFe->fn, fn);
				ret = entryNro;
                fs->fcnWr(fs, k, fs->rwBuff, fs->blkSize, fs->absAddr);
				// Marcamos el bloque como usado
                fs->fcnRd(fs, fs->rwBuff, firstBlock, fs->blkSize, fs->absAddr);
                for (j = fs->blkSize - 1; j > fs->blkSize - 1 - sizeof(uint32_t); j--)
					fs->rwBuff[j] = USED_ENTRY;
				// Fijamos como tamaño cero
				for(j=0; j< sizeof(uint32_t);j++)
                    fs->rwBuff[fs->blkSize - sizeof(uint32_t) - 1 - j] = 0x00;
                fs->fcnWr(fs, firstBlock, fs->rwBuff, fs->blkSize, fs->absAddr);
			}
			entryNro++;
		}
	}
	return ret;
}

uint32_t dbfsGetFreeBlock(dbfsRoot *fs, uint32_t differentOf)
{
	uint32_t find = invalidAddr;
	uint32_t pPrev;
	unsigned int i;
	unsigned char endFree;
	uint32_t auxPtr;
	uint32_t fsLastAddr = fs->length;
	pPrev = (fs->maxFiles * sizeof(dbfsFileEntry));
	do {
		if (pPrev != differentOf)
		{
            fs->fcnRd(fs, (unsigned char *)&auxPtr, pPrev + fs->blkSize - sizeof(uint32_t), sizeof(uint32_t), fs->absAddr);
			endFree = 1;
            for (i = 0; (i < sizeof(uint32_t)) && (endFree); i++)
				endFree = ((auxPtr >> (i*8)) & 0xFF) == EMPTY_ENTRY;
			if (endFree)
				find = pPrev;
			else
				pPrev += fs->blkSize;
		} else
			pPrev += fs->blkSize;

	} while ((find == invalidAddr) && (pPrev < fsLastAddr));
	return find;
}


int dbfsUnlink( char *fn, struct stFileSystems *xfs)
{
	dbfsRoot *dbfs = (dbfsRoot *) xfs->fsData;
	int idx = dbfsFileExists(dbfs, (char *) &fn[strlen(((struct stFileSystems*)xfs)->fsName)+1]);
	unsigned int i, j;
	uint32_t blkEntry;
	uint32_t nextBlk;
	// No existe el archivo.
	if (idx < 0)
		return idx;
	unsigned int byteOff = idx * sizeof(dbfsFileEntry);
	unsigned int blkNro = (byteOff / dbfs->blkSize);
	uint32_t blkAddr = (blkNro * dbfs->blkSize);
	unsigned int blkOff = (byteOff % dbfs->blkSize);
    dbfs->fcnRd(dbfs, dbfs->rwBuff, blkAddr, dbfs->blkSize, dbfs->absAddr);

	blkEntry = ((dbfsFileEntry *) &dbfs->rwBuff[blkOff])->firstBlock;
	for (i = 0; i < sizeof(dbfsFileEntry); i++)
		dbfs->rwBuff[i + blkOff] = EMPTY_ENTRY;
    dbfs->fcnWr(dbfs, blkAddr, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
	while (!checkEmpty(blkEntry) && (blkEntry != USED_ENTRY)) {
        dbfs->fcnRd(dbfs, dbfs->rwBuff, blkEntry, dbfs->blkSize, dbfs->absAddr);
        copyPtr(&nextBlk, (uint32_t *)&dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t)]);
        for (j = dbfs->blkSize - 1 - sizeof(uint32_t); j < dbfs->blkSize; j++)
			dbfs->rwBuff[j] = EMPTY_ENTRY;
        dbfs->fcnWr(dbfs, blkEntry, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
		blkEntry = nextBlk;
	}
	return 0;
}

void dbfsOpenDir(struct stFileSystems *fs, dirEntry *dir)
{
    dir->fs = fs;
	dir->idxEntry = 0;
	dir->lastOffset = 0;
}

fileEntry* dbfsReadDir(dirEntry *dir)
{
	dbfsRoot * dbfs = (dbfsRoot *) dir->fs->fsData;
	uint32_t blkNro;
	uint32_t blkAddr;
	uint32_t blkOff;
	static dbfsFileEntry fe;
	while (dir->idxEntry < dbfs->maxFiles)
	{
		blkNro = ((dir->idxEntry*sizeof(dbfsFileEntry)) / dbfs->blkSize);
		blkOff = ((dir->idxEntry*sizeof(dbfsFileEntry)) % dbfs->blkSize);
		blkAddr = (blkNro * dbfs->blkSize);
        dbfs->fcnRd(dbfs, dbfs->rwBuff, blkAddr, dbfs->blkSize, dbfs->absAddr);
		memcpy(&fe, &dbfs->rwBuff[blkOff], sizeof(dbfsFileEntry));
		if (!checkEmpty(fe.firstBlock))
		{
			dir->fe.fn = fe.fn;
            dir->fe.memOffset = (uint32_t) (blkAddr + blkOff);
			dir->fe.size = dbfsGetFileSize(dbfs, dir->idxEntry, &(dir->fe.status));
			dir->idxEntry++;
			return &dir->fe;
		}
		dir->idxEntry++;
	}
	return 0;
}

void dbfsCloseDir(dirEntry *dir)
{

}

extern fileDesc fileDescriptorTable[MAXFILEDESCRIPTORS];

int dbfsOpen(char *name, int flags, int mode, struct stFileSystems *fs)
{
	uint8_t status;
	int fileEntryId = dbfsFileExists(fs->fsData, (char *) &name[strlen(((struct stFileSystems*)fs)->fsName)+1]);
	int ret = getFirstFreeFD();
	dbfsRoot * dbfs = ((dbfsRoot *) ((struct stFileSystems *) fs)->fsData);

	dbfsFileState *fileState;
	// Si el nombre es más largo que lo que soporta el fs -> fin
	if (strlen(&name[strlen(((struct stFileSystems*)fs)->fsName)+1]) >= dbFSMxFileNameLen)
		return -1;

	// Si no hay file descriptors disponibles suspendemos las operaciones
	if (ret == -1)
		return ret;

	// Dadas las características de la implementación de las operaciones de lectura
	// y escritura no hay diferencia en el modo de acceso al archivo. Por lo que si hay
	// file descriptors libres y el nombre es válido el archivo se abrirá.

	if (flags & O_CREAT)  // Bandera de creación del archivo
	{
		if (fileEntryId < 0) // Si el archivo no existe, se crea
		{
			fileEntryId = dbfsCreateFile(dbfs, &name[strlen(((struct stFileSystems*)fs)->fsName)+1]);
			if (fileEntryId >= 0) {
				fileState = malloc(sizeof(dbfsFileState));
				fileState->dbfs = (dbfsRoot *) fs->fsData;
				fileState->offSet = 0;
				fileState->entryId = fileEntryId;
				fileDescriptorTable[ret].fs = fs;
				fileDescriptorTable[ret].fileData = fileState;
			} else {
				freeFD(ret); // Liberamos el file descriptor
				return -1;
			}
		} else // El archivo existe y debe truncarse
		{
			fileState = malloc(sizeof(dbfsFileState));
			fileState->dbfs = (dbfsRoot *) fs->fsData;
			fileState->offSet = 0;
			fileState->entryId = fileEntryId;

			dbfsTruncFile(fileState);
			fileDescriptorTable[ret].fs = fs;
			fileDescriptorTable[ret].fileData = fileState;
		}
	} else // Solo debemos abrir el archivo
	{
		if (fileEntryId >= 0) {
			// Intentamos abrir el archivo
			fileState = malloc(sizeof(dbfsFileState));
			fileState->dbfs = dbfs;
			fileState->entryId = fileEntryId;
			fileState->offSet = 0;
			if (flags & O_TRUNC)
				dbfsTruncFile(fileState);
			fileDescriptorTable[ret].fs = fs;
			fileDescriptorTable[ret].fileData = fileState;
			if (flags & O_APPEND)
				fileState->offSet = dbfsGetFileSize(fileState->dbfs, fileState->entryId, &status);
		}
		else
		{
			freeFD(ret); // Liberamos el file descriptor porque no podemos abrir un archivo que no existe
			ret = -1;
		}
	}
	return ret;
}

int dbfsRead(int fd, uint8_t *buf, int len)
{
	dbfsFileState *fState;
	uint8_t status;
	dbfsFileEntry fe;
	unsigned int blkSpc;
	unsigned int blkCnt;
	unsigned int blkOff;
	unsigned int toRead;
	uint32_t fSize;
	int readCount = 0;
	int cont;
	uint32_t blkSrt;

	if (fd >= 0) {
		fState = (dbfsFileState*) fileDescriptorTable[fd].fileData;
		fSize = dbfsGetFileSize(fState->dbfs, fState->entryId, &status);
		dbfsGetFileEntryFromId(fState->dbfs, fState->entryId, &fe);
		// Si no hemos alcanzado el fin del archivo
		if (fState->offSet < fSize)
		{
            blkSpc = fState->dbfs->blkSize - sizeof(uint32_t);
			blkCnt = fState->offSet / blkSpc;
			blkOff = fState->offSet % blkSpc;
			blkSrt = fe.firstBlock;
			for (cont = 0; (cont < blkCnt) && (!checkEmpty(blkSrt)); cont++)
			{
                fState->dbfs->fcnRd(fState->dbfs, fState->dbfs->rwBuff, blkSrt, fState->dbfs->blkSize, fState->dbfs->absAddr);
                copyPtr(&blkSrt, (uint32_t *)&fState->dbfs->rwBuff[fState->dbfs->blkSize - sizeof(uint32_t)]);
			}
			/* Si el tamaño del archivo (como está registrado) es mayor que la estructura
			 * de listas enlazadas, finalizamos retornando 0. En caso que pueda iniciarse la
			 * lectura, pero después se encuentre que el siguiente nodo es nulo, también se
			 * finaliza con lo que se pudo leer.
			 */
			readCount = 0;
			while ((readCount < len) && (fState->offSet < fSize) && !checkEmpty(blkSrt)) {
                fState->dbfs->fcnRd(fState->dbfs, fState->dbfs->rwBuff, blkSrt, fState->dbfs->blkSize, fState->dbfs->absAddr);
				toRead = len - readCount;
				if(toRead > fSize - fState->offSet)
					toRead = fSize - fState->offSet;
				if (toRead > blkSpc - blkOff)
					toRead = blkSpc - blkOff;
				memcpy((unsigned char *) &buf[readCount],
						&fState->dbfs->rwBuff[blkOff], toRead);
				readCount += toRead;
				blkOff = 0;
				fState->offSet += toRead;
                copyPtr(&blkSrt, (uint32_t *)&fState->dbfs->rwBuff[fState->dbfs->blkSize - sizeof(uint32_t)]);
			}
		}
	}
	return readCount;
}

/*
 * En esta implementación se calculan los bloques alojados, los bloques necesarios y se
 * realiza el alojamiento de los bloques según se requieren, encadenando los bloques en
 * un solo paso.
 */
int dbfsWrite(int fd, const uint8_t *buf, int len)
{
	dbfsFileState *fState;
	dbfsFileEntry fe;
	dbfsRoot * dbfs;
	uint8_t status;
	unsigned int blkSpc;
	unsigned int blkOff;
	int writeCount = 0;
	unsigned int toWrite;
    int  i;
    uint32_t j;
	uint32_t blkSrt;
	uint32_t newBlk;
	uint32_t fSize;
    uint32_t needBlocks;
    uint32_t allocatedBlocks;
    uint32_t offBlocks;
    uint32_t blkNro;
    unsigned char writeEnd = 0;
	if (fd >= 0)
	{
		fState = (dbfsFileState*) fileDescriptorTable[fd].fileData;
		dbfs = fState->dbfs;
        blkSpc = dbfs->blkSize - sizeof(uint32_t);
		dbfsGetFileEntryFromId(dbfs, fState->entryId, &fe);
		fSize = dbfsGetFileSize(dbfs, fState->entryId, &status);

		/*
		 *  Calculamos la cantidad de bloques alojados
		 * */

		allocatedBlocks = (fSize + 4) / blkSpc;
		if (((fSize + 4) % blkSpc) != 0)
			allocatedBlocks  ++;

		/*
		 * Cantidad de bloques totales requeridos para alojar
		 * el archivo completo. Este número es coincidente con
		 * el número de bloque donde se debe guardar el tamaño
		 * del archivo.
		 * */

		needBlocks = (fState->offSet + len + 4)/ blkSpc;
		if (((fState->offSet + len + 4) % blkSpc) != 0)
			needBlocks++;

		/*
		 * Cantidad de bloques que debe avanzarce
		 * */

		offBlocks = (fState->offSet) / blkSpc;

		/*
		 * Avanzamos la cantidad necesaria de bloques en función del offset
		 * y contamos cuantos bloques hemos recorrido
		 * */

		blkNro = 1;
		blkSrt = fe.firstBlock;
		while(blkNro <= offBlocks)
		{
            dbfs->fcnRd(dbfs, dbfs->rwBuff, blkSrt, dbfs->blkSize, dbfs->absAddr);
            copyPtr(&blkSrt, (uint32_t *)&dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t)]);
			blkNro ++;
		}

		blkOff = (fState->offSet) % blkSpc;

		while(!writeEnd)
		{
			toWrite = len - writeCount;
			if (toWrite > blkSpc - blkOff)
				toWrite = blkSpc - blkOff;
			fState->offSet += toWrite;
			if (blkNro <= allocatedBlocks)
			{
                dbfs->fcnRd(dbfs, dbfs->rwBuff, blkSrt, dbfs->blkSize, dbfs->absAddr);
			    memcpy(&dbfs->rwBuff[blkOff], &buf[writeCount], toWrite);
			    blkOff = 0;
                copyPtr(&newBlk, (uint32_t *)&dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t)]);

			    /*
			     * Si el bloque no es el último y es igual a la cantidad de bloques alojados
			     * deberemos encadenarlo con el siguiente.
			     * */

			    if ((blkNro == allocatedBlocks) && (blkNro != needBlocks))
			    {
			    	newBlk = dbfsGetFreeBlock(dbfs, USED_ENTRY);
			    	/*
			    	 * Si no hay más bloques disponibles para realizar el alojamiento
			    	 * entonces no es posible escribir más información y finalizamos con lo
			    	 * escrito hasta el momento.
			    	 * */
			    	if (newBlk == invalidAddr)
			    	{
			    		newBlk = USED_ENTRY;
			    		needBlocks = blkNro;
			    	}
                    copyPtr((uint32_t *)&dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t)], &newBlk);
			    }

			    /*
			     * Si es el último bloque, agregamos el tamaño del archivo
			     * */

			    if (blkNro == needBlocks)
			    {
					if (fSize < fState->offSet)
						fSize = fState->offSet;
					for(i = 1; i <= 4; i++)
					{
                        dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t) - i] = fSize & 0xFF;
				     	fSize = fSize >> 8;
					}
					writeEnd = 1;
			    }
                dbfs->fcnWr(dbfs, blkSrt, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
			    blkSrt = newBlk;
			    writeCount += toWrite;
			    blkNro++;
			}
			else
			{
                dbfs->fcnRd(dbfs, dbfs->rwBuff, blkSrt, dbfs->blkSize, dbfs->absAddr);
				memcpy(&dbfs->rwBuff[blkOff], &buf[writeCount], toWrite);
			    /*
			     * Si es el último bloque, agregamos el tamaño del archivo
			     * y la marca de fin.
			     * */
			    if (blkNro == needBlocks)
			    {
                    for(j = 0; j < sizeof(uint32_t); j++)
						dbfs->rwBuff[dbfs->blkSize - 1 - j] = USED_ENTRY;
					if (fSize < fState->offSet)
						fSize = fState->offSet;
					for(i = 1; i <= 4; i++)
					{
                        dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t) - i] = fSize & 0xFF;
				     	fSize = fSize >> 8;
					}
                    dbfs->fcnWr(dbfs, blkSrt, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
					writeCount += toWrite;
					writeEnd = 1;
			    }
			    else
			    {
			    	/*
			    	 * Alojamos un nuevo bloque
			    	 * */
			    	newBlk = dbfsGetFreeBlock(dbfs, blkSrt);
			    	/*
			    	 * Lo encadenamos
			    	 * */
                    copyPtr((uint32_t *)&dbfs->rwBuff[dbfs->blkSize - sizeof(uint32_t)], &newBlk);
                    dbfs->fcnWr(dbfs, blkSrt, dbfs->rwBuff, dbfs->blkSize, dbfs->absAddr);
			    	blkSrt = newBlk;
			    	writeCount += toWrite;
			    }
			    blkNro++;
			}
		}

	}
	return writeCount;
}



long dbfsSeek(int fd, long off, int from)
{
	dbfsFileState *fState;
	uint8_t status;
	uint32_t fSize;
	long ret = -1;

	if (fd >= 0) {
		fState = fileDescriptorTable[fd].fileData;
		fSize = dbfsGetFileSize(fState->dbfs, fState->entryId, &status);
		switch (from) {
		case SEEK_SET:
			if ((off < fSize) && (off >= 0))
				fState->offSet = off;
			break;
		case SEEK_CUR:
			if ((fState->offSet + off <= fSize) && ((fState->offSet + off >= 0)))
				fState->offSet = fState->offSet + off;
			break;
		case SEEK_END:
			if (((fSize) + off >= 0) && (off <= 0))
				fState->offSet = fSize + off;
			break;
		}
		ret = fState->offSet;
	}
	return ret;
}

int dbfsClose(int fd)
{
	dbfsFileState *fState;

	if (fd >= 0) {
		fState = (dbfsFileState*) fileDescriptorTable[fd].fileData;
	    if (fState->dbfs->fcnCls)
	    	fState->dbfs->fcnCls(0);
		free((dbfsFileEntry*) fileDescriptorTable[fd].fileData);
		freeFD(fd);
	}
	return 0; // No es necesario hacer nada
}

#ifdef __cplusplus
 }
#endif
