/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* DatBox File System V1.2
 *
 * DatBox File System es un sistema de archivos diseñado para ser utilizado en sistemas embebidos,
 * con una estructura que permite ser integrado con newlib a través de implementaciones sencillas de system calls.
 * El sistema de almacenamiento de se basa en listas simplemente enlazadas. Cada archivo es una lista enlazada,
 * con un punto de entrada dado a partir de un vector de estructuras. Este vector de estructuras (dbfsFileEntry)
 * tiene una cantidad de elementos determinada por la cantidad máxima de archivos que se desea almacenar. Asu vez,
 * esta cantidad de archivos no puede tomar un valor aleatorio. El tamaño de cada estructura debe ser un submúltiplo
 * del tamaño del bloque que se utilizará para realizar operaciones de escritura. El factor de ajuste para lograr esto
 * es la longitud máxima de nombres de cada archivo.
 * Además, la cantidad máxima de archivos que se puede alojar debe ser múltiplo entero del tamaño de bloque de escritura.
 * Por ejemplo:
 * Si se tiene sizeof(dbfsFileEntry) =  64 bytes y el tamaño del sector es 512 bytes entonces la cantidad máxima
 * de archivos que se puede alojar debe ser un múltiplo de (512/64) = 8. Valores factibles son 8, 16, 32, 64, 128.
 *
 * Para resolver el problema de la alineación, todas las operaciones de lectura y escritura se realizan utilizando
 * un buffer del mismo tamaño que el tamaño de bloque que se defina. Estos buffers deben ser proporcionados desde la
 * la aplicación a través de la estructura dbfsRoot, utilizada para almacenar la configuración del file system.
 *
 * Changes:
 *
 * v1.2
 * 29/03/2018: Bug fix: varios errores en el almacenamiento y definición de los tamaños de los archivos.
 *             Update: modificación del algoritmo de escritura de modo que cada sector sea escrito solo
 *             una vez.
 *
 * v1.1
 * 04/01/2018: Bug fix: error en la definición de las funciones de lectura y escritura sobre la memoria flash/eeprom
 * 04/01/2018: Modificación de la forma de direccionamiento dentro del sistema de archivo, cambiando direcciones
 * absolutas por desplazamientos.
 *
 * v1.0
 * 02/01/2018: Modificación del indicador de tamaño removido de la estructura del punto de entrada
 * y reubicada en el final del último bloque del archivo.
 *
 */


#ifndef datBoxFs_h
#define datBoxFs_h
#include <fs.h>
#include <stdint.h>
#include <stdio.h>
#define invalidAddr ((uint32_t) -1)
#define BLKEMPTY EMPTY_ENTRY

#ifdef __cplusplus
 extern "C"{
#endif

// Valor utilizado para representar una entrada vacía
#define EMPTY_ENTRY (0xFF)
#define USED_ENTRY  (0x00)

// Longitud máxima del nombre de un archivo.
#define dbFSMxFileNameLen (28)

uint8_t checkEmpty(uint32_t toTest);

struct stDbFsStruct;

typedef void (*openFunction)(uint32_t dummy);
typedef void (*writeFunction)(struct stDbFsStruct *, uint32_t offset, uint8_t *readFrom, uint32_t len, void * baseAddr);
typedef void (*readFunction) (struct stDbFsStruct *, uint8_t *writeTo, uint32_t offset, uint32_t len, void * baseAddr);
typedef void (*closeFunction)(uint32_t dummy);

struct stDbFsStruct
{
   void *        absAddr;      /* Dirección de base donde se contendrán todos los datos */
   uint32_t      length;       /* Espacio disponible para el file system                */
   uint8_t *     rwBuff;       /* Debe ser del mismo tamaño que writeAlign              */
   openFunction  fcnOpn;
   writeFunction fcnWr;        /* Puntero a la función de escritura                     */
   readFunction  fcnRd;        /* Puntero a la función de lectura                       */
   closeFunction fcnCls;
   uint32_t      maxFiles;     /* Cantidad máxima de archivos en el file system         */
   uint32_t      blkSize;      /* Tamaño del bloque de asignación mínimo (128)          */
#ifdef PCDEV
   FILE * fileHandle;
#endif
};

typedef struct stDbFsStruct dbfsRoot;

/*
 * Cuidado! El tamaño de la estructura siguiente tiene que se múltiplo de la alineación
 * de escritura utilizada. Se puede agregar un campo para forzar la alinecación o la
 * longitud del nombre del archivo.
 **/

typedef struct
{
	char fn[dbFSMxFileNameLen];
	uint32_t firstBlock;    // Dirección del primer bloque vinculado al archivo.
}dbfsFileEntry;

typedef struct
{
	int32_t entryId;     // Id del archivo, con este valor se puede calcular número de bloque, offset y dirección
	uint32_t offSet;     // Posición del cursor dentro del archivo una vez abierto
	dbfsRoot * dbfs;     // Puntero al filesystem.
}dbfsFileState;

void dbfsGetFileEntryFromId(dbfsRoot *fs,int id, dbfsFileEntry *fe);
void dbfsFormat(struct stFileSystems *);
uint32_t dbfsGetFreeBlock(dbfsRoot *, uint32_t);
int32_t dbfsFileExists(dbfsRoot *, char *);
void dbfsTruncFile(dbfsFileState *);

// Abre un archivo dado por name, con las opciones indicadas en flags
int dbfsOpen(char *name, int flags, int mode, struct stFileSystems *fs);
int dbfsRead (int fd, uint8_t *buf, int len);
int dbfsWrite(int fd, const uint8_t *buf, int len);
long dbfsSeek(int fd, long off, int from);
int dbfsClose(int);

//
int dbfsUnlink(char *fn, struct stFileSystems *fs);
void dbfsOpenDir(struct stFileSystems *fs, dirEntry *dir);
fileEntry* dbfsReadDir(dirEntry *dir);
void dbfsCloseDir(dirEntry *dir);

//void copyPtr(uint32_t *to, uint32_t * from);
#define copyPtr(to, from) \
	((unsigned char *)to)[0] = ((unsigned char *)from)[0]; \
	((unsigned char *)to)[1] = ((unsigned char *)from)[1]; \
	((unsigned char *)to)[2] = ((unsigned char *)from)[2]; \
	((unsigned char *)to)[3] = ((unsigned char *)from)[3];


#ifdef __cplusplus
 }
#endif

#endif
