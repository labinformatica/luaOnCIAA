/* Copyright 2017, 2018 Sergio Burgos
 * Copyright 2017, 2018 Félix Taborda
 * Copyright 2017, 2018 Exequiel Benavídez
 * Copyright 2017, 2018 Juan Moragues
 * Copyright 2017, 2018 Sergio Comas
 * Copyright 2017, 2018 Andrés Tapari
 * All rights reserved.
 *
 * This file is part of Lua on CIAA project.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef fs_h
#define fs_h

#ifdef __cplusplus
 extern "C"{
#endif

#include <config.h>
#include <stdint.h>
#include <sys/stat.h>

#define USING_FS

struct stFileSystems;
struct stDirEntry;
struct stFileEntry;
// Funciones sobre directorios
typedef void (*ptrOpenDir)(struct stFileSystems *, struct stDirEntry *);
typedef struct stFileEntry* (*ptrReadDir)(struct stDirEntry *);
typedef void (*ptrCloseDir)(struct stDirEntry *);

// Funciones sobre archivos
typedef int (*ptrUnlink)(char *, struct stFileSystems *); // Puntero a una función que elimine un archivo del file system
typedef int (*ptrStat) (char *, struct stat *st);
typedef int (*ptrOpenFile) (char *name, int, int, struct stFileSystems *);
typedef int (*ptrReadFile) (int, uint8_t *, int);
typedef int (*ptrWriteFile)(int, const uint8_t *, int);
typedef long (*ptrSeekFile)(int, long, int);
typedef int (*ptrCloseFile)(int);
typedef void (*ptrFormat)(struct stFileSystems *);

struct stFileSystems
{
    char fsName[MAXMOUNTPOINTNAME];
    ptrOpenDir openDir;
    ptrReadDir readDir;
    ptrCloseDir closeDir;
    ptrOpenFile openFile;
    ptrReadFile readFile;
    ptrWriteFile writeFile;
    ptrSeekFile seekFile;
    ptrCloseFile closeFile;
    ptrUnlink unlink;
    ptrStat stat;
    ptrFormat format;
    void *fsData;
} __attribute__((aligned (4)));


typedef struct
{
  struct stFileSystems *fs;
  void *fileData;
  unsigned char inUse;
}fileDesc;

#define FILE_OK             (0)
#define FILE_SIZE_ERROR     (1 << 0)
#define FILE_UNKNOWN_ERROR  (1 << 1)
#define FILE_CORRUPT 		(1 << 2)

typedef struct stFileEntry
{
	char * fn;
	uint32_t memOffset;
	uint32_t size;
	uint8_t status;
} fileEntry;

typedef struct stDirEntry
{
	unsigned int idxEntry;   // Número de archivo accedido por última vez
	struct stFileSystems * fs;        // File system (podría haber varios)
	fileEntry fe;            // Estructura con los datos
	unsigned int lastOffset; // Variable auxiliar que puede utilizarce con diferentes fines
							 // según la implementación del file system, en el caso de romFs
		                     // y dbfs representa el desplazamiento en el filesystem
} dirEntry;

struct stFileSystems *getRegisteredFS(unsigned int fsId);
unsigned char registerFS(struct stFileSystems *);
unsigned char registerStdFD(struct stFileSystems *, int fd);
unsigned int getRegisteredFSCount(void);

int getFirstFreeFD(void);
void freeFD(int fd);

#ifdef __cplusplus
 }
#endif

#endif
